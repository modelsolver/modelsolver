[![Coverage Status](https://coveralls.io/repos/bitbucket/modelsolver/modelsolver/badge.svg?branch=develop)](https://coveralls.io/bitbucket/modelsolver/modelsolver?branch=develop) [![Maven Central](https://maven-badges.herokuapp.com/maven-central/com.meliorbis.economics/ModelSolver/badge.svg)](https://maven-badges.herokuapp.com/maven-central/com.meliorbis.economics/ModelSolver/badge.svg)

#The ModelSolver Library#

The ModelSolver Library provides tools for solving economic models using Java and other JVM languages. It is part of the [ModelSolver Toolkit](http://modelsolver.bitbucket.org/). The library is provided under an [MIT License](https://bitbucket.org/modelsolver/modelsolver/raw/bfac78997821dea61923c87b5f39d001d732601a/license.txt).

For sample models using the Library (in [Scala](www.scala-lang.org)) see [Incomplete Markets - Scala](https://bitbucket.org/modelsolver/incomplete-markets-scala).