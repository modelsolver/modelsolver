<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>com.meliorbis.economics</groupId>
	<artifactId>ModelSolver</artifactId>
	<version>1.1</version>
	<packaging>jar</packaging>

	<name>ModelSolver</name>
	<description>A library for solving economic models, particularly 
	macroeconomic models with heterogeneous agents who have model-consistent
	expectations</description>
	<url>https://bitbucket.org/modelsolver/modelsolver</url>

	<licenses>
		<license>
			<name>MIT License</name>
			<url>http://opensource.org/licenses/MIT</url>
			<distribution>repo</distribution>
		</license>
	</licenses>

	<organization>
		<name>Meliorbis</name>
		<url>http://www.meliorbis.com</url>
	</organization>

	<developers>
		<developer>
			<id>toby</id>
			<name>Tobias Grasl</name>
			<email>toby@meliorbis.org</email>
			<url>http://www.meliorbis.com</url>
			<organization>Meliorbis</organization>
			<organizationUrl>http://www.meliorbis.com</organizationUrl>
			<roles>
				<role>developer</role>
			</roles>
			<timezone>+1</timezone>
		</developer>
	</developers>

	<scm>
		<url>scm:git:ssh://git@bitbucket.org:modelsolver/modelsolver.git</url>
	</scm>

	<distributionManagement>
		<snapshotRepository>
			<id>ossrh</id>
			<url>https://oss.sonatype.org/content/repositories/snapshots</url>
		</snapshotRepository>
		<repository>
			<id>ossrh</id>
			<url>https://oss.sonatype.org/service/local/staging/deploy/maven2/</url>
		</repository>
	</distributionManagement>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
	</properties>

	<build>
		<sourceDirectory>src/main/java</sourceDirectory>
		<testSourceDirectory>src/test/java</testSourceDirectory>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>2.3.2</version>
				<configuration>
					<source>1.8</source>
					<target>1.8</target>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-source-plugin</artifactId>
				<version>2.2.1</version>
				<executions>
					<execution>
						<id>attach-sources</id>
						<goals>
							<goal>jar-no-fork</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-javadoc-plugin</artifactId>
				<version>2.9.1</version>
				<executions>
					<execution>
						<id>attach-javadocs</id>
						<goals>
							<goal>jar</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-gpg-plugin</artifactId>
				<version>1.6</version>
				<executions>
					<execution>
						<id>sign-artifacts</id>
						<phase>verify</phase>
						<goals>
							<goal>sign</goal>
						</goals>
						<configuration>
							<keyname>E8FB9C29</keyname>
							<passphraseServerId>E8FB9C29</passphraseServerId>
						</configuration>
					</execution>
				</executions>
			</plugin>
			<plugin>
			    <groupId>org.eluder.coveralls</groupId>
			    <artifactId>coveralls-maven-plugin</artifactId>
			    <version>4.0.0</version>
			    <configuration>
			        <repoToken>zEbV7vqqsCJpm2Mtom7WVSQIO4LcdWuWO</repoToken>
			    </configuration>
			</plugin>
			<plugin>
				<groupId>org.jacoco</groupId>
				<artifactId>jacoco-maven-plugin</artifactId>
				<version>0.7.5.201505241946</version>
				<executions>
					<execution>
						<id>prepare-agent</id>
						<goals>
							<goal>prepare-agent</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
		</plugins>
	</build>
	<dependencies>
		<dependency>
			<groupId>commons-lang</groupId>
			<artifactId>commons-lang</artifactId>
			<version>2.6</version>
		</dependency>
		<dependency>
			<groupId>com.meliorbis.numerics</groupId>
			<artifactId>Numerics</artifactId>
			<version>1.2</version>
			<exclusions>
				<exclusion>
					<artifactId>hamcrest-core</artifactId>
					<groupId>org.hamcrest</groupId>
				</exclusion>
			</exclusions>
		</dependency>

		<dependency>
			<groupId>commons-cli</groupId>
			<artifactId>commons-cli</artifactId>
			<version>1.2</version>
		</dependency>
		<dependency>
			<groupId>org.picocontainer</groupId>
			<artifactId>picocontainer</artifactId>
			<version>2.14.1</version>
		</dependency>
		<dependency>
			<groupId>net.sourceforge.collections</groupId>
			<artifactId>collections-generic</artifactId>
			<version>4.01</version>
		</dependency>

		<!-- <dependency> <groupId>org.la4j</groupId> <artifactId>la4j</artifactId> 
			<version>0.4.0</version> </dependency> <dependency> <groupId>com.googlecode.netlib-java</groupId> 
			<artifactId>netlib-java</artifactId> <version>0.9.3</version> </dependency> -->
		<dependency>
			<groupId>org.jmock</groupId>
			<artifactId>jmock-junit4</artifactId>
			<version>2.6.0</version>
		</dependency>
		<dependency>
			<groupId>org.hamcrest</groupId>
			<artifactId>hamcrest-core</artifactId>
			<version>1.3</version>
		</dependency>
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit-dep</artifactId>
			<version>4.11</version>
			<type>pom</type>
		</dependency>
	</dependencies>
</project>
