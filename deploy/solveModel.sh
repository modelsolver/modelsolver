#!/bin/sh

LIBS=./lib

java -classpath './lib/*'  -Dcom.meliorbis.debug=false -Xmx8000M -Dcom.meliorbis.numerics.threads=30 -Dcom.meliorbis.newWeight=1 -Dcom.meliorbis.aggPrecision=1e-6 com.meliorbis.economics.model.config.HetAgentILModelRunner -gradDensity "." -simulate
