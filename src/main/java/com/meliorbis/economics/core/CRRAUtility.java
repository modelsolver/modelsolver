package com.meliorbis.economics.core;

/**
 * A Utility function that exhibits constant relative risk aversion
 * 
 * @author Tobias Grasl
 */
public class CRRAUtility implements UtilityFunction
{
	private final UtilityFunction _delegand;
	
	/**
	 * Constructs the function
	 * 
	 * @param eta_ The parameter &eta; in the utility function
	 */
	public CRRAUtility(double eta_)
	{
		// Is eta an integer?
		if(Math.abs(Math.floor(eta_)- eta_) < 1e-15)
		{
			_delegand = new IntegerPowerUtility((int)Math.floor(eta_));
		}
		else
		{
			_delegand = new GeneralPowerUtility(eta_);
		}
	}

	/**
	 * @param inputs_ The inputs to the utility function. Should be of length 1
	 * 
	 * @return The calculated utility. Will return -&infin; for negative inputs
	 */
	public double utility(double... inputs_)
	{
		if(inputs_[0] <= 0)
		{
			return Double.NEGATIVE_INFINITY;
		}
		
		return _delegand.utility(inputs_);
	}

	/**
	 * @param index_ The index of the input variable for which to calculate the
	 * marginal utility. Must be 0.
	 * @param inputs_ The inputs at which to calculate the marginal utility
	 * 
	 * @return The marginal utility
	 */
	public double marginalUtility(int index_, double... inputs_)
	{
		return _delegand.marginalUtility(index_, inputs_);
	}


	/**
	 * @see UtilityFunction#inverseMarginalUtility(double)
	 */
	@Override
	public double inverseMarginalUtility(double utility_)
	{
		return _delegand.inverseMarginalUtility(utility_);
	}


	private class GeneralPowerUtility implements UtilityFunction
	{
		private final double _eta;
		private final double _utilPower;
		private final double _utilMult;
		
		public GeneralPowerUtility(double eta_)
		{
			_eta = eta_;
			
			_utilPower = (1d-_eta);
			_utilMult = 1/_utilPower;
		}	
		
		@Override
		public double utility(double... inputs_)
		{
			if(inputs_[0] <= 0d)
			{
				return Double.NEGATIVE_INFINITY;
			}
			
			return Math.pow(inputs_[0],_utilPower)*_utilMult;
		}
	
		@Override
		public double marginalUtility(int index_, double... inputs_)
		{
			return Math.pow(inputs_[index_],-_eta);
		}

		@Override
		public double inverseMarginalUtility(double  utility_)
		{
			return Math.pow(utility_, -1d/_eta);
		}
	}
	
	private class IntegerPowerUtility implements UtilityFunction
	{
		private final int _power;
		private final double _factor;
		
		public IntegerPowerUtility(int power_)
		{
			_power = power_;
			_factor = 1d/(1-_power);
		}

		@Override
		public double utility(double... inputs_)
		{
			double result = 1;
			
			for(int count = 0; count< Math.abs(1-_power); count++)
			{
				result *= inputs_[0];
			}
			
			if((1-_power) < 0)
			{
				result = 1d/result;
			}
			
			return result *= _factor;
		}

		@Override
		public double marginalUtility(int index_, double... inputs_)
		{
			double result = 1;
			
			for(int count = 0; count< Math.abs(_power); count++)
			{
				result *= inputs_[0];
			}
			
			if((-_power) < 0)
			{
				result = 1d/result;
			}
			
			return result;
		}

		@Override
		public double inverseMarginalUtility(double utility_)
		{
			// Won't be fast because the power is non-integral
			return Math.pow(utility_, -1d/_power);
		}
	}
}
