/**
 * 
 */
package com.meliorbis.economics.core;

/**
 * 
 * @author Tobias Grasl
 */
public class LogUtility implements UtilityFunction {

	/* (non-Javadoc)
	 * @see com.meliorbis.economics.core.IUtilityFunction#utility(java.lang.Double[])
	 */
	@Override
	public double utility(double... inputs_) {
		return Math.log(inputs_[0]);
	}

	/* (non-Javadoc)
	 * @see com.meliorbis.economics.core.IUtilityFunction#marginalUtility(int, java.lang.Double[])
	 */
	@Override
	public double marginalUtility(int index_, double... inputs_) {
		return 1d/inputs_[0];
	}

	/* (non-Javadoc)
	 * @see com.meliorbis.economics.core.IUtilityFunction#inverseMarginalUtility(java.lang.Double)
	 */
	@Override
	public double inverseMarginalUtility(double utility_) {
		return 1d/utility_;
	}

}
