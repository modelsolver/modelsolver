/**
 * 
 */
package com.meliorbis.economics.core;

/**
 * A labour-augmenting Cobb-Douglas production function
 * 
 * @author Tobias Grasl
 */
public class LabourAugmentingCDProduction implements ProductionFunction
{
	private final double _capitalPower;
	private final double _labourPower;
	private final double _depreciationRate;
	
	/**
	 * Constructs the production function with the provided parameters
	 * 
	 * @param capitalPower_ The power on capital in the production function
	 * @param labourPower_ The power on labour in the production function
	 * @param depreciationRate_ The depreciation rate
	 */
	public LabourAugmentingCDProduction(double capitalPower_, double labourPower_, double depreciationRate_)
	{
		_capitalPower = capitalPower_;
		_labourPower = labourPower_;
		_depreciationRate = depreciationRate_;
	}

	/**
	 * Constructs a CRS production function with the provided parameters
	 * 
	 * @param capitalPower_ The power on capital in the production function. The power on labour is (1-capitalPower_)
	 * @param depreciationRate_ The depreciation rate
	 */
	public LabourAugmentingCDProduction(double capitalPower_, double depreciationRate_)
	{
		_capitalPower = capitalPower_;
		_labourPower = 1d - _capitalPower;
		_depreciationRate = depreciationRate_;
	}

	/* (non-Javadoc)
	 * @see com.meliorbis.economics.core.IProductionFunction#output(double, double, double)
	 */
	@Override
	public double output(double productivity_, double capital_, double labour_)
	{
		return Math.pow(capital_, _capitalPower)*Math.pow(productivity_*labour_, _labourPower);
	}

	/* (non-Javadoc)
	 * @see com.meliorbis.economics.core.IProductionFunction#netReturn(double, double, double)
	 */
	@Override
	public double netReturn(double productivity_, double capital_, double labour_)
	{
		return _capitalPower*Math.pow(capital_, _capitalPower-1)*Math.pow(productivity_*labour_, _labourPower)-_depreciationRate;
	}

	/* (non-Javadoc)
	 * @see com.meliorbis.economics.core.IProductionFunction#wage(double, double, double)
	 */
	@Override
	public double wage(double productivity_, double capital_, double labour_)
	{
		return productivity_*_labourPower*Math.pow(capital_, _capitalPower)*Math.pow(productivity_*labour_, _labourPower-1);
	}

	/* (non-Javadoc)
	 * @see com.meliorbis.economics.core.IProductionFunction#impliedCapLabRatio(double, double)
	 */
	@Override
	public double impliedCapLabRatio(double productivity_, double r_)
	{
		throw new UnsupportedOperationException();
	}

}
