/**
 * 
 */
package com.meliorbis.economics.core;

/**
 * Represents a general utility function
 * 
 * @author Tobias Grasl
 */
public interface UtilityFunction
{
	/**
	 * Determines the utility derived from consuming the given inputs
	 * 
	 * @param inputs_ arbitrary-length array of inputs
	 * 
	 * @return The utility achieved
	 */
	double utility(double... inputs_);
	
	/**
	 * Determines marginal utility with respect to the index_'th input
	 * 
	 * @param index_ The index of the input with respect to which we desire marginal
	 * utility
	 * @param inputs_ The point at which marginal utility is to be determined
	 * 
	 * @return The marginal utility
	 */
	double marginalUtility(int index_, double... inputs_);
	
	/**
	 * The input for the given marginal utility
	 * NOTE: Only works for single-input utility, of course
	 * 
	 * @param utility_ The utility level achieved 
	 * 
	 * @return The input (consumption) to achieve the given utility
	 */
	double inverseMarginalUtility(double utility_);
}
