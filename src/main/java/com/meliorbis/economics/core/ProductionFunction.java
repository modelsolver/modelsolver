package com.meliorbis.economics.core;

/**
 * A function that takes productivity capital and labour as inputs, and can calculate wage, bet return on capital and output
 * 
 * @author Tobias Grasl
 */
public interface ProductionFunction
{
	/**
	 * Determine output given the inputs
	 * 
	 * @param productivity_ The productivity factor
	 * @param capital_ The capital deployed in production
	 * @param labour_ The labour deployed in production
	 * 
	 * @return The output produced
	 */
	public double output(double productivity_, double capital_, double labour_);
	
	/**
	 * Determine net interest rate
	 * 
	 * @param productivity_ The productivity factor
	 * @param capital_ The capital deployed in production
	 * @param labour_ The labour deployed in production
	 * 
	 * @return The net return on capital produced
	 */
	public double netReturn(double productivity_, double capital_, double labour_);
	
	/**
	 * Determine the wage given inputs
	 * 
	 * @param productivity_ The productivity factor
	 * @param capital_ The capital deployed in production
	 * @param labour_ The labour deployed in production
	 * 
	 * @return The resulting wage
	 */
	public double wage(double productivity_, double capital_, double labour_);
	
	/**
	 * Determines the capital/labour ratio implied by a given interest rate
	 * 
	 * @param productivity_ The productivity factor
	 * @param r_ The interest rate
	 * 
	 * @return The capital labour ratio that would produce the given interest
	 * rate
	 */
	public double impliedCapLabRatio(double productivity_, double r_);
}
