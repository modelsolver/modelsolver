/**
 * 
 */
package com.meliorbis.economics.core;

/**
 * A cobb-douglas production function
 * 
 * @author toby
 */
public class CobbDouglasProduction implements ProductionFunction
{
	private final double _capitalPower;
	private final double _labourPower;
	private final double _depreciationRate;
	
	public CobbDouglasProduction(double capitalPower_, double labourPower_, double deprecitationRate_)
	{
		_capitalPower = capitalPower_;
		_labourPower = labourPower_;
		_depreciationRate = deprecitationRate_;
	}

	@Override
	public double output(double productivity_, double capital_, double labour_)
	{
		return productivity_*Math.pow(capital_, _capitalPower)*Math.pow(labour_, _labourPower);
	}

	@Override
	public double netReturn(double productivity_, double capital_, double labour_)
	{
		return productivity_*_capitalPower*Math.pow(capital_, _capitalPower-1)*Math.pow(labour_, _labourPower)-_depreciationRate;
	}

	@Override
	public double wage(double productivity_, double capital_, double labour_)
	{
		return productivity_*_labourPower*Math.pow(capital_, _capitalPower)*Math.pow(labour_, _labourPower-1);
	}

	@Override
	public double impliedCapLabRatio(double productivity_, double r_)
	{
		// Note this assumes CRS, whereas the rest does not!
		return Math.pow(((r_+_depreciationRate)/productivity_)/_capitalPower,1/(_capitalPower-1));
	}
}
