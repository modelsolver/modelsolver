/**
 * 
 */
package com.meliorbis.economics.infrastructure.expectations;

import com.meliorbis.economics.model.State;

/**
 * @author Tobias Grasl
 *
 */
public class ExpectationProviderUnderUncertainty implements ExpectationProvider
{

	/* (non-Javadoc)
	 * @see com.meliorbis.economics.infrastructure.expectations.ExpectationProvider#getExpectedAggregateState(int[], int[], com.meliorbis.economics.model.State, int)
	 */
	@Override
	public double getExpectedAggregateState(int[] transition_, int[] currentAggs_, State<?> state_, int stateIndex_)
	{
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see com.meliorbis.economics.infrastructure.expectations.ExpectationProvider#getExpectedAggregateControl(int[], int[], com.meliorbis.economics.model.State, int)
	 */
	@Override
	public double getExpectedAggregateControl(int[] transition_, int[] currentAggs_, State<?> state_, int stateIndex_)
	{
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see com.meliorbis.economics.infrastructure.expectations.ExpectationProvider#getExpectedIndividualTransition(int[], int[], com.meliorbis.economics.model.State)
	 */
	@Override
	public double getExpectedIndividualTransition(int[] transition_, int[] currentAggs_, State<?> state_)
	{
		// TODO Auto-generated method stub
		return 0;
	}

}
