/**
 * 
 */
package com.meliorbis.economics.infrastructure.expectations;

import com.meliorbis.economics.model.State;

/**
 * An interface for accessing expectations of future values, conditional on current aggregates
 * and future shocks
 * 
 * @author Tobias Grasl
 */
public interface ExpectationProvider
{
	/**
	 * Returns the expectations of the indicated aggregate state variable given the inputs.
	 * 
	 * @param transition_ The exogenous state transition
	 * @param currentAggs_ The current aggregates when expectations are formed
	 * @param state_ The calculation state
	 * @param stateIndex_ The index of the state variable for which the expectation is required
	 * 
	 * @return The expected value
	 */
	double getExpectedAggregateState(int[] transition_, int[] currentAggs_, State<?> state_, 
			int stateIndex_);
	
	/**
	 * Returns the expectations of the indicated aggregate control variable given the inputs.
	 * 
	 * @param transition_ The exogenous state transition from current to prior 
	 * @param currentAggs_ The current aggregates when expectations are formed
	 * @param state_ The calculation state
	 * @param controlIndex_ The index of the control variable for which the expectation is required
	 * 
	 * @return The expected value
	 */
	double getExpectedAggregateControl(int[] transition_, int[] currentAggs_, State<?> state_, 
			int controlIndex_);
	
	/**
	 * Returns the expectations of the individual transition function given the inputs.
	 * 
	 * @param transition_ The exogenous state transition from current to prior 
	 * @param currentAggs_ The current aggregates when expectations are formed
	 * @param state_ The calculation state
	 * 
	 * @return The expected value
	 */
	double getExpectedIndividualTransition(int[] transition_, int[] currentAggs_, State<?> state_);
}
