/**
 * 
 */
package com.meliorbis.economics.infrastructure;

import com.meliorbis.economics.model.Model;
import com.meliorbis.economics.model.ModelConfig;
import com.meliorbis.economics.model.State;

/**
 * Base class for solvers
 * 
 * @author Tobias Grasl
 * 
 * @param <C> The Config type
 * @param <S> The State type
 * @param <M> The Model type
 */
public abstract class SolverBase<C extends ModelConfig, S extends State<C>, M extends Model<C, S>> extends Base
{
	final protected C _config;
	final protected M _model;

	
	
	/**
	 * @param model_ The model being solved
	 * @param config_ The configuration of the model being solved
	 */
	protected SolverBase(M model_, C config_)
	{
		super();
		
		_model = model_;
		_config = config_;
	}
}
