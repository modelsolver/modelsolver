package com.meliorbis.economics.infrastructure;

/**
 * Base of the exception hierarchy to be used by the Solver framework
 */
public class SolverException extends Exception
{
     public SolverException(String description_)
     {
         super(description_);
     }

    public SolverException(String description_, Throwable cause_)
    {
        super(description_, cause_);
    }
}
