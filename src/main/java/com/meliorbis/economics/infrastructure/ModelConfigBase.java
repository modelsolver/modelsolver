/**
 * 
 */
package com.meliorbis.economics.infrastructure;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.meliorbis.economics.infrastructure.simulation.SimState;
import com.meliorbis.economics.model.SettableModelConfig;
import com.meliorbis.numerics.generic.MultiDimensionalArray;
import com.meliorbis.numerics.generic.primitives.DoubleArray;
import com.meliorbis.numerics.io.NumericsReader;
import com.meliorbis.numerics.io.NumericsWriter;

/**
 * A base class for model config that holds the generally requires objects
 * 
 * @author Tobias Grasl
 */
public abstract class ModelConfigBase implements SettableModelConfig
{
	private List<DoubleArray<?>> _indEndoStates = Collections.emptyList();
	private List<DoubleArray<?>> _indEndoSimStates= Collections.emptyList();
	private List<DoubleArray<?>> _indExoStates= Collections.emptyList();
	private List<DoubleArray<?>> _aggEndoStates= Collections.emptyList();
	private List<DoubleArray<?>> _aggExoStates= Collections.emptyList();
	private List<DoubleArray<?>> _aggNormalisingExoStates= Collections.emptyList();
	private DoubleArray<?> _exoStateTransitionProbs;
	private List<DoubleArray<?>> _aggControls = Collections.emptyList();
	private List<DoubleArray<?>> _ctrlTargets = null;
	
	private int[] _aggregatesKnownInAdvance = new int[0];
	private int[] _controlsAffectingExpns = new int[0];
	

    private File _solutionDir;
	private SimState _initialSimState;
	private MultiDimensionalArray<?, ?> _initialExoStates;

	private boolean _aggUncert;
	private boolean _indUncert;
	
	public ModelConfigBase()
	{
		
	}
	
	public ModelConfigBase(List<DoubleArray<?>> indEndoStates_,
			List<DoubleArray<?>> indExoStates_,
			List<DoubleArray<?>> aggEndoStates_,
			List<DoubleArray<?>> aggExoStates_,
			List<DoubleArray<?>> aggNormalisingExoStates_,
			DoubleArray<?> exoStateTransitionProbs_)
	{
		_indEndoStates = indEndoStates_;
		_indExoStates = indExoStates_;
		_aggEndoStates = aggEndoStates_;
		_aggExoStates = aggExoStates_;
		_aggNormalisingExoStates = aggNormalisingExoStates_;
		_exoStateTransitionProbs = exoStateTransitionProbs_;
		
		updateAggUncert();
		updateIndUncert();
	}

	private void updateAggUncert()
	{
		// If any of the agg exo states...
		_aggUncert = _aggExoStates.stream().anyMatch( array -> array.numberOfElements() > 1);
		
		// ...or any of the normalising exo states have more than one potential value,
		// then there is aggregate uncertainty
		_aggUncert |= _aggNormalisingExoStates.stream().anyMatch( array -> array.numberOfElements() > 1);
	}
	
	private void updateIndUncert()
	{
		// If any of the ind exo states have more than one potential value,
		// then there is individual uncertainty
		_indUncert = _indExoStates.stream().anyMatch( array -> array.numberOfElements() > 1);
	}
	
	/* (non-Javadoc)
	 * @see com.meliorbis.economics.model.IModelConfig#getIndividualExogenousStates()
	 */
	@Override
	public List<DoubleArray<?>> getIndividualExogenousStates()
	{
		return _indExoStates;
	}

	/* (non-Javadoc)
	 * @see com.meliorbis.economics.model.IModelConfig#getIndividualEndogenousStates()
	 */
	@Override
	public List<DoubleArray<?>> getIndividualEndogenousStates()
	{
		return _indEndoStates;
	}

	/* (non-Javadoc)
	 * @see com.meliorbis.economics.model.IModelConfig#getAggregateExogenousStates()
	 */
	@Override
	public List<DoubleArray<?>> getAggregateExogenousStates()
	{
		return _aggExoStates;
	}

	/* (non-Javadoc)
	 * @see com.meliorbis.economics.model.IModelConfig#getAggregateEndogenousStates()
	 */
	@Override
	public List<DoubleArray<?>> getAggregateEndogenousStates()
	{
		return _aggEndoStates;
	}

	@Override
	public List<DoubleArray<?>> getAggregateNormalisingExogenousStates()
	{
		return _aggNormalisingExoStates;
	}

	/* (non-Javadoc)
	 * @see com.meliorbis.economics.model.IModelConfig#getExogenousStateTransiton()
	 */
	@Override
	public DoubleArray<?> getExogenousStateTransition()
	{
		return _exoStateTransitionProbs;
	}

    @Override
    public File getSolutionDirectory()
    {
        return _solutionDir;
    }

    public void setIndividualExogenousStatesForSimulation(List<DoubleArray<?>> simStates_)
	{
		_indEndoSimStates = simStates_;
	}

	@Override
	public List<DoubleArray<?>> getIndividualEndogenousStatesForSimulation()
	{
		return _indEndoSimStates.isEmpty() ? getIndividualEndogenousStates() : _indEndoSimStates;
	}

	@Override
	public void setIndividualExogenousStates(List<DoubleArray<?>> indExoStates_)
	{
		_indExoStates = indExoStates_;
		
		updateIndUncert();
	}
	
	@Override
	public void setIndividualExogenousStates(DoubleArray<?>... indExoStates_)
	{
		setIndividualExogenousStates(Arrays.asList(indExoStates_));		
	}

	@Override
	public void setIndividualEndogenousStates(List<DoubleArray<?>> indEndoStates_)
	{
		_indEndoStates = indEndoStates_;
	}
	
	
	@Override
	public void setIndividualEndogenousStates(DoubleArray<?>... indEndoStates_)
	{
		setIndividualEndogenousStates(Arrays.asList(indEndoStates_));
	}

	@Override
	public void setIndividualEndogenousStatesForSimulation(DoubleArray<?>... indEndoSimStates_)
	{
		setIndividualEndogenousStatesForSimulation(Arrays.asList(indEndoSimStates_));
	}

	@Override
	public void setAggregateExogenousStates(DoubleArray<?>... aggExoStates_)
	{
		setAggregateExogenousStates(Arrays.asList(aggExoStates_));
	}

	@Override
	public void setAggregateEndogenousStates(DoubleArray<?>... aggEndoStates_)
	{
		setAggregateEndogenousStates(Arrays.asList(aggEndoStates_));
	}

	@Override
	public void setAggregateControls(DoubleArray<?>... aggControls_)
	{
		setAggregateControls(Arrays.asList(aggControls_));
	}
	
	@Override
	public void setControlTargets(DoubleArray<?>... controlTargets_)
	{
		setControlTargets(Arrays.asList(controlTargets_));
	}

	@Override
	public void setAggregateNormalisingExogenousStates(DoubleArray<?>... aggNormStates_)
	{
		setAggregateNormalisingExogenousStates(Arrays.asList(aggNormStates_));
	}

	@Override
	public void setIndividualEndogenousStatesForSimulation(List<DoubleArray<?>> indEndoSimStates_)
	{
		_indEndoSimStates = indEndoSimStates_;
	}

	@Override
	public void setAggregateExogenousStates(List<DoubleArray<?>> aggExoStates_)
	{
		_aggExoStates = aggExoStates_;
		
		updateAggUncert();
	}

	@Override
	public void setAggregateEndogenousStates(List<DoubleArray<?>> aggEndoStates_)
	{
		_aggEndoStates = aggEndoStates_;
	}

	@Override
	public void setAggregateNormalisingExogenousStates(List<DoubleArray<?>> aggNormStates_)
	{
		_aggNormalisingExoStates = aggNormStates_;
		
		updateAggUncert();
	}

	@Override
	public void setExogenousStateTransiton(DoubleArray<?> transitionProbs_)
	{
		_exoStateTransitionProbs = transitionProbs_;
	}

    @Override
    public void setSolutionDirectory(File dir_)
    {
        _solutionDir = dir_;
    }

    @Override
    public List<DoubleArray<?>> getAggregateControls()
	{
		return _aggControls;
	}
    
    @Override
    public List<DoubleArray<?>> getControlTargets()
	{
		return _ctrlTargets == null ? _aggControls : _ctrlTargets;
	}

    @Override
	final public void setAggregateControls(List<DoubleArray<?>> aggControls)
	{
		_aggControls = aggControls;
	}
    
    @Override
   	final public void setControlTargets(List<DoubleArray<?>> ctrlTargets_)
   	{
   		_ctrlTargets = ctrlTargets_;
   	}

	@Override
	final public int getAggregateExogenousStateCount() {
		return _aggExoStates.size();
	}

	@Override
	final public int getAggregateEndogenousStateCount() {
		return _aggEndoStates.size();
	}
	
	@Override
	final public int getAggregateControlCount() {
		return _aggControls.size();
	}

	@Override
	final public int getIndividualEndogenousStateCount() {
		return _indEndoStates.size();
	}

	@Override
	final public int getIndividualExogenousStateCount()
	{
		return _indExoStates.size();
	}
	
	@Override
	final public int getAggregateNormalisingStateCount()
	{
		return _aggNormalisingExoStates.size();
	}

	public void setInitialSimState(SimState initialSimState_)
	{
		_initialSimState = initialSimState_;
	}
	
	@Override
	public SimState getInitialSimState()
	{
		return _initialSimState;
	}
	
	public void setInitialExogenousStates(MultiDimensionalArray<?, ?> initialExoStates_)
	{
		_initialExoStates = initialExoStates_;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <N extends Number> MultiDimensionalArray<N, ?> getInitialExogenousStates()
	{
		return (MultiDimensionalArray<N, ?>) _initialExoStates;
	}

	@Override
	public boolean hasAggUncertainty()
	{
		return _aggUncert;
	}

	@Override
	public boolean hasIndUncertainty()
	{
		return _indUncert;
	}

	@Override
	public int[] getAggregatesKnownWithCertainty()
	{
		return _aggregatesKnownInAdvance;
	}

	@Override
	public int[] getControlsAffectingExpectations()
	{
		return _controlsAffectingExpns;
	}

	@Override
	public void setAggregatesKnownWithCertainty(int... indexes_)
	{
		_aggregatesKnownInAdvance = indexes_;
	}

	@Override
	public void setControlsAffectingExpectations(int... indexes_)
	{
		_controlsAffectingExpns = indexes_;
	}

	/**
	 * This default implementation does nothing
	 */
	@Override
	public void writeParameters(NumericsWriter writer_) throws IOException
	{
	}

	/**
	 * This default implementation does nothing
	 */
	@Override
	public void readParameters(NumericsReader reader_) throws IOException
	{
	}
	
	
}
