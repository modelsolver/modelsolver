package com.meliorbis.economics.infrastructure;

import com.meliorbis.numerics.convergence.Criterion;
import com.meliorbis.numerics.generic.primitives.DoubleArray;

/**
 * State of a calculation which includes an aggregate
 * 
 * @author Tobias Grasl
 *
 */
public interface AggregateProblemState
{
	/**
	 * @param newVals_ The new value for the aggregate transition rule
	 */
	void setAggregateTransition(DoubleArray<?> newVals_);
	
	/**
	 * @return The currently assumed aggregate state transition
	 */
	DoubleArray<?> getAggregateTransition();
	
	/**
	 * Sets the grid of expected future endogenous states given current states and
	 * future exo states.
     *
     * The grid dimensions should be:
     *
     * 1) Current shocks
     * 2) Future shocks
     * 3) Future Perm shocks
     * 4) Current States
     * 5) Current Controls
     * 6) n - number of states
	 * 
	 * @param expectedStates_ The array of expected states
	 */
	void setExpectedAggregateStates(DoubleArray<?> expectedStates_);
	
	/**
	 * @return A grid of what the future deterministic state would be if the future
	 * stochastic state has a particular realisation
     *
     * See the set method for the dimension order
	 */
	DoubleArray<?> getExpectedAggregateStates();
	
	/**
	 * @param errorValue_ The current criterion value for the aggregate problem solution.
	 */
	public void setAggregateError(double errorValue_);
	
	/**
	 * @return The current criterion of the aggregate problem solution
	 */
	public Criterion getAggregateCriterion();
}
