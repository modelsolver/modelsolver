package com.meliorbis.economics.infrastructure.notifications;

import java.util.ArrayList;
import java.util.List;

import com.meliorbis.economics.model.State;
import com.meliorbis.numerics.generic.primitives.DoubleArray;

/**
 * Aggregates a bunch of change listeners and notifies them when notified
 * 
 * @author Tobias Grasl
 * 
 * @param <S> The type of State to be observed
 */
public class Notifier<S extends State<?>> implements ArrayObserver<S>
{
	List<ArrayObserver<S>> _listeners = new ArrayList<ArrayObserver<S>>();
	
	@Override
	public void changed(DoubleArray<?> oldArray_, DoubleArray<?> newArray_, S state_)
	{
		for (ArrayObserver<S> listener : _listeners)
		{
			listener.changed(oldArray_, newArray_, state_);
		}
	}
	
	public void registerListener(ArrayObserver<S> listener_)
	{
		_listeners.add(listener_);
	}
}
