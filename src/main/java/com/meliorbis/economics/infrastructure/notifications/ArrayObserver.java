/**
 * 
 */
package com.meliorbis.economics.infrastructure.notifications;

import com.meliorbis.economics.model.State;
import com.meliorbis.numerics.generic.primitives.DoubleArray;

/**
 * A functional interface to receive notifications when an array value is updated
 * 
 * @author Tobias Grasl
 * 
 * @param <S> The type of State to be observed
 */
@FunctionalInterface
public interface ArrayObserver<S extends State<?>>
{
	
	/**
	 * Notifies the implementor that the array in question has changed
	 * 
	 * @param oldArray_ The old values
	 * @param newArray_ The new values
	 * @param state_ The current calculation state
	 */
	void changed(DoubleArray<?> oldArray_, DoubleArray<?> newArray_, S state_);
}
