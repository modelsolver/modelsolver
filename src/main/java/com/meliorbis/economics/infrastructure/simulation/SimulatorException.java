package com.meliorbis.economics.infrastructure.simulation;

import com.meliorbis.economics.infrastructure.SolverException;

/**
 * Exception to be thrown when the simulator goes wrong
 */
public class SimulatorException extends SolverException
{
    public SimulatorException(String description_)
    {
        super(description_);
    }

    public SimulatorException(String description_, Throwable cause_)
    {
        super(description_, cause_);
    }
}
