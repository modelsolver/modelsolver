package com.meliorbis.economics.infrastructure.simulation;

import com.meliorbis.numerics.generic.MultiDimensionalArray;
import com.meliorbis.numerics.generic.primitives.DoubleArray;

/**
 * Holds the aggregate state of a single period
 *
 * @param <T> The type of the shocks
 */
public class PeriodAggregateState<T>
{
    private MultiDimensionalArray<T, ?> _shocks;

    private DoubleArray<?> _states;
    private DoubleArray<?> _controls;

    public PeriodAggregateState(){}

    public PeriodAggregateState(
    		MultiDimensionalArray<T, ?> shocks_,
            DoubleArray<?> states_,
            DoubleArray<?> controls_)
    {
        _shocks = shocks_;
        _states = states_;
        _controls = controls_;
    }
    public DoubleArray<?> getControls()
    {
        return _controls;
    }

    public void setControls(DoubleArray<?> controls_)
    {
        _controls = controls_;
    }

    public MultiDimensionalArray<T, ?> getShocks()
    {
        return _shocks;
    }

    public DoubleArray<?> getStates()
    {
        return _states;
    }

    public void setStates(DoubleArray<?> states_)
    {
        _states = states_;
    }

    public void setShocks(MultiDimensionalArray<T, ?> shocks_)
    {
        _shocks = shocks_;
    }
}
