package com.meliorbis.economics.infrastructure.simulation;

import com.meliorbis.economics.model.Model;
import com.meliorbis.numerics.generic.primitives.DoubleArray;

public interface DiscretisedDistributionSimulator extends Simulator<DiscretisedDistribution, Integer>
{
	/**
	 * Performs a single-period transition from the provided source distribution into the target. The transition 
	 * function is defined by the X-values {@code gridpoints} and the Y-values {@code transitionFn_}, and the individual
	 * transition between states according to {@code exoTransProbs}.
	 * 
	 * @param sourceDist_ The distribution in the initial period
	 * @param targetDist_ The distribution in the next period, which will be filled
	 * @param gridPoints_ The x-values of the transition function
	 * @param transitionFn_ The y-values of the transition function
	 * @param exoTransProbs_ The probabilities of changing between states
	 * @param model_ The model being simulated
	 */
	public void transition(
			final DiscretisedDistribution sourceDist_, 
			final DiscretisedDistribution targetDist_, 
			final DoubleArray<?> gridPoints_,
			final DoubleArray<?> transitionFn_, 
			final DoubleArray<?> exoTransProbs_, 
			final Model<?,?> model_);
}
