/**
 * 
 */
package com.meliorbis.economics.infrastructure.simulation;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.meliorbis.economics.model.State;
import com.meliorbis.economics.model.Model;

/**
 * @author Tobias Grasl
 * 
 * @param <D> The type of Simulation-State to observe
 * @param <T> The numeric type of shocks in the simulation
 */
public final class AggregateSimulationObserver<D extends SimState, T extends Number> implements SimulationObserver<D, T>
{
	final private List<SimulationObserver<D, T>> _observers;

	public AggregateSimulationObserver()
	{
		_observers = Collections.emptyList();
	}
	
	@SafeVarargs
	public AggregateSimulationObserver(SimulationObserver<D, T>... observers_)
	{
		_observers = new ArrayList<SimulationObserver<D, T>>();
		_observers.addAll(Arrays.asList(observers_));
	}
	
	public void addObserver(SimulationObserver<D, T> observer_)
	{
		_observers.add(observer_);
	}
	
	/* (non-Javadoc)
	 * @see com.meliorbis.economics.infrastructure.ISimulationObserver#beingSimulation(com.meliorbis.economics.infrastructure.Simulator.SimState, com.meliorbis.economics.model.ICalcState)
	 */
	@Override
	public void beginSimulation(D initialState_, State<?> calcState_, Model<?,?> model_, int periods_)
	{
		for (SimulationObserver<D, T> observer : _observers)
		{
			observer.beginSimulation(initialState_, calcState_, model_, periods_);
		}
	}

	/* (non-Javadoc)
	 * @see com.meliorbis.economics.infrastructure.ISimulationObserver#periodSimulated(com.meliorbis.economics.infrastructure.Simulator.SimState, com.meliorbis.economics.model.ICalcState, int)
	 */
	@Override
	public void periodSimulated(D currentState_, TransitionRecord<D, T> record_, State<?> calcState_, int period_)
	{
		for (SimulationObserver<D, T> observer : _observers)
		{
			observer.periodSimulated(currentState_, record_, calcState_, period_);
		}
	}

	/* (non-Javadoc)
	 * @see com.meliorbis.economics.infrastructure.ISimulationObserver#endSimulation(com.meliorbis.economics.infrastructure.Simulator.SimState, com.meliorbis.economics.model.ICalcState)
	 */
	@Override
	public void endSimulation(D finalState_, State<?> calcState_)
	{
		for (SimulationObserver<D, T> observer : _observers)
		{
			observer.endSimulation(finalState_, calcState_);
		}
	}

	/* (non-Javadoc)
	 * @see com.meliorbis.economics.infrastructure.ISimulationObserver#wroteSimulation(com.meliorbis.economics.infrastructure.Simulator.SimResults, com.meliorbis.economics.model.ICalcState, java.io.File)
	 */
	@Override
	public void wroteSimulation(SimulationResults<D, ? extends T> results_, State<?> calcState_, File directory_)
	{
		for (SimulationObserver<D, T> observer : _observers)
		{
			observer.wroteSimulation(results_, calcState_, directory_);
		}
	}

}
