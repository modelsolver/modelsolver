/**
 * 
 */
package com.meliorbis.economics.infrastructure.simulation;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.meliorbis.economics.model.State;
import com.meliorbis.economics.model.Model;
import com.meliorbis.numerics.io.NumericsWriter;
import com.meliorbis.numerics.io.NumericsWriterFactory;

/**
 * Saves the simulation state for each period
 * 
 * @author Tobias Grasl
 * 
 * @param <T> The numeric type of shocks in the simulation
 */
public class DensitySavingSimulationObserver<T extends Number> implements SimulationObserver<DiscretisedDistribution, T>
{
	private static final Logger LOG = Logger.getLogger(DensitySavingSimulationObserver.class.getName());
	private final File _simDir;
    private final NumericsWriterFactory _writerFactory;

    public DensitySavingSimulationObserver(File simDir_, NumericsWriterFactory writerFactory_)
	{
		_simDir = simDir_;
        _writerFactory = writerFactory_;
    }

	/* (non-Javadoc)
	 * @see com.meliorbis.economics.infrastructure.ISimulationObserver#beginSimulation(com.meliorbis.economics.infrastructure.Simulator.SimState, com.meliorbis.economics.model.ICalcState, int)
	 */
	@Override
	public void beginSimulation(DiscretisedDistribution initialState_, State<?> calcState_, Model<?,?> model_, int periods_)
	{
		writeState(initialState_, 0);
	}

	private void writeState(SimState state_, int period_)
	{
		File periodDir = new File(_simDir, "_"+Integer.toString(period_));

        NumericsWriter writer = _writerFactory.create(periodDir);

		try
		{
			state_.write(writer);
		} catch (IOException e)
		{
			LOG.log(Level.WARNING, "Unable to save sim state in period "+period_,e);
		}
        finally
        {
            try
            {
                writer.close();
            } catch (IOException e){}
        }

    }

	/* (non-Javadoc)
	 * @see com.meliorbis.economics.infrastructure.ISimulationObserver#periodSimulated(com.meliorbis.economics.infrastructure.Simulator.SimState, com.meliorbis.economics.model.ICalcState, int)
	 */
	@Override
	public void periodSimulated(DiscretisedDistribution currentState_, TransitionRecord<DiscretisedDistribution, T> record_, State<?> calcState_, int period_)
	{
		// We get the output state and the current period, but we save it as the input state for
		// the next period
		writeState(currentState_, period_+1);
	}

	/* (non-Javadoc)
	 * @see com.meliorbis.economics.infrastructure.ISimulationObserver#endSimulation(com.meliorbis.economics.infrastructure.Simulator.SimState, com.meliorbis.economics.model.ICalcState)
	 */
	@Override
	public void endSimulation(DiscretisedDistribution finalState_, State<?> calcState_)
	{

	}

	/* (non-Javadoc)
	 * @see com.meliorbis.economics.infrastructure.ISimulationObserver#wroteSimulation(com.meliorbis.economics.infrastructure.Simulator.SimResults, com.meliorbis.economics.model.ICalcState, java.io.File)
	 */
	@Override
	public void wroteSimulation(SimulationResults<DiscretisedDistribution, ? extends T> results_, State<?> calcState_, File directory_)
	{

	}
}
