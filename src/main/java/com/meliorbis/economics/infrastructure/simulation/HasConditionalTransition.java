/**
 * 
 */
package com.meliorbis.economics.infrastructure.simulation;

import com.meliorbis.economics.model.Model;
import com.meliorbis.economics.model.ModelConfig;
import com.meliorbis.economics.model.State;
import com.meliorbis.numerics.generic.MultiDimensionalArray;
import com.meliorbis.numerics.generic.primitives.DoubleArray;

/**
 * This interface allows models to override the simulation transition to construct
 * a state-specific transition that may depend on both current and future exogenous states
 * 
 * @author Tobias Grasl
 * 
 * @param <C> The config type
 * @param <S> The State type
 */
public interface HasConditionalTransition<C extends ModelConfig, S extends State<C>> extends Model<C, S>
{
	/**
	 * The model should return the individual transition given the current aggregate endogenous states and the current and
	 * future aggregate exogenous states
	 * 
	 * @param distribution_ The simulation state at which to get the individual 
	 * transition
	 * @param state_ The current calculation state
	 * @param aggEndoStates_ The endogenous aggregate states in the current period
	 * @param aggControls_ The aggregate control variables in the current period
	 * @param aggExoStateIndices_ The exogenous aggregate states in the current period
	 * @param futureAggExpStateIndices_ The exogenous aggregate states in the next period
	 * 
	 * @return The appropriate individual transition function
	 * 
	 * @param <N> The numeric type of aggregate shocks in the model
	 */
	<N extends Number> DoubleArray<?> getTransitionAtAggregateState(SimState distribution_, S state_, DoubleArray<?> aggEndoStates_, DoubleArray<?> aggControls_,
            MultiDimensionalArray<N, ?> aggExoStateIndices_,
            MultiDimensionalArray<N, ?> futureAggExpStateIndices_);
}
