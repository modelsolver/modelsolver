package com.meliorbis.economics.infrastructure.simulation;

import com.meliorbis.numerics.generic.MultiDimensionalArray;
import com.meliorbis.numerics.generic.primitives.DoubleArray;

/**
 * Value class for holding the data produced by a simulation transition
 * 
 * @param <S> Simulation-state type
 * @param <T> Numeric type of shocks
 */
public final class TransitionRecord<S extends SimState, T extends Number> extends PeriodAggregateState<T>
{
	DoubleArray<?> _transitionAtAggs;
	S _resultingDist;
	private MultiDimensionalArray<T, ?> _futureShocks;
	private double expectedPopulation = 1;

	public void setTransitionAtAggs(DoubleArray<?> transitionAtAggs_)
	{
		_transitionAtAggs = transitionAtAggs_;
	}

	public void setResultingDist(S resultingDist_)
	{
		_resultingDist = resultingDist_;
	}

	public DoubleArray<?> getTransitionAtAggregates()
	{
		return _transitionAtAggs;
	}

	public S getResultingDistribution()
	{
		return _resultingDist;
	}

	public MultiDimensionalArray<T, ?> getFutureShocks()
	{
		return _futureShocks;
	}

	public void setFutureShocks(MultiDimensionalArray<T, ?> futureShocks_)
	{
		_futureShocks = futureShocks_;
	}

	public double getExpectedPopulation()
	{
		return expectedPopulation;
	}

	public void setExpectedPopulation(double expectedPopulation_)
	{
		expectedPopulation = expectedPopulation_;
	}
	
}