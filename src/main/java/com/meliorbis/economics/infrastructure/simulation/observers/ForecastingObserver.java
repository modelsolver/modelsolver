/**
 * 
 */
package com.meliorbis.economics.infrastructure.simulation.observers;

import static com.meliorbis.numerics.DoubleArrayFactories.createArrayOfSize;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.meliorbis.economics.infrastructure.Base;
import com.meliorbis.economics.infrastructure.simulation.SimState;
import com.meliorbis.economics.infrastructure.simulation.SimulationObserver;
import com.meliorbis.economics.infrastructure.simulation.SimulationResults;
import com.meliorbis.economics.infrastructure.simulation.TransitionRecord;
import com.meliorbis.economics.model.Model;
import com.meliorbis.economics.model.State;
import com.meliorbis.economics.model.StateWithControls;
import com.meliorbis.numerics.function.primitives.DoubleGridFunctionFactory;
import com.meliorbis.numerics.function.primitives.MultiValuedDoubleFunction;
import com.meliorbis.numerics.generic.MultiDimensionalArray;
import com.meliorbis.numerics.generic.impl.IntegerArray;
import com.meliorbis.numerics.generic.primitives.DoubleArray;
import com.meliorbis.numerics.io.NumericsWriter;

/**
 * Creates short-term (1-period) forecasts of states and controls during simulation.
 * 
 * @author Tobias Grasl
 * 
 * @param <S> Simulation-state type
 * @param <N> Numeric type of shocks
 */
public class ForecastingObserver<S extends SimState, N extends Number> extends Base implements SimulationObserver<S, N> 
{
	private static final Logger LOG = Logger.getLogger(ForecastingObserver.class.getName());
	
	MultiValuedDoubleFunction _futureStateForecastFn;
	MultiValuedDoubleFunction _futureControlForecastFn;
	
	DoubleArray<?> _stateForecasts;
	DoubleArray<?> _controlForecasts;
	
	double[] _forecastInputs;
	
	// Need this to be able to extract shock values for discrete shocks
	List<DoubleArray<?>> _aggregateExogenousStates;
	List<DoubleArray<?>> _aggregateNormalisingStates;	
	
	int _numControls;
	int _numStates;
	int _numShocks;
	int _numNormShocks;


	
	@Override
	public void beginSimulation(S initialState_, State<?> calcState_, Model<?, ?> model_, int periods_)
	{
		// The domain of state forecasts is states, controls, shocks
		List<DoubleArray<?>> gridPoints = new ArrayList<>();
		
		_aggregateExogenousStates = model_.getConfig().getAggregateExogenousStates();
		_aggregateNormalisingStates = model_.getConfig().getAggregateNormalisingExogenousStates();
		
		/* Expectation grid dimensions:
		 * 
		 * 1) Current shocks
	     * 2) Future shocks
	     * 3) Future Perm shocks
	     * 4) Current States
	     * 5) Current Controls
	     * 6) n - number of controls
	     */
		gridPoints.addAll(_aggregateExogenousStates);
		gridPoints.addAll(_aggregateExogenousStates);
		gridPoints.addAll(_aggregateNormalisingStates);
		gridPoints.addAll(model_.getConfig().getAggregateEndogenousStates());
		
		_numStates = model_.getConfig().getAggregateEndogenousStateCount();
		_numShocks = model_.getConfig().getAggregateExogenousStateCount();
		_numNormShocks = model_.getConfig().getAggregateNormalisingStateCount();
		_numControls = model_.getConfig().getAggregateControlCount();
		
		// Create an array to hold inputs for forecasting
		_forecastInputs = new double[gridPoints.size()];
				
		// If the model has controls, create a function to forecast those
		// and add controls as inputs to the future state forecasts
		final DoubleGridFunctionFactory fnFactory = new DoubleGridFunctionFactory();
		
		if(calcState_ instanceof StateWithControls<?>)
		{
			// Create the contemporaneous control forecasting function
			_futureControlForecastFn = fnFactory.createFunction(gridPoints, ((StateWithControls<?>)calcState_).getExpectedAggregateControls());
			int[] size_ =
			{ periods_, _numControls };

			_controlForecasts = createArrayOfSize(size_);			
		}
		
		_futureStateForecastFn = fnFactory.createFunction(gridPoints, calcState_.getExpectedAggregateStates());
		int[] size_ =
		{ periods_, _numStates };
		
		_stateForecasts = createArrayOfSize(size_);
		
		
	}
	
	double[] shockInputs(MultiDimensionalArray<N, ?> shocks, MultiDimensionalArray<N, ?> futureShocks_)
	{
		double[] shockValues = new double[2*_numShocks + _numNormShocks];
		
		// If its a double array they are continuous shocks, and they can be used as is
		if(shocks instanceof DoubleArray) {
			// Only copy normal shocks from the current period...
			System.arraycopy(((DoubleArray<?>)shocks).toArray(),0,shockValues,0,_numShocks);
			
			// ...but normal and normalising shocks from the next period
			System.arraycopy(((DoubleArray<?>)futureShocks_).toArray(),0,shockValues,_numShocks,_numShocks+_numNormShocks);
		}
		// Otherwise, they are discrete shocks specifying the level and the actual value needs to be
		// found
		else {			
			for(int i = 0; i < _numShocks; i++)
			{
				int shock = ((IntegerArray)shocks).get(i);
				int futShock = ((IntegerArray)futureShocks_).get(i);
				
				shockValues[i] = _aggregateExogenousStates.get(i).get(shock);
				shockValues[i + _numShocks] = _aggregateExogenousStates.get(i).get(futShock);
			}
			
			for(int i = 0; i < _numNormShocks; i++)
			{
				int futNormShock = ((IntegerArray)futureShocks_).get(_numShocks + i);
				shockValues[i + 2*_numShocks] = _aggregateNormalisingStates.get(i).get(futNormShock);
			}
		}	
		return shockValues;
	}

	@Override
	public synchronized void periodSimulated(S currentState_, TransitionRecord<S, N> record_, State<?> calcState_, int period_)
	{
		// In the last period, no more state forecasting!
		if(period_ == _stateForecasts.size()[0]-1) {
			return;
		}
		
		// In the initial period, copy the initial values into the first position
		if(period_ == 0)
		{
			_stateForecasts.at(0).fill(record_.getStates());
			
			if(_controlForecasts != null)
			{
				_controlForecasts.at(0).fill(record_.getControls());
			}
		}
		
		double[] currentStates = record_.getStates().toArray();
		double[] shocks = shockInputs(record_.getShocks(), record_.getFutureShocks());
		
		
		// Copy the states to the input array
		System.arraycopy(shocks, 0, _forecastInputs, 0, shocks.length);
		System.arraycopy(currentStates, 0, _forecastInputs, shocks.length, currentStates.length);
					
		// Fill next-period forecast states with the values
		_stateForecasts.at(period_ + 1).fill(_futureStateForecastFn.callWithDouble(_forecastInputs));
		
		if(_futureControlForecastFn != null)
		{
			_controlForecasts.at(period_ + 1).fill(_futureControlForecastFn.callWithDouble(_forecastInputs));
		}
	}

	@Override
	public void endSimulation(S finalState_, State<?> calcState_)
	{
		// Ain't nuttin ta do
	}

	@Override
	public void wroteSimulation(SimulationResults<S, ? extends N> results_, State<?> calcState_, File directory_)
	{
		try
		{
			final NumericsWriter writer = getWriterFactory().create(new File(directory_.getParentFile(),"forecast1P.mat"));
			writer.writeArray("states", _stateForecasts);
			writer.writeArray("controls", _controlForecasts);
			writer.close();
		} catch (IOException e)
		{
			LOG.warning(String.format("Exception writing short-term forecasts: %s", e.getMessage()));
		}
	}
}
