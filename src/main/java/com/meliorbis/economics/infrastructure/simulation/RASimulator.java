/**
 * 
 */
package com.meliorbis.economics.infrastructure.simulation;

import static com.meliorbis.numerics.DoubleArrayFactories.createArrayOfSize;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import com.meliorbis.economics.model.Model;
import com.meliorbis.economics.model.ModelConfig;
import com.meliorbis.economics.model.ModelException;
import com.meliorbis.economics.model.ModelWithControls;
import com.meliorbis.economics.model.State;
import com.meliorbis.economics.model.StateWithControls;
import com.meliorbis.numerics.function.primitives.DoubleGridFunctionFactory;
import com.meliorbis.numerics.function.primitives.MultiValuedDoubleFunction;
import com.meliorbis.numerics.generic.MultiDimensionalArray;
import com.meliorbis.numerics.generic.primitives.DoubleArray;
import com.meliorbis.numerics.io.NumericsWriterFactory;
import com.meliorbis.utils.Utils;

/**
 * A Simulator for simulating Representative Agent models with continuous shocks
 * 
 * @author Tobias Grasl
 */
public class RASimulator extends AbstractSimulator<RepresentativeAgentSimState, Double>
{

	public RASimulator(NumericsWriterFactory writerFactory_,
			AggregateSimulationObserver<RepresentativeAgentSimState, Double> observer_)
	{
		super(writerFactory_, observer_);
	}

	public RASimulator(NumericsWriterFactory writerFactory_)
	{
		super(writerFactory_);
	}

	/* (non-Javadoc)
	 * @see com.meliorbis.economics.infrastructure.ISimulator#createShockSequence(com.meliorbis.numerics.generic.IMultiDimensionalArray, int, com.meliorbis.economics.model.IModel)
	 */
	@Override
	public MultiDimensionalArray<Double, ?> createShockSequence(MultiDimensionalArray<Double, ?> initialShockStates_, int periods_,
			Model<?, ?> model_)
	{
		DoubleArray<?> exoStateTransition = model_.getConfig().getExogenousStateTransition();

		int[] indShockIndex = Utils.repeatArray(-1, exoStateTransition.numberOfDimensions());

		// Just select the first individual shock state as a source
		for (int i = 0; i < model_.getConfig().getIndividualExogenousStateCount(); i++)
		{
			indShockIndex[i] = 0;
		}

		/*
		 * Take the sum over all possible individual future states at each
		 * aggregate transition point; this will give us the aggregate
		 * transition probability
		 */
		final int nAggExoStates = model_.getConfig().getAggregateExogenousStateCount();
		final int nAggNormStates = model_.getConfig().getAggregateNormalisingStateCount();
		final int nIndExoStates = model_.getConfig().getIndividualExogenousStateCount();

		DoubleArray<?> aggTransition = exoStateTransition.at(indShockIndex).across(Utils.sequence(nAggExoStates, nAggExoStates + nIndExoStates))
				.sum();

		Random random;

		random = new Random();
		int[] size_ =
		{ periods_, nAggExoStates + nAggNormStates };
		
		DoubleArray<?> allShocks = createArrayOfSize(size_);

		int[] currentAggShock = new int[] {11,0};

		int[] allInitShocks = new int[nAggExoStates + nAggNormStates];

		System.arraycopy(currentAggShock, 0, allInitShocks, 0, nAggExoStates);

		allShocks.at(0).fill(1d,1d);

		int shockPeriod = 1;

		while (shockPeriod < periods_)
		{
			/*
			 * Draw the next values for aggregate shocks at random
			 */
			int[] futureAggShock = Utils.drawRandomState(aggTransition.at(currentAggShock[0]), random);

			// TODO: Fix, only works for single shock
			allShocks.set(model_.getConfig().getAggregateExogenousStates().get(0).get(futureAggShock[0]),shockPeriod,0);

			currentAggShock = /* copy only the transient shocks */Arrays.copyOf(futureAggShock, currentAggShock.length);
			shockPeriod++;
		}
		// allShocks.fillDimensions(ArrayUtils.toObject(allInitShocks), 1);
		return allShocks;
	}

	/* (non-Javadoc)
	 * @see com.meliorbis.economics.infrastructure.ISimulator#createShockSequence(com.meliorbis.numerics.generic.IMultiDimensionalArray, int, com.meliorbis.economics.model.IModel, java.lang.Number)
	 */
	@Override
	public MultiDimensionalArray<Double, ?> createShockSequence(MultiDimensionalArray<Double, ?> initialShockStates_, int periods_,
			Model<?, ?> model_, Double seed_)
	{
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.meliorbis.economics.infrastructure.ISimulator#simAggregate(com.meliorbis.numerics.generic.IMultiDimensionalArray, com.meliorbis.economics.infrastructure.simulation.PeriodAggregateState, com.meliorbis.economics.model.IModel, com.meliorbis.economics.individual.IIndividualProblemState, java.io.File, java.lang.String)
	 */
	@Override
	public <S extends State<?>> SimulationResults<RepresentativeAgentSimState, Double> simAggregate(MultiDimensionalArray<Double, ?> shocks_,
			PeriodAggregateState<Double> initialState_, Model<?, S> model_, S state_, File outDir_, String resultsPath_) throws ModelException,
			SimulatorException
	{
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.meliorbis.economics.infrastructure.ISimulator#simAggregatesForecastingControls(com.meliorbis.numerics.generic.IMultiDimensionalArray, com.meliorbis.economics.infrastructure.simulation.PeriodAggregateState, com.meliorbis.economics.model.IModelWithControls, com.meliorbis.economics.model.IStateWithControls, java.io.File, java.lang.String)
	 */
	@Override
	public <C extends ModelConfig, SC extends StateWithControls<C>, MC extends ModelWithControls<C, SC>> SimulationResults<RepresentativeAgentSimState, Double> simAggregatesForecastingControls(
			MultiDimensionalArray<Double, ?> shocks_, PeriodAggregateState<Double> initialState_, MC model_, SC state_, File outDir_,
			String resultsPath_) throws ModelException, SimulatorException
	{
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.meliorbis.economics.infrastructure.ISimulator#simulate(int, int, com.meliorbis.economics.infrastructure.simulation.ISimState, com.meliorbis.numerics.generic.IMultiDimensionalArray, com.meliorbis.economics.model.IModel, com.meliorbis.economics.individual.IIndividualProblemState, com.meliorbis.economics.infrastructure.ISimulationObserver, java.io.File, java.lang.String)
	 */
	@Override
	public <S extends State<?>, M extends Model<?, S>> SimulationResults<RepresentativeAgentSimState, Double> simulate(int periods_, int burnIn_,
			RepresentativeAgentSimState initialState_, MultiDimensionalArray<Double, ?> initialShockStates_, M model_, S calcState_,
			SimulationObserver<RepresentativeAgentSimState, Double> observer_, File outputDir_, String resultsPath_) throws SimulatorException, ModelException
	{
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.meliorbis.economics.infrastructure.ISimulator#simulate(int, int, com.meliorbis.economics.infrastructure.simulation.ISimState, com.meliorbis.numerics.generic.IMultiDimensionalArray, com.meliorbis.economics.model.IModel, com.meliorbis.economics.individual.IIndividualProblemState, com.meliorbis.economics.infrastructure.ISimulationObserver)
	 */
	@Override
	public <S extends State<?>, M extends Model<?, S>> SimulationResults<RepresentativeAgentSimState, Double> simulate(int periods_, int burnIn_,
			RepresentativeAgentSimState initialState_, MultiDimensionalArray<Double, ?> initialShockStates_, M model_, S calcState_,
			SimulationObserver<RepresentativeAgentSimState, Double> observer_) throws SimulatorException, ModelException
	{
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.meliorbis.economics.infrastructure.ISimulator#simulateTransition(com.meliorbis.economics.infrastructure.simulation.ISimState, com.meliorbis.economics.model.IModel, com.meliorbis.economics.individual.IIndividualProblemState, com.meliorbis.numerics.generic.IMultiDimensionalArray, com.meliorbis.numerics.generic.IMultiDimensionalArray)
	 */
	@Override
	public <S extends State<?>, M extends Model<?, S>> TransitionRecord<RepresentativeAgentSimState, Double> simulateTransition(
			RepresentativeAgentSimState distribution_, M model_, S calcState_, MultiDimensionalArray<Double, ?> priorAggShockIndices_,
			MultiDimensionalArray<Double, ?> futureShocks_) throws ModelException
	{
		DoubleArray<?> individualPolicy = calcState_.getIndividualPolicy();
		
		List<DoubleArray<?>> gridArrays = new ArrayList<DoubleArray<?>>();
		gridArrays.addAll(model_.getConfig().getIndividualEndogenousStates());
		gridArrays.addAll(model_.getConfig().getIndividualExogenousStates());
		gridArrays.addAll(model_.getConfig().getAggregateEndogenousStates());
		gridArrays.addAll(model_.getConfig().getAggregateExogenousStates());
		gridArrays.addAll(model_.getConfig().getAggregateNormalisingExogenousStates());
		
		MultiValuedDoubleFunction policyFunction = new DoubleGridFunctionFactory().createFunction(gridArrays, individualPolicy);
		
		double[] inputs = Utils.repeatArray(1d,model_.getConfig().getIndividualEndogenousStateCount()+
		                             model_.getConfig().getIndividualExogenousStateCount()+
		                             model_.getConfig().getAggregateEndogenousStateCount()+
		                             model_.getConfig().getAggregateExogenousStateCount()+
		                             model_.getConfig().getAggregateNormalisingStateCount());
		
		// Copy the individual states
		System.arraycopy(distribution_.getStates().toArray(), 0, inputs, 0, model_.getConfig().getIndividualEndogenousStateCount());
		
		// Copy the individual states to the aggregate states also 
		System.arraycopy(distribution_.getStates().toArray(), 0, inputs, model_.getConfig().getIndividualEndogenousStateCount()+
                model_.getConfig().getIndividualExogenousStateCount(), model_.getConfig().getIndividualEndogenousStateCount());
				
		// Copy the aggregate shocks
		System.arraycopy(((DoubleArray<?>)priorAggShockIndices_).toArray(), 0, inputs, model_.getConfig().getIndividualEndogenousStateCount()+
                model_.getConfig().getIndividualExogenousStateCount()+
                model_.getConfig().getAggregateEndogenousStateCount(), model_.getConfig().getAggregateExogenousStateCount() + model_.getConfig().getAggregateNormalisingStateCount());

		// Evaluate the policy function at that point
		DoubleArray<?> nextPeriodStates = policyFunction.callWithDouble(inputs);
		
		TransitionRecord<RepresentativeAgentSimState, Double> record = new TransitionRecord<RepresentativeAgentSimState, Double>();
		
		record.setStates(distribution_.getStates());
		record.setShocks(priorAggShockIndices_);
		record.setFutureShocks(futureShocks_);
		
		record.setResultingDist(new RepresentativeAgentSimState(nextPeriodStates));
		
		return record;
	}

	/* (non-Javadoc)
	 * @see com.meliorbis.economics.infrastructure.ISimulator#createState()
	 */
	@Override
	public RepresentativeAgentSimState createState()
	{
		return new RepresentativeAgentSimState();
	}

	/* (non-Javadoc)
	 * @see com.meliorbis.economics.infrastructure.ISimulator#findErgodicDist(com.meliorbis.economics.model.IModel, com.meliorbis.economics.individual.IIndividualProblemState, com.meliorbis.economics.infrastructure.simulation.ISimState)
	 */
	@Override
	public <S extends State<?>, M extends Model<?, S>> RepresentativeAgentSimState findErgodicDist(M model_, S state_,
			RepresentativeAgentSimState simState_) throws ModelException, SimulatorException
	{
		throw new UnsupportedOperationException();
	}

}
