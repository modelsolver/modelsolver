package com.meliorbis.economics.infrastructure.simulation;

import java.io.File;
import java.io.IOException;

import com.meliorbis.numerics.generic.primitives.DoubleArray;
import com.meliorbis.numerics.io.NumericsReader;
import com.meliorbis.numerics.io.NumericsWriter;

/**
 * The state of a simulation in a given period
 * 
 * @author Tobias Grasl
 */
public interface SimState
{
	public double mean(DoubleArray<?> levels_);

	public double variance(DoubleArray<?> levels_);

	public double variance(DoubleArray<?> levels_, double mean_);

	/**
	 * Writes the arrays that make up the distribution into the provided writer. If name_ is null or empty, they
	 * will be written to the base path of the writer, otherwise they will be written as a structure with the name
	 * provided
	 *
	 * @param writer_ The writer to write to
	 * @param name_ The name of the structure to write to, or null for direct writing to the base path
	 *
	 * @throws IOException In the event of failure
	 */
	public void write(NumericsWriter writer_, String name_) throws IOException;

	/**
	 * Writes the arrays that make up the distribution into the provided writer. The files are written to the base
	 * path of the writer
	 *
	 * @param writer_ The writer to write to
	 *
	 * @throws IOException In the event of failure
	 */
	public void write(NumericsWriter writer_) throws IOException;

	/**
	 * Reads the arrays that make up the distribution from the provided reader. If name_ is null or empty, they
	 * will be read from the base path of the reader, otherwise they will be read as a structure with the name
	 * provided
	 *
	 * @param reader_ The reader to read from
	 * @param name_ The name of the structure to read, or null for direct reading of arrays from the base path
	 *
	 * @throws IOException In the event of failure
	 */
	public void read(NumericsReader reader_, String name_) throws IOException;

	/**
	 * Reads the arrays that make up this distribution from the base path of the provided reader
	 *
	 * @param file_ The path to read from
	 *
	 * @throws IOException In the event of failure
	 */
	public void read(File file_) throws IOException;

}