package com.meliorbis.economics.infrastructure.simulation;

import java.io.File;

import com.meliorbis.economics.model.State;
import com.meliorbis.economics.model.Model;

/**
 * An interface that can be used to observe the stages of simulation
 * 
 * @author Tobias Grasl
 * 
 * @param <D> The Simulation-state Type
 * @param <T> The numeric type of shocks
 */
public interface SimulationObserver<D extends SimState, T extends Number>
{
	/**
	 * Called when the simulation begins
	 * 
	 * @param initialState_ The initial state that starts the simulation
	 * @param calcState_ The model state being simulated
	 * @param model_ The model being simulated
	 * @param periods_ The number of periods this simulation will have
	 */
	public void beginSimulation(D initialState_, State<?> calcState_, Model<?,?> model_, int periods_);
	
	/**
	 * Called when a period has been completed
	 * 
     * @param currentState_ The sim state arrived at
     * @param record_ The record if the last transition (period)
     * @param calcState_ The model state being simulated
     * @param period_ The period that has just been completed
     */
	public void periodSimulated(D currentState_, TransitionRecord<D, T> record_, State<?> calcState_, int period_);
	
	/**
	 * Called when the simulation has completed
	 * 
	 * @param finalState_ The final state of the simulation
	 * @param calcState_ The model state being simulated
	 */
	public void endSimulation(D finalState_, State<?> calcState_);
	
	/**
	 * Called when the simulation has been written
	 * 
	 * @param results_ The results of the simulation
	 * @param calcState_ The model state being simulated
	 * @param directory_ The directory in which the state was written
	 */
	public void wroteSimulation(SimulationResults<D, ? extends T> results_, State<?> calcState_, File directory_);
	
	
	public static <F extends SimState, V extends Number> SimulationObserver<F, V> silent()
    {
        return new SimulationObserver<F, V>()
        {
            @Override
            public void wroteSimulation(SimulationResults<F, ? extends V> results_, State<?> calcState_, File directory_)
            {
            }

            @Override
            public void periodSimulated(F currentState_, TransitionRecord<F, V> record_, State<?> calcState_, int period_)
            {
            }

            @Override
            public void endSimulation(F finalState_, State<?> calcState_)
            {
            }

            @Override
            public void beginSimulation(F initialState_, State<?> calcState_, Model<?,?> model_, int periods_)
            {
            }
        };
    }
	
}
