/**
 * 
 */
package com.meliorbis.economics.infrastructure;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.meliorbis.economics.model.ModelConfig;
import com.meliorbis.economics.model.State;
import com.meliorbis.economics.model.StateWithControls;
import com.meliorbis.numerics.convergence.Criterion;
import com.meliorbis.numerics.convergence.DoubleCriterion;
import com.meliorbis.numerics.convergence.MaxCriterion;
import com.meliorbis.numerics.generic.MultiDimensionalArray;
import com.meliorbis.numerics.generic.primitives.DoubleArray;
import com.meliorbis.numerics.io.NumericsWriter;

/**
 * Base class with common functionality for states, with some methods only to be
 * accessed by GridSolver
 * 
 * @author Tobias Grasl
 * 
 * @param <C> The Config type
 */
public abstract class AbstractStateBase<C extends ModelConfig> implements State<C>
{
	private int _solutionPeriod = 0;
	private DoubleArray<?> _expectedIndividualTransition;
	private DoubleArray<?> _capitalGrid;
	private DoubleArray<?> _individualTransition;
	protected DoubleArray<?> _individualTransitionForSimulation;
	private DoubleArray<?> _aggregateStateTransition;
	private DoubleArray<?> _expectedAggregateStates;
	private DoubleArray<?> _normalizedSOPStates;

	// The model this state is a state for
	private C _config;
	private DoubleArray<?> _expectedAggregateControls;
	protected DoubleArray<?> _simSOPStates;

	private Criterion _individualCriterion;
	private DoubleCriterion _aggregateCriterion;
	
	/**
	 * @param config_ The config being used for the calculation
	 */
	protected AbstractStateBase(C config_)
	{
		_config = config_;
	}

	/**
	 * @return The model being calculated which this state is for
	 */
	@Override
	final public C getConfig()
	{
		return _config;
	}

	@Override
	public void incrementPeriod()
	{
		_solutionPeriod++;
	}

	@Override
	public int getPeriod()
	{
		return _solutionPeriod;
	}

	/**
	 * This default implementation assumes that the simulation uses the same
	 * grid as the solution
	 */
	@Override
	public DoubleArray<?> getIndividualPolicyForSimulation()
	{
		if (_individualTransitionForSimulation == null)
		{
			return getIndividualPolicy();
		}

		return _individualTransitionForSimulation;
	}

	/**
	 * This default implementation assumes that the simulation uses the same
	 * grid as the solution
	 */
	@Override
	public DoubleArray<?> getEndOfPeriodStatesForSimulation()
	{
		return getEndOfPeriodStates();
	}

	@Override
	public void setExpectedIndividualTransition(DoubleArray<?> val_)
	{
		_expectedIndividualTransition = val_;
	}

	@Override
	public DoubleArray<?> getExpectedIndividualTransition()
	{
		return _expectedIndividualTransition;
	}

	public void setEndOfPeriodStates(DoubleArray<?> capitalGrid_)
	{
		_capitalGrid = capitalGrid_;
	}

	@Override
	public DoubleArray<?> getEndOfPeriodStates()
	{
		return _capitalGrid;
	}

	@Override
	public final DoubleArray<?> getIndividualPolicy()
	{
		return _individualTransition;
	}

	@Override
	final public void setIndividualPolicy(DoubleArray<?> newIndividualTransition_)
	{
		_individualTransitionForSimulation = null;

		_individualTransition = newIndividualTransition_;
	}

	@Override
	public void setAggregateTransition(DoubleArray<?> newVals_)
	{
		_aggregateStateTransition = newVals_;
	}

	@Override
	public DoubleArray<?> getAggregateTransition()
	{
		return _aggregateStateTransition;
	}

	@Override
	public void setExpectedAggregateStates(DoubleArray<?> expectedStates_)
	{
		_expectedAggregateStates = expectedStates_;
	}

	@Override
	public DoubleArray<?> getExpectedAggregateStates()
	{
		return _expectedAggregateStates;
	}

	public void setNormalisedStartOfPeriodStates(DoubleArray<?> newVals_)
	{
		_normalizedSOPStates = newVals_;
	}

	public void setStartOfPeriodStatesForSimulation(DoubleArray<?> newVals_)
	{
		_simSOPStates = newVals_;
	}

	@Override
	public DoubleArray<?> getNormalisedStartOfPeriodStates()
	{
		return _normalizedSOPStates;
	}

	@Override
	public DoubleArray<?> getStartOfPeriodStatesForSimulation()
	{
		return _simSOPStates;
	}
	
	

	@Override
	public void setNamedArrays(Map<String, DoubleArray<?>> arraysByName_)
	{
		// Default does nothing
	}

	public DoubleArray<?> getExpectedAggregateControls()
	{
		return _expectedAggregateControls;
	}

	public void setExpectedAggregateControls(DoubleArray<?> newExpectedControls_)
	{
		_expectedAggregateControls = newExpectedControls_;
	}

	@SuppressWarnings("unchecked")
	public void write(NumericsWriter writer_)
	{
		try
		{
			// Model Inputs
			writer_.writeArray("transitionProbs", this.getConfig().getExogenousStateTransition());
			writeStructureFromList("aggStates", this.getConfig().getAggregateEndogenousStates(), "X", writer_);
			writeStructureFromList("indStates", this.getConfig().getIndividualEndogenousStates(), "x", writer_);
			writeStructureFromList("indStatesSim", this.getConfig().getIndividualEndogenousStatesForSimulation(), "x", writer_);

			if (this.getConfig().getAggregateControlCount() > 0)
			{
				writeStructureFromList("aggControls", _config.getAggregateControls(), "C", writer_);

				writer_.writeArray("aggExpControls", ((StateWithControls<C>) this).getExpectedAggregateControls());

			}

			writeStructureFromList("aggShocks", this.getConfig().getAggregateExogenousStates(), "Z", writer_);
			writeStructureFromList("aggPersistentShocks", this.getConfig().getAggregateNormalisingExogenousStates(), "Zp", writer_);
			writeStructureFromList("indShocks", this.getConfig().getIndividualExogenousStates(), "z", writer_);

			// Actual calculation state
			writer_.writeArray("aggTransition", this.getAggregateTransition());
			writer_.writeArray("indTransition", this.getIndividualPolicy());
			writer_.writeArray("indTransitionSim", this.getIndividualPolicyForSimulation());

			getConfig().writeParameters(writer_);

		} catch (IOException e)
		{
			// LOG.log(Level.SEVERE, "Error writing state", e);
		} finally
		{
		}
	}

	private void writeStructureFromList(String structName_, List<DoubleArray<?>> listOfArrays_, String prefix_, NumericsWriter writer_)
			throws IOException
	{
		// If the array is empty or null, there is nothing to do (lucky us!)
		if (listOfArrays_ == null || listOfArrays_.size() == 0)
		{
			return;
		}

		Map<String, MultiDimensionalArray<? extends Number, ?>> results = new HashMap<String, MultiDimensionalArray<? extends Number, ?>>();

		for (int i = 0; i < listOfArrays_.size();/* increment in block! */)
		{
			final DoubleArray<?> currentValues = listOfArrays_.get(i);

			// pre-increment because MATLAB users are used to 1-based arrays, so
			// we don't want to confuse them!
			results.put(String.format("%s_%s", prefix_, ++i), currentValues);
		}

		// Write the structure to the provided writer
		writer_.writeStructure(structName_, results);
	}
	
	

	@Override
	public void setIndividualError(double errorValue_)
	{
		_individualCriterion = new DoubleCriterion("Ind", errorValue_);
	}

	@Override
	public Criterion getIndividualCriterion()
	{
		return _individualCriterion;
	}

	@Override
	public void setAggregateError(double errorValue_)
	{
		_aggregateCriterion = new DoubleCriterion("Agg", errorValue_);
	}

	@Override
	public Criterion getAggregateCriterion()
	{
		return _aggregateCriterion;
	}

	/**
	 * This default implementation takes either the individual or aggregate criterion, depending on which
	 * has been set, or the max of the two if they are both defined
	 * 
	 * @return The criterion.
	 */
	@Override
	public Criterion getConvergenceCriterion()
	{
		Criterion indCrit = getIndividualCriterion();
		Criterion aggCrit = getAggregateCriterion();
		
		if(indCrit != null) {
			if(aggCrit != null) {
				return new MaxCriterion(indCrit, aggCrit);
			}
			else {
				return indCrit;
			}
		} else if (aggCrit != null) {
			// Assumes this is not null!
			return aggCrit;
		}
		
		return Criterion.INITIAL;
	}

}
