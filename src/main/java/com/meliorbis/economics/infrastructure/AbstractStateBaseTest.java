package com.meliorbis.economics.infrastructure;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

import com.meliorbis.economics.model.ModelConfig;

public class AbstractStateBaseTest
{
	private static class StateImpl extends AbstractStateBase<ModelConfig>
	{

		protected StateImpl(ModelConfig config_)
		{
			super(config_);
		}		
	}
	@Before
	public void setUp() throws Exception
	{
	}

	@Test
	public void test()
	{
		StateImpl stateImpl = new StateImpl(new ModelConfigForTests());
		
		// Should return INITIAL, but something
		assertNotNull(stateImpl.getConvergenceCriterion());
		
		stateImpl.setIndividualError(3d);
		
		// Returns just individual
		assertEquals(3d, stateImpl.getConvergenceCriterion().getValue(), 1e-10);
		
		stateImpl.setAggregateError(2d);
		
		// Should return max, so 2 has no effect
		assertEquals(3d, stateImpl.getConvergenceCriterion().getValue(), 1e-10);
		
		stateImpl.setAggregateError(4d);
		
		// should return max, so 4 has effect
		assertEquals(4d, stateImpl.getConvergenceCriterion().getValue(), 1e-10);

		stateImpl = new StateImpl(new ModelConfigForTests());
		
		stateImpl.setAggregateError(3d);
		
		// Returns just aggregate
		assertEquals(3d, stateImpl.getConvergenceCriterion().getValue(), 1e-10);
		
		stateImpl.setIndividualError(2d);
		
		// Should return max, so 2 has no effect
		assertEquals(3d, stateImpl.getConvergenceCriterion().getValue(), 1e-10);
		
		stateImpl.setIndividualError(4d);
		
		// should return max, so 4 has effect
		assertEquals(4d, stateImpl.getConvergenceCriterion().getValue(), 1e-10);

		stateImpl = new StateImpl(new ModelConfigForTests());
	}

}
