/**
 * 
 */
package com.meliorbis.economics.aggregate.transformedDist;

import static com.meliorbis.numerics.DoubleArrayFactories.createArrayOfSize;

import java.util.ArrayList;
import java.util.List;

import com.meliorbis.economics.aggregate.ByShockLevelAggregateSolver;
import com.meliorbis.economics.aggregate.derivagg.DerivAggCalcState;
import com.meliorbis.economics.infrastructure.simulation.DiscretisedDistribution;
import com.meliorbis.economics.infrastructure.simulation.DiscretisedDistributionSimulator;
import com.meliorbis.economics.infrastructure.simulation.SimulationObserver;
import com.meliorbis.economics.infrastructure.simulation.SimulationResults;
import com.meliorbis.economics.model.Model;
import com.meliorbis.economics.model.ModelConfig;
import com.meliorbis.economics.model.ModelException;
import com.meliorbis.numerics.IntArrayFactories;
import com.meliorbis.numerics.generic.IntegerArray;
import com.meliorbis.numerics.generic.primitives.DoubleArray;
import com.meliorbis.numerics.index.impl.Index;
import com.meliorbis.numerics.index.impl.Index.SubSpaceSplitIterator;
import com.meliorbis.utils.Pair;
import com.meliorbis.utils.Utils;

/**
 * Determines the forecasting function by adjusting the distribution to each of 
 * the aggregate grid points in turn and updating the forecast for that point 
 * using that transformed distribution.
 * 
 * @author Tobias Grasl
 * 
 * @param <C> The Config type
 * @param <S> The State type
 * @param <M> The Model type
 */
public abstract class TransformedDistributionSolver<
			C extends ModelConfig, 
			S extends DerivAggCalcState<C>, M extends Model<C, S>> 
								extends ByShockLevelAggregateSolver<C, S, M>
{
	private DoubleArray<?> _aggVariables;
	final protected DiscretisedDistributionSimulator _simulator;

	public TransformedDistributionSolver(M model_, C config_, DiscretisedDistributionSimulator simulator_)
	{
		super(model_, config_);
		_simulator = simulator_;
	}
	
	
	@Override
	public void initialise(S state_)
	{
		super.initialise(state_);

		/* Create an array to hold endogenous aggregate values on the grid
		 */
		List<Integer> aggEndoSizes = new ArrayList<Integer>();
		Utils.addLengthsToList(aggEndoSizes, _config.getAggregateEndogenousStates());
		aggEndoSizes.add(_config.getAggregateEndogenousStateCount());
		
		_aggVariables = createArrayOfSize(aggEndoSizes);
		
		/* Fill the array with the endogenous aggregate grid values
		 */
		for(int state = 0; state < _config.getAggregateEndogenousStateCount(); state++) 
		{
			_aggVariables.lastDimSlice(state).fillDimensions(_config.getAggregateEndogenousStates().get(state),state);
		}
	}

	@Override
	protected Pair<DoubleArray<?>, DoubleArray<?>> calculateTransitionForShocks(int[] currentShockIndexes_, S state_) throws ModelException
	{
		// Get the distribution to use for these shock levels...
		DiscretisedDistribution distribution = state_.getDistributionForState(currentShockIndexes_);
		
		// ...and calculate the endogenous aggregates under that distribution
		IntegerArray<?> shocksArray = IntArrayFactories.createIntArray(currentShockIndexes_);
		double[] endoAggs = _model.calculateAggregateStates(distribution, shocksArray, state_);
		
		// Iterate over all except the last dimension
		SubSpaceSplitIterator iterator = new Index(_aggVariables.size()).iterator(Utils.sequence(0, _aggVariables.numberOfDimensions()-1));
		
		DoubleArray<?> targetArray = createArrayOfSize(_aggVariables.size());
		
		while(iterator.hasNext()) {
			iterator.nextInt();
			
			int[] currentFullIndex = iterator.getCurrentFullIndex();
			DoubleArray<?> targetAggs = _aggVariables.at(currentFullIndex);
			
			DiscretisedDistribution adjusted = adjustDistributionToAggregates(distribution, endoAggs, targetAggs.toArray());
			int[] size_ =
			{ 2, 2 };

			IntegerArray<?> simShocksArray = IntArrayFactories.createIntArrayOfSize(size_);
			simShocksArray.set(currentShockIndexes_[0], 0,0);
			simShocksArray.set(currentShockIndexes_[0], 1,0);
					
			
			SimulationResults<DiscretisedDistribution, Integer> simulationResults = 
					_simulator.
						simulateShocks(
								adjusted, 
								simShocksArray, 
								_model, 
								state_, 
								SimulationObserver.silent());
			
			double[] transformedAggs = _model.calculateAggregateStates(simulationResults.getFinalState(), shocksArray, state_);
			
			targetArray.at(currentFullIndex).fill(transformedAggs);
		}
		
		return new Pair<DoubleArray<?>, DoubleArray<?>>(targetArray,null);
	}

	abstract protected DiscretisedDistribution adjustDistributionToAggregates(DiscretisedDistribution distribution, double[] aggsForDist_, double[] targetAggregateStates_);
}
