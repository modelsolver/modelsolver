/**
 * 
 */
package com.meliorbis.economics.aggregate;

import com.meliorbis.economics.model.Model;
import com.meliorbis.economics.model.ModelConfig;
import com.meliorbis.economics.model.State;
import com.meliorbis.numerics.generic.primitives.DoubleArray;
import com.meliorbis.utils.Pair;

/**
 * Aggregate solver used for solving representative agent problems, which picks the value at the same
 * point in the ind policy as the aggregate policy for each state level 
 * 
 *  @author Tobias Grasl
 *  
 *  @param <C> The Config type
 *  @param <S> The State type
 *  @param <M> The Model type
 */
public class RAAggregateSolver<C extends ModelConfig, S extends State<C>, M extends Model<C, S>> extends AggregateSolverBase<C, S, M> implements AggregateProblemSolver<S>
{

	/**
	 * @param model_ The model being solved
	 * @param config_ The configuration to solve the model under
	 */
	public RAAggregateSolver(M model_, C config_)
	{
		super(model_, config_);
	}

	/* (non-Javadoc)
	 * @see com.meliorbis.economics.aggregate.AggregateSolverBase#calculateAggregatePolicies(com.meliorbis.economics.individual.IIndividualProblemState)
	 */
	@Override
	protected Pair<DoubleArray<?>, DoubleArray<?>> calculateAggregatePolicies(S state_)
	{
//		IDoubleArray<?> statesPolicy = _model.createAggregateVariableGrid(_config.getAggregateEndogenousStateCount());
//		
//		IMultiValuedDoubleFunction individualPolicyFunction = state_.getIndividualPolicy();
//		
//		double[] valsArray = Utils.repeatArray(1d,((DoubleGridFunction)individualPolicyFunction).getValues().numberOfDimensions()-1);
//		
//		// Iterate over all the dimensions but the highest, which is the one containing future values
//		IDoubleSubspaceSplitIterator aggValuesIterator = statesPolicy.iteratorAcross(Utils.sequence(0,statesPolicy.numberOfDimensions() - 1));
//
//		// At each point...
//		while(aggValuesIterator.hasNext()) {
//			
//			aggValuesIterator.nextDouble();
//			
//			int[] currentAggIndex = aggValuesIterator.getIndex();
//
//			// First deal with aggregate endogenous states...
//			for(int i = 0; i < _config.getAggregateEndogenousStateCount(); i++) {
//				// Set the individual state to the value of the aggregate state
//				double aggStateValue = _config.getAggregateEndogenousStates().get(i).get(currentAggIndex[i]);
//				
//				valsArray[i] = valsArray[i + _model.getAggDetStateStart()] = aggStateValue;
//			}
//			
//			// Then deal with aggregate exogenous states
//			for(int i = 0; i < _config.getAggregateExogenousStateCount(); i++) {
//				// Set the aggregate shock to the relevant value
//				valsArray[i + _model.getAggDetStateStart() + _config.getAggregateEndogenousStateCount()] = _config.getAggregateExogenousStates().get(i).get(currentAggIndex[i + _config.getAggregateEndogenousStateCount()]);
//			}
//			
//			// Call the function with the values that have been set up
//			IDoubleArray<?> results = individualPolicyFunction.callWithDouble(valsArray);
//			
//			// Store teh results in the aggregate policy
//			statesPolicy.at(aggValuesIterator.getFullIndex()).fill(results);
//		}
//		
//		// Assumes no controls at present
//		return new Pair<IDoubleArray<?>, IDoubleArray<?>>(statesPolicy, null);
		throw new RuntimeException("Not yet Implemented");
	}

}
