/**
 * 
 */
package com.meliorbis.economics.aggregate.derivagg;

import com.meliorbis.economics.infrastructure.simulation.DiscretisedDistribution;
import com.meliorbis.economics.model.ModelConfig;
import com.meliorbis.economics.model.State;
import com.meliorbis.numerics.generic.primitives.DoubleArray;


/**
 * A calculation state that holds the necessary values to perform derivative aggregation
 * 
 * @author Tobias Grasl
 * 
 * @param <C> The Config type
 */
public interface DerivAggCalcState<C extends ModelConfig> extends State<C>
{
	/**
	 * @param shockLevels_ The indexes in the discrete shock ranges being processed, and which a distribution
	 * is required for
	 *
	 * @return The distribution to use as a starting point, given the shock levels, to determine the aggregate 
	 * forecast rule
	 */
	DiscretisedDistribution getDistributionForState(int[] shockLevels_);
	
	/**
	 * Used by the Derivative Aggregation Solver, sets the derivatives calculated
	 * by the solver.
	 * 
	 * Default does nothing
	 * 
	 * @param exoStates_ The exogenous state grid index these derivatives are for
	 * @param currentAggs_ The aggregate states under the distribution for which the derivation was performed
	 * @param futureAggs_ The future aggregate states under the distribution for which the derivation was performed
	 * @param derivatives_ The derivatives - one array for each order
	 */
	default void setDerivatives( int[] exoStates_, DoubleArray<?> currentAggs_, DoubleArray<?> futureAggs_, DoubleArray<?>... derivatives_) {};
}
