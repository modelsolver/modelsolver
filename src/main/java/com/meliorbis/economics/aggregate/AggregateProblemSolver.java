/**
 * 
 */
package com.meliorbis.economics.aggregate;

import com.meliorbis.economics.model.State;

/**
 * Interface for classes that implement solvers to solve for aggregate transition rules
 *  
 * @author Tobias Grasl
 * 
 * @param <S> The State-Object Type for this Solver
 */
public interface AggregateProblemSolver<S extends State<?>>
{
	/**
	 * Perform initialisation before the calculation commences
	 * 
	 * @param state_ The state being used for the the calculation
	 */
	void initialise(S state_);
	
	/**
	 * This method updates the aggregate transition rule on the state
	 * 
	 * @param state_ The current calculation state
	 */
	void updateAggregateTransition(S state_);
	
	/**
	 * This hook can be used by subclasses for preparation required for the main derivative aggregation calc
	 * 
	 * @param state_ The state of the calculation
	 */
	default void prepareAggregatePolicyCalculation(S state_)
	{
		// Default does nothing
	}
}
