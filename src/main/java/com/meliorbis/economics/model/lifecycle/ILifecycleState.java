/**
 * 
 */
package com.meliorbis.economics.model.lifecycle;

import com.meliorbis.economics.model.State;
import com.meliorbis.numerics.generic.primitives.DoubleArray;

/**
 * To be implemented by classes holding the state during (and after) the
 * solution of a lifecycle model
 * 
 * TODO: This is not implemented yet
 * 
 * @author Tobias Grasl
 */
@SuppressWarnings("rawtypes")
public interface ILifecycleState extends State
{
	/**
	 * Updates the individual transition rule for agents of the specified age to the provided rule
	 * 
	 * @param rule_ The rule
	 * @param age_ The age to update for
	 */
	void setIndividualTransition(DoubleArray<?> rule_, int age_);

    /**
     * Sets the grid of expected future aggregate controls given current states and
     * future exo states.
     *
     * The grid dimensions should be:
     *
     * 1) Current shocks
     * 2) Future shocks
     * 3) Future Perm shocks
     * 4) Current States
     * 5) Current Controls
     * 6) n - number of states
     *
     * @param expectedStates_ The array of expected states
     */
    void setExpectedAggregateControls(DoubleArray<?> expectedStates_);

    /**
     * @return A grid of what the future aggregate controls are expected to be given a certain
     * stochastic transition
     */
    DoubleArray<?> getExpectedAggregateControls();
}
