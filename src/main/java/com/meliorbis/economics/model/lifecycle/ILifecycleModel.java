/**
 * 
 */
package com.meliorbis.economics.model.lifecycle;

import java.util.List;

import com.meliorbis.economics.infrastructure.simulation.DiscretisedDistribution;
import com.meliorbis.economics.model.Model;
import com.meliorbis.economics.model.ModelException;
import com.meliorbis.economics.model.State;
import com.meliorbis.numerics.generic.MultiDimensionalArray;
import com.meliorbis.numerics.generic.primitives.DoubleArray;

/**
 * Provides additional methods required in handling a lifecycle model
 * 
 * TODO: This is not implemented yet
 * 
 * @author Tobias Grasl
 */
@SuppressWarnings("rawtypes")
public interface ILifecycleModel extends Model
{
	/**
	 * Performs calculations regarding a single future state given a current
	 * state.
	 * 
	 * @param age_ The age for which to perform the transition
	 * @param transitionIndex_
	 *            The index of the transition matrix, i.e. the current state
	 *            followed by the future state
	 * @param aggDetStateIndex_
	 *            The future deterministic state
	 * @param state_
	 *            The state of the calculation, which may be updated
	 * 
	 * @return A double array
	 * 
	 * @throws ModelException In the event of failure
	 */
	DoubleArray<?> handleIndividualTransition(int age_, 
			int[] transitionIndex_, 
			int[] aggDetStateIndex_, State state_) throws ModelException;

	/**
	 * @return The number of generations that coexist in the economy
	 */
	int getNumberOfGenerations();

	/**
	 * Gets the distribution of zero-age agents in the given shock state
	 * 
	 * @param shocks_
	 *            The aggregate shock state
	 * 
	 * @return The initial distribution
	 * @throws ModelException In the event of failure
	 */
	DoubleArray<?> getZeroAgeDist(MultiDimensionalArray<Integer, ?> shocks_) 
			throws ModelException;

	double getProductivityFactor();

	/**
	 * @return The number of aggregate control variables in this model
	 */
	int getAggregateControlCount();

	/**
	 * @return The levels to be used for each aggregate control variable
	 */
	List<DoubleArray<?>> getAggregateControls();

	/**
	 * @return The levels to be used for each aggregate control variable
	 */
	List<Boolean> isControlPredetermined();

	/**
	 * Given the individual transition function dependend on aggregate controls,
	 * calculate the actual implied aggregate controls from the individual
	 * transition at each combination of aggregate controls
	 *
	 * @param simState_ The current simulation state
	 * @param individualTransitionByAggregateControl_
	 *            The individual transition function, conditional on the grid
	 *            values of aggregate controls
	 *            
	 * @param currentAggStates_ The aggregate state values
	 * @param priorAggShockIndices_ The agggregate shock realisation
	 * @param calcState_ The state of the calculation
	 *
	 * @return A grid that has one dimension for each aggregate control plus one
	 *         across the different controls, sized according to the number of
	 *         values of that control we are solving the model for, which
	 *         indicates for each point what the implied aggregate control
	 *         values are at that point
	 *
	 * @throws ModelException If the model implementation encounters an error
	 */
	DoubleArray<?> calculateAggregateControls(
			DiscretisedDistribution simState_, 
			DoubleArray<?> individualTransitionByAggregateControl_,
			double[] currentAggStates_, Integer[] priorAggShockIndices_, 
			State calcState_) throws ModelException;

	double[] calcImpliedControls(DiscretisedDistribution simState_, State state_, Integer[] currentAggShocks_, double[] currentStates_,
			double[] currentControls_, DoubleArray<?> kpMat) throws ModelException;
}
