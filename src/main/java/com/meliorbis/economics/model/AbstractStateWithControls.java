/**
 * 
 */
package com.meliorbis.economics.model;

import com.meliorbis.economics.infrastructure.AbstractStateBase;
import com.meliorbis.numerics.generic.primitives.DoubleArray;

/**
 * Holds individual and aggregate controls policies
 * 
 * @author Tobias Grasl
 * 
 * @param <C> The config type
 */
public abstract class AbstractStateWithControls<C extends ModelConfig>
	extends AbstractStateBase<C> implements StateWithControls<C>
{
	private DoubleArray<?> _aggControlsPolicy;
	private DoubleArray<?> _indControlsPolicy;
	private DoubleArray<?> _indControlsPolicySim;
	
	protected AbstractStateWithControls(C config_)
	{
		super(config_);
	}


	@Override
	public void setCurrentControlsPolicy(DoubleArray<?> newPolicy_)
	{
		_aggControlsPolicy = newPolicy_;
	}

	@Override
	public DoubleArray<?> getCurrentControlsPolicy()
	{
		return _aggControlsPolicy;
	}

	public void setIndividualControlsPolicy(DoubleArray<?> indControlsPolicy_)
	{
		_indControlsPolicy = indControlsPolicy_;
		
	}
	
	@Override
	public DoubleArray<?> getIndividualControlsPolicy()
	{
		return _indControlsPolicy;
	}
	
	@Override
	public DoubleArray<?> getIndividualControlsPolicyForSimulation()
	{
		if(_indControlsPolicySim == null) {
			return getIndividualControlsPolicy();
		}
		
		return _indControlsPolicySim;
	}
	
	@Override
	public void setIndividualControlsPolicyForSimulation(DoubleArray<?> newPolicy_) 
	{
		_indControlsPolicySim = newPolicy_;
	}
	

}
