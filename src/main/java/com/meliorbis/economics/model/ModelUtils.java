/**
 * 
 */
package com.meliorbis.economics.model;


/**
 * Utility methods for dealing with models
 * 
 * @author Tobias Grasl
 */
public abstract class ModelUtils
{
	// Prevent construction
	private ModelUtils(){}
}
