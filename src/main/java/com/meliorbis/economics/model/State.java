/**
 * 
 */
package com.meliorbis.economics.model;

import java.util.Map;

import com.meliorbis.economics.individual.IndividualProblemState;
import com.meliorbis.economics.infrastructure.AggregateProblemState;
import com.meliorbis.numerics.convergence.Criterion;
import com.meliorbis.numerics.generic.primitives.DoubleArray;

/**
 * Holds the state of the calculation whilst a model is being solved and simulated
 * 
 * @author Tobias Grasl
 * 
 * @param <C> The configuration type used with this State's Model
 */
public interface State<C extends ModelConfig> extends IndividualProblemState, 
	AggregateProblemState
{	
	/**
	 * Increments the count of periods that have been solved
	 */
	void incrementPeriod();
	
	/**
	 * @return The period we are in
	 */
	int getPeriod();
	
	/**
	 * Passes a map of arrays by name that should be set
	 * 
	 * @param arraysByName_ The arrays to be set
	 */
	void setNamedArrays(Map<String, DoubleArray<?>> arraysByName_);
	

	/**
	 * Returns the criterion that the Solver will check against the target
	 * value to determine whether the solution has converged
	 * 
	 * @return The criterion
	 */
	Criterion getConvergenceCriterion();

    /**
     * @return The model being calculated which this state is for
     */
    C getConfig();
}
