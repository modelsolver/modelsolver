package com.meliorbis.economics.model;

import com.meliorbis.economics.infrastructure.SolverException;

/**
 * Exception to be thrown by model classes
 */
public class ModelException extends SolverException
{
    public ModelException(String description_)
    {
        super(description_);
    }

    public ModelException(String description_, Throwable cause_)
    {
        super(description_, cause_);
    }
}
