/**
 * 
 */
package com.meliorbis.economics.model;

import com.meliorbis.economics.infrastructure.simulation.SimState;
import com.meliorbis.economics.infrastructure.simulation.TransitionRecord;
import com.meliorbis.numerics.generic.primitives.DoubleArray;

/**
 * Provides the callback method for events on a model
 * 
 * @author Tobias Grasl
 * 
 * @param <C> The config type
 * @param <S> The state type
 */
public interface ModelEvents<C extends ModelConfig, S extends State<C>>
{

	/**
	 * Handles the aftermath of an aggregate simulation step
	 * 
	 * @param currentAggState_ The aggregate state resulting from the simulation step
	 * @param priorShockIndex_ The shock at the beginning of the period of simulation
	 * @param currentShockIndex_ The shock at the end of the period of simulation
	 * @param state_  The model state
	 * 
	 * @throws ModelException If there are issues
	 */
	default void afterAggregateTransition(DoubleArray<?> currentAggState_,
			Integer[] priorShockIndex_, 
			Integer[] currentShockIndex_,
			S state_) throws ModelException { }

	/**
	 * Before the interpolation step of a simulation period has been performed, but after 
	 * aggregates have been calculated, this method is called and can modify the current 
	 * distribution as well as the transition record
	 * 
	 * @param currentDist_ The current distribution
	 * @param record_ The transition record resulting from the interpolation
	 * @param state_ The calculation state
	 * 
	 * @param <D> The type of simulation state to use
	 */
	default <D extends SimState> void beforeSimInterpolation(D currentDist_,
			TransitionRecord<? extends SimState, ? extends Number> record_, 
			S state_) { }

	/**
	 * After the interpolation step of a simulation period has been performed, 
	 * this callback is called and can modify the resulting transition record
	 * 
	 * @param record_ The transition record resulting from the interpolation
	 * @param state_ The calculation state
	 */
	default void afterSimInterpolation(
			TransitionRecord<? extends SimState, ? extends Number> record_, 
			S state_) { }

	/**
	 * A callback method which is called when the overall algorithm fails to converge. The default
	 * does nothing.
	 * 
	 * @param state_ The state of the calculation
	 */
	default void convergenceFailed(S state_) { }
	
	/**
	 * Called with the model soltution once it has been determined
	 * 
	 * @param state_ The steady state
	 */
	default void solutionFound(S state_) { }

}
