package com.meliorbis.economics.model;

import com.meliorbis.economics.infrastructure.simulation.DiscretisedDistribution;
import com.meliorbis.numerics.fixedpoint.FixedPointState;

/**
 * Fixed point state for calculations on models where the intent is to
 * find the fixed point of assumed and implied aggregates, under the
 * assumption of no aggregate risk
 * 
 * @author Tobias Grasl
 * 
 * @param <S> The state type
 * @param <M> The model type
 */
public class AggregateFixedPointState<S extends State<?>, M extends Model<?,S>> implements FixedPointState
{
    private M _model;
    private S _calcState;
	private DiscretisedDistribution _simState;
	
	public AggregateFixedPointState(M model_, S calcState_, DiscretisedDistribution simState_)
	{
        _model = model_;
        _calcState = calcState_;
		_simState = simState_;
	}

	public S getCalcState()
	{
		return _calcState;
	}

	public void setCalcState(S calcState_)
	{
		_calcState = calcState_;
	}

	public DiscretisedDistribution getSimState()
	{
		return _simState;
	}

	public void setSimState(DiscretisedDistribution simState_)
	{
		_simState = simState_;
	}

    public M getModel()
    {
        return _model;
    }

    public void setModel(M model_)
    {
        _model = model_;
    }
}
