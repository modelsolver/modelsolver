package com.meliorbis.economics.model;

import com.meliorbis.numerics.generic.primitives.DoubleArray;

/**
 * State classes for models which have aggregate controls
 *
 * @param <C> The configuration type used with this State's Model
 */
public interface StateWithControls<C extends ModelConfig> extends State<C>
{
    /**
     * Sets the grid of expected future aggregate controls given current states and
     * future exo states.
     *
     * The grid dimensions should be:
     *
     * 1) Current shocks
     * 2) Future shocks
     * 3) Future Perm shocks
     * 4) Current States
     * 5) Current Controls
     * 6) n - number of controls
     *
     * @param expectedStates_ The array of expected states
     */
    void setExpectedAggregateControls(DoubleArray<?> expectedStates_);

    /**
     * @return A grid of what the future aggregate controls are expected to be given a certain
     * stochastic transition
     */
    DoubleArray<?> getExpectedAggregateControls();

    /**
     * Updates the aggregate controls policy which predicts the current 
     * aggregate controls based on current states
     * 
     * @param newPolicy_ The value to set
     */
    void setCurrentControlsPolicy(DoubleArray<?> newPolicy_);
    
    /**
     * Returns the aggregate controls policy function. This function has dimensions for states, controls and aggregate
     * shocks, and the controls should be calculated as the fixed point of the conditional function given states and
     * shocks
     *
     * @return An array representing the controls policy function
     */
    DoubleArray<?> getCurrentControlsPolicy();
    
    /**
     * Returns the individual controls policy function, which is the same size as the individual transition except that it
     * has one point in the last grid dimension for each separate individual control variable
     * 
     * @return The policy function for individual controls conditional on aggregate and individual endogenous and exogenous states
     */
    DoubleArray<?> getIndividualControlsPolicy();

    /**
     * Returns the controls policy based on the simulation grid
     * 
     * @return The controls policy based on the simulation grid
     */
	DoubleArray<?> getIndividualControlsPolicyForSimulation();

	/**
	 * @param newPolicy_ The new controls policy for simulation
	 */
	void setIndividualControlsPolicyForSimulation(DoubleArray<?> newPolicy_);
}
