package com.meliorbis.economics.model;

import com.meliorbis.economics.infrastructure.simulation.SimState;
import com.meliorbis.numerics.generic.MultiDimensionalArray;
import com.meliorbis.numerics.generic.primitives.DoubleArray;

/**
 * Interface for models which also have aggregate controls
 * 
 * @param <C> The type used to configure this model
 * @param <S> The type used to hold state for this model
 */
public interface ModelWithControls<C extends ModelConfig, S extends StateWithControls<C>> extends Model<C,S>
{
    /**
     * Given the individual transition function conditional on aggregate control values, but already adjusted
     * to the realised aggregate states and shocks, this method calculates and returns the aggregate control values
     * at the given the input distribution, aggregate states, shocks and calculation state.
     * 
     * @param simState_ The current simulation state (distribution)
     * @param individualTransitionByAggregateControl_ The individual transition function, conditional on the grid values
     *                                                of aggregate controls
     *                                                
     * @param currentAggStates_ The current aggregate state values
     * @param currentAggShocks_ The current aggregate shock realisation
     * @param calcState_                              The state of the calculation
     * 
     * @return The value of each aggregate control in the model given the inputs
     * 
     * @throws ModelException If a model-specific error occurs
     * 
     * @param <N> The numeric type of shocks used in this model
     */
    <N extends Number> double[] calculateAggregateControls(
    		SimState simState_,
            DoubleArray<?> individualTransitionByAggregateControl_,
            double[] currentAggStates_,
            MultiDimensionalArray<N, ?> currentAggShocks_,
            S calcState_) throws ModelException;
}
