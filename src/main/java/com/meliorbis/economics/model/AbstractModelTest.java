package com.meliorbis.economics.model;

import org.junit.Before;

import com.meliorbis.economics.infrastructure.Solver;
import com.meliorbis.economics.infrastructure.simulation.DiscretisedDistributionSimulatorImpl;
import com.meliorbis.numerics.DoubleNumerics;
import com.meliorbis.numerics.Numerics;
import com.meliorbis.numerics.generic.MultiDimensionalArrayException;
import com.meliorbis.numerics.generic.primitives.DoubleArray;
import com.meliorbis.numerics.io.NOOPWriterFactory;

public class AbstractModelTest
{

	protected static final double ALLOWED_ERROR = 1e-15;
	private DoubleNumerics _numerics;
	protected Solver _gridSolver;
	protected DiscretisedDistributionSimulatorImpl _simulator;

	public AbstractModelTest()
	{
		super();
	}

	@Before
	public void setUp() throws MultiDimensionalArrayException
    {
		_numerics = DoubleNumerics.instance();
		_gridSolver = new Solver(new NOOPWriterFactory());
		_simulator = new DiscretisedDistributionSimulatorImpl(new NOOPWriterFactory());
	}
	
	protected DoubleArray<?> createArray(int... dimensions)
	{
		return _numerics.getArrayFactory().newArray(dimensions);
	}
	/**
	 * Utility method to create a 1-D array with the given data
	 * 
	 * @param doubles_ The data to put in the array
	 * 
	 * @return The newly created array
	 */
	protected DoubleArray<?> create1DArray(double... doubles_)
	{
		return getNumerics().getArrayFactory().newArray(doubles_);
	}

	public Numerics<Double> getNumerics() {
		return _numerics;
	}
}