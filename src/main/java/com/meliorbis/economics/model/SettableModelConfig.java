/**
 * 
 */
package com.meliorbis.economics.model;

import com.meliorbis.numerics.generic.primitives.DoubleArray;

import java.io.File;
import java.util.List;

/**
 * Model configurations that can be updated
 * 
 * @author Tobias Grasl
 */
public interface SettableModelConfig extends ModelConfig
{
	/**
	 * @param indExoStates_ The list of arrays of potential values for exogenous individual shocks, one for
	 * each such shock in the model
	 */
	void setIndividualExogenousStates(List<DoubleArray<?>> indExoStates_);

	/**
	 * @param indExoStates_ The list of arrays of potential values for exogenous individual shocks, one for
	 * each such shock in the model
	 */
	void setIndividualExogenousStates(DoubleArray<?>... indExoStates_);
	
	/**
	 * @param indEndoStates_ The list of arrays of potential values for endogenous individual states, one for
	 * each such state in the model
	 */
	public void setIndividualEndogenousStates(List<DoubleArray<?>> indEndoStates_);
	
	/**
	 * @param indEndoStates_ The list of arrays of potential values for endogenous individual states, one for
	 * each such state in the model
	 */
	public void setIndividualEndogenousStates(DoubleArray<?>... indEndoStates_);

	/**
	 * @param indEndoSimStates_ The list of arrays of potential values for endogenous individual states to be used in
	 * simulation, one for each such state in the model
	 */
	public void setIndividualEndogenousStatesForSimulation(List<DoubleArray<?>> indEndoSimStates_);
	
	/**
	 * @param indEndoSimStates_ The list of arrays of potential values for endogenous individual states to be used in
	 * simulation, one for each such state in the model
	 */
	public void setIndividualEndogenousStatesForSimulation(DoubleArray<?>... indEndoSimStates_);

	/**
	 * @param aggExoStates_ The list of arrays of potential values for exogenous aggregate shocks, one for each 
	 * such shock in the model
	 */
	public void setAggregateExogenousStates(List<DoubleArray<?>> aggExoStates_);
	
	/**
	 * @param aggEndoStates_ The list of arrays of potential values for endogenous aggregate states, one for
	 * each such state in the model
	 */
	public void setAggregateEndogenousStates(List<DoubleArray<?>> aggEndoStates_);
	
	/**
	 * @param aggControls_ The list of arrays of potential values for aggregate controls, one for each 
	 * such variable in the model
	 */
	public void setAggregateControls(List<DoubleArray<?>> aggControls_);
	
	/**
	 * @param ctrlTargets_ The list of control targets on the grid
	 */
	void setControlTargets(List<DoubleArray<?>> ctrlTargets_);
	
	/**
	 * @param ctrlTargets_ The ctrl targets on tge grid
	 */
	void setControlTargets(DoubleArray<?>... ctrlTargets_);
	
	/**
	 * @param aggNormStates_ The list of arrays of potential values for exogenous aggregate states used for normalisation,
	 * commonly permanent shocks, one for each such state in the model
	 */
	public void setAggregateNormalisingExogenousStates(List<DoubleArray<?>> aggNormStates_);
	
	/**
	 * @param aggExoStates_ The list of arrays of potential values for exogenous aggregate shocks, one for each 
	 * such shock in the model
	 */
	public void setAggregateExogenousStates(DoubleArray<?>... aggExoStates_);
	 
	/**
	 * @param aggEndoStates_ The list of arrays of potential values for endogenous aggregate states, one for
	 * each such state in the model
	 */
	public void setAggregateEndogenousStates(DoubleArray<?>... aggEndoStates_);
	
	/**
	 * @param aggControls_ The list of arrays of potential values for aggregate controls, one for each 
	 * such variable in the model
	 */
	public void setAggregateControls(DoubleArray<?>... aggControls_);
	
	/**
	 * @param aggNormStates_ The list of arrays of potential values for exogenous aggregate states used for normalisation,
	 * commonly permanent shocks, one for each such state in the model
	 */
	public void setAggregateNormalisingExogenousStates(DoubleArray<?>... aggNormStates_);
	
	/**
	 * @param transitionProbs_ Array with probabilities of moving from any given individual state in any given aggregate state to any
	 * combination of future individual, aggregate and normalising states.
	 */
	public void setExogenousStateTransiton(DoubleArray<?> transitionProbs_);

    /**
     * @param dir_ The directory in which to store the solved model
     */
    public void setSolutionDirectory(File dir_);
    
    /**
     * @param indexes_ The indexes of aggregate states known with certainty one period in advance, 
     * given prior aggregates.
     */
    public void setAggregatesKnownWithCertainty(int... indexes_);
    
    /**
     * @param indexes_ The indexes of aggregate controls which affect expectations
     */
    public void setControlsAffectingExpectations(int... indexes_);
    
}
