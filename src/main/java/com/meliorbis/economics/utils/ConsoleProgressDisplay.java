package com.meliorbis.economics.utils;

import org.apache.commons.lang.StringUtils;

/**
 * A Utility class that displays progress on the console in a way that is concise yet 
 * informative
 * 
 * @author Tobias Grasl
 */
public class ConsoleProgressDisplay
{
	private final int _stepsInPoint;
	private final int _pointsInLine;
	private final int _lineTotal;
	
	private int _steps = 0;
	private int _currentLineLength;
	/**
	 * Creates a display where each point represents 100 steps and there are 10 points to the line
	 */
	public ConsoleProgressDisplay()
	{
		this(100,10);
	}
	
	/**
	 * @param stepsInPoint_ The number of steps represented by each point
	 * @param pointsInLine_ The number of points in a line
	 */
	public ConsoleProgressDisplay(int stepsInPoint_, int pointsInLine_)
	{
		_stepsInPoint = stepsInPoint_;
		_pointsInLine = pointsInLine_;
		
		_lineTotal = _stepsInPoint*_pointsInLine;
	}
	
	public void nextStep()
	{
		_steps++;
		
		displayCurrentLine();
	}
	
	private void displayCurrentLine()
	{
		int numLines = _steps/_lineTotal;
		int lineRemainder = _steps % _lineTotal;
		
		int numPoints = lineRemainder/_stepsInPoint;
		int remainder = lineRemainder % _stepsInPoint;
		
		String delete = StringUtils.repeat("\b", _currentLineLength);
		String points = StringUtils.repeat(".", numPoints);
		
		String newLine = String.format("%s(%3s)(%3s)%s",delete,numLines,remainder,points);
		
		// Need to take account of the \b's
		_currentLineLength = newLine.length()-_currentLineLength;
		
		System.out.print(newLine);
	}
}
