/**
 * 
 */
package com.meliorbis.economics.utils;

/**
 * @author toby
 *
 */
public class ConsoleProgressDemo
{

	public static void main(String[] args)
	{
		ConsoleProgressDisplay consoleProgressDisplay = new ConsoleProgressDisplay();
		
		for(int i = 0; i<3134;i++)
		{
			synchronized(consoleProgressDisplay)
			{
				try
				{
					consoleProgressDisplay.wait(5);
				} catch (InterruptedException e)
				{
				}
				
				consoleProgressDisplay.nextStep();
			}
		}
	}

}
