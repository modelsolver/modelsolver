/**
 * 
 */
package com.meliorbis.economics.individual;

import com.meliorbis.economics.model.ModelException;
import com.meliorbis.economics.model.State;

/**
 * Interface for individual problem solvers that work iteratively. The framework handles the iteration, so
 * the solver needs to implement the individual steps
 * 
 * @author Tobias Grasl
 * 
 * @param <S> The State type
 */
public interface IndividualProblemSolver<S extends State<?>>
{
	/**
	 * Initialise the solver
	 * 
	 * @param state_ The state instance used in this calculation
	 */
	void initialise(S state_);
	
	/**
	 * Perform a single iteration given the current state of the calculation
	 * 
	 * @param state_ The current state of the calculation
	 * 
	 * @throws ModelException If the model causes an error during the calculation
	 */
	void performIteration(S state_) throws ModelException;
}
