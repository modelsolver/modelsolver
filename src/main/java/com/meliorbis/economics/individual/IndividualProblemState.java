/**
 * 
 */
package com.meliorbis.economics.individual;

import com.meliorbis.numerics.convergence.Criterion;
import com.meliorbis.numerics.generic.primitives.DoubleArray;

/**
 * Provides common values required for solving individual agents' problems
 * 
 * @author Tobias Grasl
 */
public interface IndividualProblemState
{
	/**
	 * @param result_ The new expectation for future individual state transition rules
	 */
	void setExpectedIndividualTransition(DoubleArray<?> result_);
	
	/**
	 * @return The expected individual state transition rules
	 */
	DoubleArray<?> getExpectedIndividualTransition();
	
	/**
	 * Gets the end-of-present-period state grid, i.e the grid of values conditional upon which the
	 * calculations are performed
	 * 
	 * @return The grid of end-of-present period values
	 */
	DoubleArray<?> getEndOfPeriodStates();

	/**
	 * Gets a grid of start-of-period values, but appropriately normalised by future shocks
	 * 
	 * @return The grid of potential start of period values
	 */
	DoubleArray<?> getNormalisedStartOfPeriodStates();

	/**
	 * @return The transition function as currently solved for, but interpolated to the simulation grid,
	 * which may be differen to the solution grid
	 */
	DoubleArray<?> getIndividualPolicyForSimulation();
	
	/**
	 * Gets the end-of-present-period state grid used for simulation, i.e the grid of values onto which
	 * each sim step is distributed
	 * 
	 * @return The grid of end-of-present period values
	 */
	DoubleArray<?> getEndOfPeriodStatesForSimulation();
	
	/**
	 * Gets the start-of-period state grid used for simulation, i.e the grid of values which a distribution
	 * represents
	 * 
	 * @return The grid of start-of-period values
	 */
	DoubleArray<?> getStartOfPeriodStatesForSimulation();
	
	/**
	 * Gets the individual state transition policy
	 * 
	 * @return The current version of the individual state transition policy
	 */
	DoubleArray<?> getIndividualPolicy();
	
	/**
	 * Updates the individual state transition policy
	 * 
	 * @param newValues_ The new version of the individual state transition policy
	 */
	void setIndividualPolicy(DoubleArray<?> newValues_);
	
	/**
	 * Updates the error value between two runs of solving the individual policy
	 * 
	 * @param errorValue_ The new error 
	 */
	void setIndividualError(double errorValue_);
	
	/**
	 * @return The current convergence criterion of the individual problem
	 */
	Criterion getIndividualCriterion();
}
