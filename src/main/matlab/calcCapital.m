function capital = calcCapital(density, capLevels)
    if length(capLevels) ~= size(density.density,1)
        capLevels = linspace(0, capLevels(length(capLevels)),size(density.density,1))';
    end
    
    capital = sum(sum(density.density.*(capLevels*ones(1,size(density.density,2)))))+...
        sum(sum(density.oa.*density.op));
end