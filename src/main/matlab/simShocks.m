function [ simResults ] = simShocks( initialK, shocks, K,Kp)
    
    simResults = zeros(length(shocks)+1,1);
    simResults(1) = initialK;
    
    for period = 1:length(shocks)
        simResults(period+1) = interp1(K,Kp(:,shocks(period,1)),simResults(period));
        
        if isnan(simResults(period+1))
            simResults = simResults(1:period);
            warning(['Out of range after ' num2str(period) ' periods']);
            return;
        end
    end

end

