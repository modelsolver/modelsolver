function checkResults(results, range)
    if nargin < 2
        range = 1:(length(results.states));
    end
    
    % Calculate the relative differences between individual and aggregate
    % simulations
    diffs = results.aggStates(range,:)./results.states(range,:)-1;
    
    runningMean = cumsum(diffs)./(1:length(diffs))';
    [val, index] = max(abs(diffs(:,1)));
    
    plot(1:9000,diffs(1:9000,1));
    hold on;
    plot(1:9000, runningMean(1:9000),'--g');
    hold off;
  
    disp(['Error Bias: ' num2str(mean(diffs)*100) '%']); 
    disp(['Error Mean: ' num2str(mean(abs(diffs))*100) '%']);
    disp(['Error StD: ' num2str(std(diffs)*100) '%']);
    disp(['Error Max: ' num2str(max(abs(diffs))*100) '%']);
    disp('---')
    for col = 1:size(results.states,2)
        disp(['Corr: ' num2str(corr(results.states(range,col),results.aggStates(range,col)))]);
        disp(['R^2: ' num2str(1 - sum((results.aggStates(range,col)-results.states(range,col)).^2)/sum((results.states(range,col)-mean(results.states(range,col))).^2))]);
    end
    
    if isfield(results,'aggControls')
        diffs = results.aggControls(range,:)./results.controls(range,:)-1;

        disp('---')
        disp('Labour');
        disp('---')
        disp(['Error Bias: ' num2str(mean(diffs)*100) '%']); 
        disp(['Error Mean: ' num2str(mean(abs(diffs))*100) '%']);
        disp(['Error StD: ' num2str(std(diffs)*100) '%']);
        disp(['Error Max: ' num2str(max(abs(diffs))*100) '%']);
        disp('---')
        disp(['Corr: ' num2str(corr(results.controls(range,1),results.aggControls(range,1)))]);
        disp(['R^2: ' num2str(1 - sum((results.aggControls(range,1)-results.controls(range,1)).^2)/sum((results.states(range,1)-mean(results.states(range,1))).^2))]);
    end
    
    if(~isfield(results,'state'))
        return
    end
    
    if ~isfield(results,'statesAgg1P')
        return
    end
    
    if ~isfield(results,'statesAgg1P')
        results = addOnePeriodAhead(results);
    end
    diffs = results.statesAgg1P(range,:)./results.states(range,:)-1;
    
    disp('---')
    disp('One Period Ahead');
    disp('---')
    disp(['Error Bias: ' num2str(mean(diffs)*100) '%']); 
    disp(['Error Mean: ' num2str(mean(abs(diffs))*100) '%']);
    disp(['Error StD: ' num2str(std(diffs)*100) '%']);
    disp(['Error Max: ' num2str(max(abs(diffs))*100) '%']);
    disp('---')
     disp(['1-Period Corr - 1: ' num2str(corr(results.states(range),results.statesAgg1P(range))-1)]);
     disp(['1-Period R^2 - 1: ' num2str(1 - sum((results.statesAgg1P(range)-results.states(range)).^2)/sum((results.states(range)-mean(results.states(range))).^2)-1)]);
   
    if isfield(results,'controlsAgg1P')
        diffs = results.controlsAgg1P(range,:)./results.controls(range,:)-1;

        disp('---')
        disp('Labour One Period Ahead');
        disp('---')
        disp(['Error Bias: ' num2str(mean(diffs)*100) '%']); 
        disp(['Error Mean: ' num2str(mean(abs(diffs))*100) '%']);
        disp(['Error StD: ' num2str(std(diffs)*100) '%']);
        disp(['Error Max: ' num2str(max(abs(diffs))*100) '%']);
        disp('---')
%       disp(['1-Period Corr - 1: ' num2str(corr(results.controls(range),results.controlsAgg1P(range))-1)]);
%       disp(['1-Period R^2 - 1: ' num2str(1 - sum((results.controlsAgg1P(range)-results.controls(range)).^2)/sum((results.controls(range)-mean(results.controls(range))).^2)-1)]);
    end
    
    if isfield(results,'ind')
        kdiff = abs(results.ind.kTilde(range) - results.ind.k(range))/mean(results.ind.k(range))*100;
        cdiff = abs(results.ind.cTilde(range) - results.ind.c(range))./results.ind.c(range)*100;
        disp('---')
        disp('Dynamic Euler Equation Error');
        disp('---')
        disp(['k mean: ' num2str(mean(kdiff)) '%']); 
        disp(['k max: ' num2str(max(kdiff)) '%']); 
        disp('---')
        disp(['c mean: ' num2str(mean(cdiff)) '%']); 
        disp(['c max: ' num2str(max(cdiff)) '%']);
    end 
end