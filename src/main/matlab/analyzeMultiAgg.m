function [gradMatrix, actualGradMatrix, errs] = analyzeMultiAgg( results, range)

    if nargin < 2
        range = 1:(length(results.shocks)-1);
    end
    
    shocks = results.shocks(range,1);
    
    priors = log(results.states(range,:));
    posts = log(results.states(range+1,:));
    posts1p = log(results.statesAgg1P(range+1,:));
    
    gradMatrix = zeros(size(posts,2),size(posts,2),numel(unique(shocks)));
    actualGradMatrix = zeros(size(posts,2),size(posts,2),numel(unique(shocks)));
    errs = zeros(2,2);
    
    for shockLevel = 0:max(shocks)
        disp('---')
        disp(['Shock Level: ' num2str(shockLevel)])
        disp('---')
        
        priorsForShock = priors(shocks == shockLevel,:);
        postsForShock = posts(shocks == shockLevel,:);
        posts1pForShock = posts1p(shocks == shockLevel,:);
        
        errs(:,shockLevel+1,1) = mean(abs(posts1pForShock./postsForShock-1)*100);
        errs(:,shockLevel+1,2) = max(abs(posts1pForShock./postsForShock-1)*100);
        
        for aggIndex = 1:size(postsForShock,2)
            disp('---')
            disp(['Aggregate: ' num2str(aggIndex)])
            disp('---')    
            linMod = LinearModel.fit(priorsForShock,postsForShock(:,aggIndex));
            
            gradMatrix(:,aggIndex,shockLevel+1) = linMod.Coefficients.Estimate(2:1+size(posts,2))';
           
        end
        
        % dKpdK
        actualGradMatrix(1,1,shockLevel+1) = diff(log(results.state.Kp(1:2,1,shockLevel+1,1)))/diff(log(results.state.K(1:2)));
        % dKpdVar
        actualGradMatrix(2,1,shockLevel+1) = diff(log(results.state.Kp(1,1:2,shockLevel+1,1)))/diff(log(results.state.vars(1:2)));
        
        % dVarpdK
        actualGradMatrix(1,2,shockLevel+1) = diff(log(results.state.Kp(1:2,1,shockLevel+1,2)))/diff(log(results.state.K(1:2)));
        % dVarpdVar
        actualGradMatrix(2,2,shockLevel+1) = diff(log(results.state.Kp(1,1:2,shockLevel+1,2)))/diff(log(results.state.vars(1:2)));
        
    end

end

