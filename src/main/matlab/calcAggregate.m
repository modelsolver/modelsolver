function results = calcAggregate(results)
    
    employed = 0.96*(results.shocks == 1) + 0.9*(results.shocks == 0);
    
    results.states = [results.states results.states(:,2).*((employed-1)*(-1))+results.states(:,1).*employed];
    results.aggStates = [results.aggStates results.aggStates(:,2).*((employed-1)*(-1))+results.aggStates(:,1).*employed];
end