function linMod = plot1PDiffs(results,shockLevel, range)
    
    if nargin < 3
        range = 1:(length(results.states)-1);
    end
    
    if nargin < 2
        shockLevel = 0;
    end
    
    diffs = log(results.statesAgg1P(range,:)) - log(results.states(range,:));
    diffs = diffs(2:length(diffs),:);
    
    priors = log(results.states(range,:));
    priors = priors(1:(length(priors)-1),:);
    
    currents = log(results.states(range,:));
    currents = currents(2:length(currents),:);
    
    shocks = results.shocks(range,1);
    shocks = shocks(1:(length(shocks)-1),:);
    
    diffs = diffs(shocks == shockLevel,:);
    priors = priors(shocks == shockLevel,:);
    currents = currents(shocks == shockLevel,:);
    
    K = results.state.gradients(shockLevel+1,1);
    Kp = results.state.gradients(shockLevel+1,2);
    
     linMod = LinearModel.fit([(priors-log(K)) (priors-log(K)).^2],diffs);
     
     predictMod = LinearModel.fit([(priors-log(K)) (priors-log(K)).^2],currents)
     
     fcErrors = abs(predictMod.feval([(priors-log(K)) (priors-log(K)).^2])-currents)*100;
     
     disp(['Optimal Forecast Errors (mean, max)(%):' num2str(mean(fcErrors)) ',' num2str(max(fcErrors))])
%     linMod.Coefficients
    disp(['1-st Order Error (%): ' num2str(linMod.Coefficients.Estimate(2)./predictMod.Coefficients.Estimate(2)*100)]);
    disp(['2-nd Order Error (%): ' num2str(linMod.Coefficients.Estimate(3)./predictMod.Coefficients.Estimate(3)*100)]);
     
     linMod2 = LinearModel.fit(exp(priors),exp(currents));
 %   scatter(exp(priors(:,1)),exp(currents(:,1))-linMod2.predict(exp(priors(:,1))),'.k');
 % figure;
      scatter(priors(:,1),diffs(:,1),'.k');
  
     points = linspace(min(priors),max(priors),100)';
    
    hold on;
    
%     plot(points,linMod.predict([(points-log(K)) (points-log(K)).^2 (points-log(K)).^3]),'-r');
     plot(points,(points-log(K))*linMod.Coefficients.Estimate(2)+linMod.Coefficients.Estimate(1),'-b');
     plot(points,(points-log(K)).^2*linMod.Coefficients.Estimate(3) + (points-log(K))*linMod.Coefficients.Estimate(2)+linMod.Coefficients.Estimate(1),'-r');
%     
% %    scatter(priors,diffs-((priors-log(K))*linMod.Coefficients.Estimate(2))-linMod.Coefficients.Estimate(1),'.m');
%     
     plot(log(K),0,'xg','MarkerSize',10);
     plot([min(priors) max(priors)],[0 0],'--k')
        
     
     hold off;
end