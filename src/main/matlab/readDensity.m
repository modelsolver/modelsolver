function [ density ] = readDensity( dir )
%UNTITLED Reads a set of arrays representing the distribution of capital
%   Detailed explanation goes here
    density.density = readFormattedCSV([dir '/density.csv']);
    density.overflowLevels = readFormattedCSV([dir '/oa.csv']);
    density.overflowProportions = readFormattedCSV([dir '/op.csv']);
end

