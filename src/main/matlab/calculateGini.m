function [ gini ] = calculateGini( popProp, capProp )
%CALCULATEGINI Calculates the gini coefficient for a discrete distribution
%where popPropn is the cumulative density and capPropn is the cummulative
%variable of interest
    gini = 1 - sum([popProp(1);diff(popProp)].*(capProp+[0;capProp(1:(length(capProp)-1))]));

end

