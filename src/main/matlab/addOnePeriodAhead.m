function results = addOnePeriodAhead(results)
    %% One period ahead predictions...
    transition = results.state.aggTransition;
    
    if ndims(transition) ~= 2
        return
    end
    
    results.statesAgg1P = zeros(size(results.states));
    results.statesAgg1P(1,:) = results.states(1,:);

    if ndims(transition) == 2
        for j = 2:length(results.statesAgg1P)
        %    results.statesAgg1P(j) = interp1(K,transition(:,shocks1(j-1,1)+1),states1(j-1));
           results.statesAgg1P(j) = exp(interp1(log(results.state.aggStates.X_1),log(transition(:,results.shocks(j-1,1)+1)),log(results.states(j-1)),'linear','extrap'));
        end
    else
        % ACTUALLY ONLY WORKS FOR 2 DIMS, DON'T KNOW HOW TO USE A VARIABLE
        % LENGHT INDEX
%         for j = 2:length(results.statesAgg1P)
%             for varIndex = 1:size(results.statesAgg1P,2)
%              currentTrans = log(transition(:,:,results.shocks(j-1,1)+1,varIndex));
%                 
%              currentTrans = interp1(log(results.state.K),currentTrans,log(results.states(j-1,1)));
%              results.statesAgg1P(j, varIndex) = exp(interp1(log(results.state.vars),currentTrans,log(results.states(j-1,2))));
%             end
%         end
    end
end