function [ popPropn, wealthPropn ] = calculateWealthDist( density, capitalLevels )
%CALCULATEWEALTHDIST Determines the cumulative wealth distribution
    if size(capitalLevels,1) < size(density.density,1)
        capitalLevels = linspace(0,max(capitalLevels),size(density.density,1))';%exp(exp(linspace(0,log(log(max(capitalLevels)+1)+1),size(density.density,1)))'-1)-1;
    end
    
    popAtCap = density.density;
    overCap = density.oa.*density.op;
    
    while ndims(popAtCap) > 2 || size(popAtCap,2)>1
        popAtCap = squeeze(sum(popAtCap,2));
        overCap = squeeze(sum(overCap,2));
    end

    overCap = squeeze(sum(overCap,1));

    popPropn = [cumsum(popAtCap);1];
    
    capitalHoldings = [popAtCap.*capitalLevels;overCap];
    
    wealthPropn = cumsum(capitalHoldings)./sum(capitalHoldings);
end

