function [ simResults ] = simShocksWithLabour( initialK, initialL, shocks, state, Ls, Ks)
    
    simResults = zeros(length(shocks),2);
    simResults(1,:) = [initialK, initialL];
    
    trans = state.Kp;
    lTrans = state.Lp;
    
    [L,K] = meshgrid(state.L,state.K);
    
    for period = 1:length(shocks)-1
        
        periodTrans = squeeze(trans(:,:,shocks(period,1)+1));
        periodLTrans = squeeze(lTrans(:,:,shocks(period,1)+1,shocks(period+1,1)+1));

        if nargin > 5
            simResults(period+1,1) = interp2(L,K,periodTrans(:,:),Ls(period),Ks(period));
        else
            simResults(period+1,1) = interp2(L,K,periodTrans(:,:),simResults(period,2),simResults(period,1));
        end
        
        if nargin > 4
            simResults(period+1,2) = interp2(L,K,periodLTrans(:,:),Ls(period),Ks(period));
        else
            simResults(period+1,2) = interp2(L,K,periodLTrans(:,:),simResults(period,2),simResults(period,1));
        end
        
        if sum(isnan(simResults(period+1)))~=0
            simResults = simResults(1:period,:);
            warning(['Out of range after ' num2str(period) ' periods']);
            return;
        end
    end

end

