function [ estimatesK, estimatesL, calcs ] = analyzeResults( results )
%ANALYZERESULTS Estimates the transition rule on the data and compares to
%that provided
    length = size(results.states,1);
    
    X = log([results.states(1:(length-1)),results.controls(1:(length-1))]);
    Y = log(results.states(2:length));
    Z = log(results.controls(2:length));
    
    shockVars = unique(results.shocks);
    
    estimatesK = zeros(size(shockVars,1),3);
    estimatesL = zeros(size(shockVars,1)*size(shockVars,1),3);
    calcs = zeros(size(shockVars,1),2);
    
    K = log(results.state.K * ones(1,size(results.state.L,1)));
    L = log(ones(size(results.state.K,1),1) * results.state.L');
    
    Kp = log(results.state.Kp(:,:,:,1));
    
    for shockVar = 1:size(shockVars)
        model = LinearModel.fit(X(results.shocks(1:(length-1),1)==shockVars(shockVar),:),Y(results.shocks(1:(length-1),1)==shockVars(shockVar),:)).Coefficients;
        estimatesK(shockVar,:) = model.Estimate.*(model.pValue < 0.05);
        
        for shockVar2 = 1:size(shockVars)
            indexes = (results.shocks(1:(length-1),1)==shockVars(shockVar))&((results.shocks(2:(length),1)==shockVars(shockVar2)));
            
            if(sum(indexes) > 10)
                model = LinearModel.fit(X(indexes,:),Z(indexes,1)).Coefficients;
                estimatesL((shockVar-1)*size(shockVars,1)+shockVar2,:) = model.Estimate.*(model.pValue < 0.05);
            end
        end        
        diffs = diff(Kp(:,:,shockVar),1,1)./diff(K,1,1);
        calcs(shockVar,1) = diffs(1,1);
        
        diffs = diff(Kp(:,:,shockVar),1,2)./diff(L,1,2);
        calcs(shockVar,2) = diffs(1,1);
    end
    
end

