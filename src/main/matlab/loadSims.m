function simResults = loadSims()
    global solDir stateDir simDir;
     
    try
        
        simResults.state.Z = readFormattedCSV([solDir '/' stateDir '/Z_agg.csv']);
        simResults.state.Kp = readFormattedCSV([solDir '/' stateDir '/Kp_agg.csv']);
        simResults.state.K = readFormattedCSV([solDir '/' stateDir '/K_agg.csv']);
        simResults.state.kp = readFormattedCSV([solDir '/' stateDir '/kp_ind.csv']);
        simResults.state.k = readFormattedCSV([solDir '/' stateDir '/k_ind.csv']);
        simResults.state.transProbs = readFormattedCSV([solDir '/' stateDir '/P_trans.csv']);
    catch e
        disp('Error Reading State');
    end
    
    try
        simResults.state.gradients = readFormattedCSV([solDir '/' stateDir '/gradients.csv']);
    catch e
        disp('No gradients');
    end
    
    try
        simResults.state.Lp = readFormattedCSV([solDir '/' stateDir '/Lp_agg.csv']);
        simResults.state.L = readFormattedCSV([solDir '/' stateDir '/L_agg.csv']);
    catch e
        disp('No labour');
    end
    
     try
        simResults.state.vars = readFormattedCSV([solDir '/' stateDir '/Var_agg.csv']);
    catch e
        disp('No vars');
    end
    
    try
        
        %% READ model solution state
        fullSimDir = [solDir '/' stateDir '/' simDir];
        simResults.states = readFormattedCSV([fullSimDir '/states.csv']);
        
        % Ditch the final 0
        simResults.states(length(simResults.states)) =  simResults.states(length(simResults.states)-1);
        
        simResults.shocks = readFormattedCSV([fullSimDir '/shocks.csv']);
        
        try
            simResults.controls = readFormattedCSV([fullSimDir '/controls.csv']);
            simResults.controls(length(simResults.controls)) =  simResults.controls(length(simResults.controls)-1);
        catch e
            disp('No controls')
        end
    catch e
        disp('No sim');
    end
    
    try
        simResults.ginis = readFormattedCSV([fullSimDir '/gini.csv']);
    catch e
        disp('No ginis');
    end
    
    try
        simResults.dens = readDensity(fullSimDir);
    catch e
        disp('No density');
    end
    
     try
        simResults.aggs = readFormattedCSV([fullSimDir '/aggs.csv']);   
    catch e
        disp('No aggregates');
    end
    
    try
        aggSimDir = [fullSimDir 'Agg'];
        simResults.aggStates = readFormattedCSV([aggSimDir '/states.csv']);   
    catch e
        disp('No aggregate sim');
    end
    
     
    try
        simResults.ind.k = readFormattedCSV([fullSimDir '/kInd.csv']);   
        simResults.ind.c = readFormattedCSV([fullSimDir '/cInd.csv']);   
        simResults.ind.kTilde = readFormattedCSV([fullSimDir '/kTilde.csv']);   
        simResults.ind.cTilde = readFormattedCSV([fullSimDir '/cTilde.csv']);   
    catch e
        disp('No individual sim');
    end
    
   
    
end