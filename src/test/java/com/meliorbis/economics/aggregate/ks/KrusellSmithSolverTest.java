package com.meliorbis.economics.aggregate.ks;

import static com.meliorbis.numerics.DoubleArrayFactories.createArray;
import static com.meliorbis.numerics.DoubleArrayFactories.createArrayOfSize;
import static com.meliorbis.numerics.IntArrayFactories.createIntArray;
import static com.meliorbis.numerics.IntArrayFactories.createIntArrayOfSize;

import java.util.Arrays;
import java.util.Collections;
import java.util.stream.IntStream;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.Test;

import com.meliorbis.economics.infrastructure.simulation.DiscretisedDistribution;
import com.meliorbis.economics.infrastructure.simulation.SimulationObserver;
import com.meliorbis.economics.infrastructure.simulation.SimulationResults;
import com.meliorbis.economics.infrastructure.simulation.Simulator;
import com.meliorbis.economics.model.Model;
import com.meliorbis.economics.model.ModelConfig;
import com.meliorbis.economics.model.ModelException;
import com.meliorbis.economics.model.State;
import com.meliorbis.numerics.generic.IntegerArray;
import com.meliorbis.numerics.generic.primitives.DoubleArray;

public class KrusellSmithSolverTest
{
	@SuppressWarnings("unchecked")
	@Test
	public void testSingleState() throws ModelException
	{
		Mockery context = new Mockery();
		
		Model<ModelConfig, State<ModelConfig>> model = context.mock(Model.class);
		ModelConfig config = context.mock(ModelConfig.class);
		Simulator<DiscretisedDistribution, Integer> simulator = context.mock(Simulator.class);
		
		
		
		State<ModelConfig> state = context.mock(State.class);

		IntegerArray<?> initialStates = createIntArray(1);
		IntegerArray<?> shockSequence = createIntArrayOfSize(10000);

		DoubleArray<?> shockLevels = createArrayOfSize(5);
		
		DiscretisedDistribution initDist = new DiscretisedDistribution();
		@SuppressWarnings("rawtypes")
		SimulationResults<DiscretisedDistribution, Integer> results = new SimulationResults();
		
		int[] offSet = new int[] {0};
		
		IntStream.range(0,10000).forEachOrdered(idx -> {
			int shock = idx % 5;
			shockSequence.set(shock, idx);
			
			if(shock == 0) {
				offSet[0] = (offSet[0]+1)%5;
			}
			

			double aggVal = shock + offSet[0];
			
			results.addPeriod(createIntArray(shock), 
					createArray(aggVal), 
					createArrayOfSize(0));
		});
		
		DoubleArray<?> aggTrans = createArrayOfSize(5,5,1);
		
		context.checking(new Expectations() {{
			allowing(config).getInitialExogenousStates(); will(returnValue(initialStates));
			allowing(config).getInitialSimState(); will(returnValue(initDist));
			allowing(config).getAggregateNormalisingStateCount(); will(returnValue(0));
			allowing(config).getAggregateExogenousStateCount(); will(returnValue(1));
			allowing(config).getAggregateExogenousStates(); will(returnValue(
					Collections.singletonList(shockLevels)));
			allowing(config).getAggregateEndogenousStateCount(); will(returnValue(1));
			allowing(config).getAggregateEndogenousStates(); will(returnValue(Arrays.asList(createArray(1,2,3,4,5))));
			allowing(config).getAggregateControlCount(); will(returnValue(0));
			
			oneOf(model).createAggregateVariableGrid(1); will(returnValue(aggTrans));
			
			oneOf(simulator).createShockSequence(initialStates, 10000, model);
				will(returnValue(shockSequence));
				
			oneOf(simulator).simulateShocks(
					with(initDist), 
					with(shockSequence),  
					with(model), 
					with(state), 
					with(any(SimulationObserver.class)));
				will(returnValue(results));
		}});
		
		KrusellSmithSolver<
		ModelConfig, 
		State<ModelConfig>, 
		Model<ModelConfig, State<ModelConfig>>, 
		DiscretisedDistribution> solver = new KrusellSmithSolver<ModelConfig, 
				State<ModelConfig>,
			Model<ModelConfig, State<ModelConfig>>,
			DiscretisedDistribution>(model, config, simulator);
		
		solver.calculateAggregatePolicies(state);
		
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void test() throws ModelException
	{
		Mockery context = new Mockery();
		
		Model<ModelConfig, State<ModelConfig>> model = context.mock(Model.class);
		ModelConfig config = context.mock(ModelConfig.class);
		Simulator<DiscretisedDistribution, Integer> simulator = context.mock(Simulator.class);
		
	
		
		State<ModelConfig> state = context.mock(State.class);

		IntegerArray<?> initialStates = createIntArray(1);
		IntegerArray<?> shockSequence = createIntArrayOfSize(10000);

		DoubleArray<?> shockLevels = createArrayOfSize(5);
		
		DiscretisedDistribution initDist = new DiscretisedDistribution();
		@SuppressWarnings("rawtypes")
		SimulationResults<DiscretisedDistribution, Integer> results = new SimulationResults();
		
		int[] offSet = new int[] {0,0,0};
		
		IntStream.range(0,10000).forEachOrdered(idx -> {
			int shock = idx % 5;
			shockSequence.set(shock, idx);
			
			if(shock == 0) {
				offSet[0] = (offSet[0]+1)%5;
				
				if(offSet[0] == 0) {
					offSet[1] = (offSet[1]+1)%5;
					

					if(offSet[1] == 0) {
						offSet[2] = (offSet[2]+1)%5;
					}
				}
				
			}
			

			double aggVal = shock + offSet[0];
			double aggVal2 = shock + offSet[1];
			double aggVal3 = shock + offSet[2];
			
			results.addPeriod(createIntArray(shock), 
					createArray(aggVal,aggVal2), 
					createArray(aggVal3,aggVal2, aggVal));
		});
		
		DoubleArray<?> aggTrans = createArrayOfSize(5,6,3,2,1,5,2);
		DoubleArray<?> aggControls = createArrayOfSize(5,6,3,2,1,5,3);
		
		context.checking(new Expectations() {{
			allowing(config).getInitialExogenousStates(); will(returnValue(initialStates));
			allowing(config).getInitialSimState(); will(returnValue(initDist));
			allowing(config).getAggregateNormalisingStateCount(); will(returnValue(0));
			allowing(config).getAggregateExogenousStateCount(); will(returnValue(1));
			allowing(config).getAggregateExogenousStates(); will(returnValue(
					Collections.singletonList(shockLevels)));
			allowing(config).getAggregateEndogenousStateCount(); will(returnValue(2));
			allowing(config).getAggregateEndogenousStates(); will(returnValue(Arrays.asList(createArray(1,2,3,4,5),createArray(2,3,4,5,6,7))));
			allowing(config).getAggregateControlCount(); will(returnValue(3));
			allowing(config).getAggregateControls(); will(returnValue(Arrays.asList(createArray(1,2,3),createArray(2,3),createArray(3))));
			
			
			oneOf(model).createAggregateVariableGrid(2); will(returnValue(aggTrans));
			oneOf(model).createAggregateVariableGrid(3); will(returnValue(aggControls));

			oneOf(simulator).createShockSequence(initialStates, 10000, model);
				will(returnValue(shockSequence));
				
			oneOf(simulator).simulateShocks(
					with(initDist), 
					with(shockSequence), 
					with(model), 
					with(state), 
					with(any(SimulationObserver.class)));
				will(returnValue(results));
		}});
		
		KrusellSmithSolver<
		ModelConfig, 
		State<ModelConfig>, 
		Model<ModelConfig, State<ModelConfig>>, 
		DiscretisedDistribution> solver = new KrusellSmithSolver<ModelConfig, 
				State<ModelConfig>,
			Model<ModelConfig, State<ModelConfig>>,
			DiscretisedDistribution>(model, config, simulator);
		
		solver.calculateAggregatePolicies(state);
	}

}
