/**
 * 
 */
package com.meliorbis.economics.aggregate.derivagg;

import static com.meliorbis.numerics.DoubleArrayFactories.createArrayOfSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.lib.concurrent.Synchroniser;
import org.junit.Before;
import org.junit.Test;

import com.meliorbis.economics.infrastructure.AbstractModel;
import com.meliorbis.economics.infrastructure.AbstractStateBase;
import com.meliorbis.economics.infrastructure.ModelConfigBase;
import com.meliorbis.economics.infrastructure.ModelConfigForTests;
import com.meliorbis.economics.infrastructure.simulation.DiscretisedDistribution;
import com.meliorbis.economics.infrastructure.simulation.DiscretisedDistributionSimulator;
import com.meliorbis.economics.infrastructure.simulation.SimState;
import com.meliorbis.economics.infrastructure.simulation.TransitionRecord;
import com.meliorbis.economics.model.Model;
import com.meliorbis.economics.model.ModelConfig;
import com.meliorbis.economics.model.ModelException;
import com.meliorbis.economics.model.StateWithControls;
import com.meliorbis.numerics.convergence.Criterion;
import com.meliorbis.numerics.convergence.DoubleCriterion;
import com.meliorbis.numerics.function.primitives.DoubleGridFunction;
import com.meliorbis.numerics.function.primitives.DoubleGridFunctionFactory;
import com.meliorbis.numerics.function.primitives.DoubleRectangularGridDomain;
import com.meliorbis.numerics.generic.impl.IntegerArray;
import com.meliorbis.numerics.generic.primitives.DoubleArray;
import com.meliorbis.numerics.generic.primitives.impl.DoubleArrayFunctions;
import com.meliorbis.numerics.test.ArrayAssert;
import com.meliorbis.numerics.test.NumericsTestBase;
import com.meliorbis.utils.Utils;

/**
 * Tests the derivative aggregation solver
 * 
 * @author Tobias Grasl
 */
public class DerivAggSolverNoControlsTest extends NumericsTestBase
{
	private static final double DELTA = 1e-10;
	Mockery _context = new Mockery() {{
		setThreadingPolicy(new Synchroniser());
	}};
	private DiscretisedDistributionSimulator _simulator;
	private AbstractModel<ModelConfig, DerivAggCalcState<ModelConfig>> _model;
	private ModelConfigBase _config;
	private DoubleArray<?> _indEndoStates;
	private DoubleArray<?> _probabilities;
	private DiscretisedDistribution _density;
	private StateForTest _state;

	private Function<int[], DiscretisedDistribution> _distProvider;
	private DiscretisedDistribution _targetDensity;
	private TransitionRecord<DiscretisedDistribution,Integer> _trans;
	public DoubleArray<?> _detAggAggDerivs;
	private DoubleRectangularGridDomain _policyDomain;
	
	abstract class StateForTest extends AbstractStateBase<ModelConfig> implements DerivAggCalcState<ModelConfig>
	{
		DoubleArray<?>[] _aggs = new DoubleArray<?>[2];
		DoubleArray<?>[] _futureAggs = new DoubleArray<?>[2];
		DoubleArray<?>[] _grads = new DoubleArray<?>[2];
		DoubleArray<?>[] _grad2s = new DoubleArray<?>[2];
		
		protected StateForTest(ModelConfig config_)
		{
			super(config_);
		}

		@Override
		public void setNamedArrays(Map<String, DoubleArray<?>> arraysByName_)
		{
			
		}

		@Override
		public void setDerivatives(int[] exoStates_, DoubleArray<?> currentAggs_, DoubleArray<?> futureAggs_, DoubleArray<?>... derivatives_)
		{
			int shock = exoStates_[0];
			
			_aggs[shock] = currentAggs_;
			_futureAggs[shock] = futureAggs_;
			_grads[shock] = derivatives_[0];
			_grad2s[shock] = derivatives_[1];
		}
	}; 
	
	abstract class StateWithControlsForTest extends StateForTest implements StateWithControls<ModelConfig>
	{
		private DoubleArray<?> _aggControlsPolicy;
		private DoubleArray<?> _indControlsPolicy;

		
		protected StateWithControlsForTest(ModelConfig config_)
		{
			super(config_);
		}

		@Override
		public void setCurrentControlsPolicy(DoubleArray<?> newPolicy_)
		{
			_aggControlsPolicy = newPolicy_;
		}
		
		

		@Override
		public void setIndividualControlsPolicyForSimulation(DoubleArray<?> newPolicy_)
		{
			_indControlsPolicy = newPolicy_;
		}

		@Override
		public DoubleArray<?> getCurrentControlsPolicy()
		{
			return _aggControlsPolicy;
		}

		@Override
		public DoubleArray<?> getIndividualControlsPolicy()
		{
			return _indControlsPolicy;
		}
		
		@Override
		public DoubleArray<?> getIndividualControlsPolicyForSimulation()
		{
			return _indControlsPolicy;
		}
		
		
	}
	
	private final class SolverForTest extends 
		DerivativeAggregationSolver<ModelConfig,DerivAggCalcState<ModelConfig>, 
												Model<ModelConfig,DerivAggCalcState<ModelConfig>>>
	{
		private DoubleArray<?> _aggDerivs;
		private DoubleArray<?> _aggAggDerivs;
		private DoubleArray<?> _thetaDerivs;
		private DoubleArray<?> _thetaDoubleDerivs;
		private DoubleArray<?> _detAggDerivs;
		private DoubleArray<?> _detAggAggDerivs;
		
		protected SolverForTest(
				Model<ModelConfig, DerivAggCalcState<ModelConfig>> model_, 
				ModelConfig config_)
		{
			super(model_, config_, DerivAggSolverNoControlsTest.this._simulator);
		}

		@Override
		public void initialise(DerivAggCalcState<ModelConfig> state_)
		{
			// Don't truncate values so we can see that it really works
			setConstrainToGrid(false);
		}

		@Override
		protected DoubleArray<?> deriveAggregationByIndividualState(
				int aggIndex_, 
				int indIndex_, 
				int[] aggExoIndex_, 
				double[] currentAggregates_, boolean current_)
		{
			return _aggDerivs;
		}

		@Override
		protected DoubleArray<?> deriveAggregationByAggregateState(
				int aggretationIndex_, 
				int stateIndex_, 
				int[] aggExoIndex_,
				double[] currentAggregates_)
		{
			return _aggAggDerivs;
		}

		@Override
		protected DoubleArray<?> deriveIndividualTransformationByTheta(
				int transformationIndex_, 
				int individualIndex_, 
				int[] aggExoIndex_,
				double[] currentAggregates_)
		{
			return _thetaDerivs;
		}

		@Override
		protected DoubleArray<?> doubleDeriveIndividualTransformationByTheta(
				int transformationIndex_, 
				int transformation2Index_,
				int individualIndex_, 
				int[] aggExoIndex_, 
				double[] currentAggregates_)
		{
			return _thetaDoubleDerivs;
		}

		@Override
		protected DoubleArray<?> deriveDeterminantAggregationByIndividualControl(
				int aggIndex_, 
				int indIndex_, 
				int[] aggExoIndex_,
				double[] currentAggregates_, 
				DoubleArray<?> controlPolicyAtAggs_)
		{
			return _detAggDerivs;
		}

		@Override
		protected DoubleArray<?> deriveDeterminantAggregationByAggregateState(
				int aggIndex_, 
				int aggState_, 
				int[] currentShockIndexes_,
				double[] aggregateStates_)
		{
			return _detAggAggDerivs;
		}
	};
	
	@Before
	public void setUp()
	{
		_simulator = _context.mock(DiscretisedDistributionSimulator.class);
		
		_config = new ModelConfigForTests();
		
		_config.setAggregateExogenousStates(createArray(1d,2d));
		_config.setAggregateNormalisingExogenousStates(createArray(1d,2d,3d));
		_config.setIndividualExogenousStates(createArray(1d,2d));

		_indEndoStates = createArray(0d,1d, 2d, 3d, 4d);
		_config.setIndividualEndogenousStates(_indEndoStates);
		_config.setAggregateEndogenousStates(_indEndoStates);
		
		_probabilities = createArray(2,2,2,2,1);
		
		_probabilities.modifying().add(1d/16d);
		
		_config.setExogenousStateTransiton(_probabilities);
		
	}
	
	private SolverForTest setUpNoControls() throws ModelException
	{
		_model = new AbstractModel<ModelConfig, DerivAggCalcState<ModelConfig>>()
		{
			@Override
			public boolean shouldUpdateAggregates(DerivAggCalcState<ModelConfig> state_)
			{
				return false;
			}

			@Override
			public DerivAggCalcState<ModelConfig> initialState()
			{
				return null;
			}

			@Override
			public double[] calculateAggregateStates(SimState simState_, IntegerArray currentAggShock_, DerivAggCalcState<ModelConfig> calcState_)
					throws ModelException
			{
				return new double[] {
						((DiscretisedDistribution)simState_)._density.mean(_config.getIndividualEndogenousStatesForSimulation().get(0),0)
						};
			}
			
			
		};
		_model.setConfig(_config);
		_model.initialise();
		
		// Uniform distribution
		DoubleArray<?> gradDens = createArray(5,2);
		
		_density = new DiscretisedDistribution();
		_density._density = gradDens;
		_density._overflowAverages = createArray(1,2);
		_density._overflowProportions= createArray(1,2);

		gradDens.modifying().add(1d/gradDens.numberOfElements());

		DoubleArray<?> gradDens2 = createArray(5,2);
		gradDens2.modifying().add(1d/gradDens2.numberOfElements());
		_targetDensity = new DiscretisedDistribution();
		_targetDensity._density = gradDens2;
		_targetDensity._overflowAverages = createArray(1,2);
		_targetDensity._overflowProportions= createArray(1,2);
		
		_state = new StateForTest(_config) {

			@Override
			public DiscretisedDistribution getDistributionForState(int[] shockLevels_)
			{
				return _distProvider.apply(shockLevels_);
			}

			@Override
			public Criterion getConvergenceCriterion()
			{
				return DoubleCriterion.of(0);
			}
			
		};
		
		DoubleArray<?> initialTrans = createArray(5,2,1);
		_state.setAggregateTransition(initialTrans);
		
		
		DoubleArray<?> periodStartGrid = createArray(5,2,5,2,1,1);
		periodStartGrid.modifying().across(0).add(_indEndoStates);
		
		_state.setStartOfPeriodStatesForSimulation(periodStartGrid);
		
		DoubleArray<?> eopStates = createArray(5,2);
		eopStates.modifying().across(0).add(_indEndoStates);
		_state.setEndOfPeriodStates(eopStates);
		
		_trans = new TransitionRecord<DiscretisedDistribution,Integer>();
		
		_trans.setResultingDist(_targetDensity);
		_trans.setStates(createArray(2d));
		_trans.setControls(createArray(0));
		
		_context.checking(new Expectations() {{
		
			allowing(_simulator).simulateTransition(
					with(_density), with(_model), with(_state), with(any(IntegerArray.class)), 
					with(any(IntegerArray.class)));
			will(returnValue(_trans));
			
		}});
		
		SolverForTest solver = new SolverForTest(_model, _config);
		solver._aggDerivs = createArray(5,2);
		solver._aggDerivs.modifying().add(1);
		
		solver._aggAggDerivs = createArray(5,2);
		
		solver._thetaDerivs = createArray(5,2);
		solver._thetaDerivs.modifying().across(0).add(_indEndoStates);

		solver._thetaDoubleDerivs = createArray(5,2);

		solver.initialise(_state);
		return solver;
	}
	
	
	@Test
	public void testCalc_dX_dTheta() throws ModelException
	{
		SolverForTest solver = setUpNoControls();
		DerivativeAggregationSolver<ModelConfig, 
		DerivAggCalcState<ModelConfig>, 
		Model<ModelConfig, DerivAggCalcState<ModelConfig>>>.DAValues daValues = 
		solver.new DAValues(new int[] {0}, _density, new double[] {1d}, new double[0]);
		
		daValues.dx_dTheta[0][0] = solver._thetaDerivs;
		daValues.dx_dTheta_of[0][0] = createArray(1,2); // No overflow

		daValues.dF_dx[0][0] = solver._aggDerivs;
		daValues.dF_dx_of[0][0] = createArray(1,2); // No overflow
	
		solver.calc_dX_dTheta(daValues);
		
		// Uniform dist coupled with proportional increase (0,1,...,4) means:
		// dX/dTheta = (4 * 5/2)/5 = 2
		assertEquals(2d, daValues.dX_dTheta[0],DELTA);
		
		DiscretisedDistribution dens = _density.createSameSized();
		
		// Non-uniform dist, only at level = 3 are there agents
		dens._density.at(3).fill(.75,.25);
		
		daValues = solver.new DAValues(new int[] {0}, dens, new double[] {1}, new double[0]);
		
		daValues.dx_dTheta[0][0] = solver._thetaDerivs;
		daValues.dx_dTheta_of[0][0] = createArray(1,2); // No overflow

		daValues.dF_dx[0][0] = solver._aggDerivs;
		daValues.dF_dx_of[0][0] = createArray(1,2); // No overflow
		
		solver.calc_dX_dTheta(daValues);
		
		assertEquals(3d, daValues.dX_dTheta[0],DELTA);
		
		// Test the overflow
		dens = _density.createSameSized();
		
		// Non-uniform dist, only at level = 3 are there agents
		dens._overflowProportions.fill(.75,.25);
		dens._overflowAverages.fill(8,12);
		
		daValues = solver.new DAValues(new int[] {0}, dens, new double[] {1}, new double[0]);
		
		daValues.dx_dTheta[0][0] = solver._thetaDerivs;
		daValues.dx_dTheta_of[0][0] = createArray(1,2).fill(8,12);
		
		daValues.dF_dx[0][0] = solver._aggDerivs;
		daValues.dF_dx_of[0][0] = createArray(1,2).fill(1);
		
		solver.calc_dX_dTheta(daValues);
		
		// 3/4 * 8 + 1/4 * 12
		assertEquals(9, daValues.dX_dTheta[0],DELTA);

		// Test a different trans - uniform!
		solver._thetaDerivs.fill(1d);
		
		daValues = solver.new DAValues(new int[] {0}, _density, new double[] {1}, new double[0]);
		
		daValues.dx_dTheta[0][0] = solver._thetaDerivs.copy().fill(1);
		daValues.dx_dTheta_of[0][0] = createArray(1,2).fill(1);
		
		daValues.dF_dx[0][0] = solver._aggDerivs;
		daValues.dF_dx_of[0][0] = createArray(1,2).fill(1);
		
		solver.calc_dX_dTheta(daValues);
	
		assertEquals(1, daValues.dX_dTheta[0],DELTA);
	}
	
	@Test
	public void testCollect_f() throws ModelException
	{
		SolverForTest solver = setUpNoControls();
		
		DerivativeAggregationSolver<ModelConfig, 
		DerivAggCalcState<ModelConfig>, 
		Model<ModelConfig, DerivAggCalcState<ModelConfig>>>.DAValues daValues = 
			solver.new DAValues(new int[] {0}, _density, new double[] {1d}, new double[0]);

		DoubleArray<?> transition = _model.createIndividualTransitionGrid();
		transition.fillDimensions(Utils.sequence(1d, 6d, 6),
				Utils.sequence(_model.getAggStochStateStart(), transition.numberOfDimensions()));
		
		_state.setIndividualPolicy(transition);
		solver.collect_f(daValues, _state);
		
		assertTrue(daValues.indStatesPolicy != null);
		
		createPolicyDomain();
		
		assertEquals(_policyDomain, daValues.indStatesPolicy.getDomain());
		assertEquals(1d, daValues.indStatesPolicy.getValues().reduce(iterator -> {
			
			double ret = 1d;
			while(iterator.hasNext()) {
				if(iterator.nextDouble() != 2) ret = 0d;
			}
			
			return ret;
		}), DELTA);
	}
	
	@Test
	public void testCalc_df() throws ModelException
	{
		SolverForTest solver = setUpNoControls();
		
		DerivativeAggregationSolver<ModelConfig, 
		DerivAggCalcState<ModelConfig>, 
		Model<ModelConfig, DerivAggCalcState<ModelConfig>>>.DAValues daValues = 
			solver.new DAValues(new int[] {0}, _density, new double[] {1d}, new double[0]);
		
		createPolicyDomain();
		
		DoubleArray<?> values = _policyDomain.createValueGrid(1);
		
		DoubleArray<?> indStates = _config.getIndividualEndogenousStatesForSimulation().get(0);
		values = values.across(0).add(
				(DoubleArray<?>)(indStates.multiply(indStates))).across(2).
					add(_config.getAggregateEndogenousStates().get(0).multiply(5));
		
		DoubleGridFunction policyFn = new DoubleGridFunctionFactory().createFunction(
				_policyDomain, values);
		
		daValues.indStatesPolicy = policyFn;
		
		solver.calc_df(daValues);
		
		
		// Since this is a first order solver, the derivative by agg states should be restricted
		// to those states
		assertEquals(_policyDomain.getNumberOfDimensions() - 1,daValues.df_dX[0].getDomain().getNumberOfDimensions());
		
		// Should be uniform deriv of 5 along X
		assertEquals(5,daValues.df_dX[0].getValues().first(), DELTA);
		assertEquals(5,daValues.df_dX[0].getValues().last(), DELTA);

		DoubleArray<?> gradient = DoubleArrayFunctions.gradWeigthedHarmonic(
				indStates.multiply(indStates), indStates, 0);
		
		ArrayAssert.assertEquals(gradient,daValues.df_dx[0].getValues().at(-1,0), DELTA);
		
		// Ind state deriv is anyway restricted
		assertEquals(_policyDomain.getNumberOfDimensions()-1,daValues.df_dX[0].getDomain().getNumberOfDimensions());
		
		/* Now test that second order does not restrict agg states, to allow for second order deriv
		 */
		solver.secondOrder(true);
		
		solver.calc_df(daValues);
		
		assertEquals(_policyDomain.getNumberOfDimensions(),daValues.df_dX[0].getDomain().getNumberOfDimensions());
		
		// Should be uniform deriv of 5 along x
		assertEquals(5,daValues.df_dX[0].getValues().first(), DELTA);
		assertEquals(5,daValues.df_dX[0].getValues().last(), DELTA);

		ArrayAssert.assertEquals(gradient,daValues.df_dx[0].getValues().at(-1,0), DELTA);
	}

	private void createPolicyDomain()
	{
		ArrayList<DoubleArray<?>> policyFunctionInputs = new ArrayList<DoubleArray<?>>();

		policyFunctionInputs.addAll(_config.getIndividualEndogenousStatesForSimulation());
		policyFunctionInputs.addAll(_config.getIndividualExogenousStates());
		policyFunctionInputs.addAll(_config.getAggregateEndogenousStates());
		policyFunctionInputs.addAll(_config.getAggregateControls());
		/** No agg shocks, these are already selected away **/
		
		_policyDomain = new DoubleGridFunctionFactory().createDomain(policyFunctionInputs);
	}
	
	@Test
	public void testCalc_dXp_dX() throws ModelException{
		
		SolverForTest solver = setUpNoControls();
		
		DiscretisedDistribution dens = _density.createSameSized();
		dens._density.fill(1d/dens._density.numberOfElements());
		
		SolverForTest.DAValues val = solver.new DAValues(new int[] {0}, dens, 
				new double[] {1d}, new double[] {});

		DiscretisedDistribution futureDens = _density.createSameSized();
		
		futureDens._density.fill(1d/futureDens._density.numberOfElements());

		ArrayList<DoubleArray<?>> policyFunctionInputs = new ArrayList<DoubleArray<?>>();

		policyFunctionInputs.addAll(_config.getIndividualEndogenousStatesForSimulation());
		policyFunctionInputs.addAll(_config.getIndividualExogenousStates());
		policyFunctionInputs.addAll(_config.getAggregateEndogenousStates());
		policyFunctionInputs.addAll(_config.getAggregateControls());
		
		int[] size = policyFunctionInputs.stream().mapToInt(x -> x.numberOfElements()).toArray();
		
		DoubleArray<?> values = createArray(size).add(2);
		
		
		
		val._futureDistribution = futureDens;
		val.df_dx[0] = new DoubleGridFunctionFactory().createFunction(
				policyFunctionInputs, values);
		val.df_dx_of[0] = createArrayOfSize(2).add(0);
		
		val.df_dX[0] = new DoubleGridFunctionFactory().createFunction(
				policyFunctionInputs, createArray(size).add(0));
		val.df_dX_of[0] = createArrayOfSize(2).add(0);
		
		val.dx_dX[0][0] = createArrayOfSize(_density._density.size()).add(3);
		val.dx_dX_of[0][0] = createArrayOfSize(2).add(0);
		
		val.future_dF_dx[0][0] = createArrayOfSize(_density._density.size()).add(5);
		val.future_dF_dx_of[0][0] = createArrayOfSize(2).add(0);
		
		// This should not be used since *current* aggregation not required here!
		val.dF_dx[0][0] = createArrayOfSize(_density._density.size()).add(4);
		val.dF_dx[0][0] = createArrayOfSize(2).add(0);
		
		solver.calc_dXp_dX(val);
		
		assertTrue(val.dXp_dX != null);
		assertEquals(30d, val.dXp_dX.get(0), DELTA);
		
		// Now try the case where there is also impact from X on individual decision
		val.df_dX[0].getValues().modifying().add(1);

		solver.calc_dXp_dX(val);
		assertEquals(35d, val.dXp_dX.get(0), DELTA);
		
	}
	
	@Test
	public void testCalc_dx_dX() throws ModelException
	{
		SolverForTest solver = setUpNoControls();
		
		DerivativeAggregationSolver<ModelConfig, 
		DerivAggCalcState<ModelConfig>, 
		Model<ModelConfig, DerivAggCalcState<ModelConfig>>>.DAValues daValues = 
			solver.new DAValues(new int[] {0}, _density, new double[] {1d}, new double[0]);
				

		daValues.dx_dTheta[0][0] = solver._thetaDerivs;
		daValues.dx_dTheta_of[0][0] = createArray(1,2); // No overflow
		
		daValues.dX_dTheta[0] = 2;

		solver.calc_dx_dX(daValues);
		
		
		ArrayAssert.assertEquals(daValues.dx_dTheta[0][0].divide(2d), daValues.dx_dX[0][0],DELTA);
		
		DiscretisedDistribution dens = _density.createSameSized();
		
		// Non-uniform dist, only at level = 3 are there agents
		dens._density.at(3).fill(.75,.25);
		
		daValues = solver.new DAValues(new int[] {0}, dens, new double[] {1}, new double[0]);
		
		daValues.dx_dTheta[0][0] = solver._thetaDerivs;
		daValues.dx_dTheta_of[0][0] = createArray(1,2); // No overflow
		
		daValues.dX_dTheta[0] = 3;
		
		solver.calc_dx_dX(daValues);
		
		ArrayAssert.assertEquals(daValues.dx_dTheta[0][0].divide(3d), daValues.dx_dX[0][0],DELTA);
		
		// Test the overflow
		 dens = _density.createSameSized();
		
		// Non-uniform dist, only at level = 3 are there agents
		dens._overflowProportions.fill(.75,.25);
		dens._overflowAverages.fill(8,12);
		
		daValues = solver.new DAValues(new int[] {0}, dens, new double[] {1}, new double[0]);
		
		daValues.dx_dTheta[0][0] = solver._thetaDerivs;
		daValues.dx_dTheta_of[0][0] = createArray(1,2); // No overflow
		
		daValues.dX_dTheta[0] = 9;
		
		solver.calc_dx_dX(daValues);

		ArrayAssert.assertEquals(daValues.dx_dTheta[0][0].divide(9d), daValues.dx_dX[0][0],DELTA);

		// Test a different trans - uniform!
		daValues = solver.new DAValues(new int[] {0}, _density, new double[] {1}, new double[0]);
		
		daValues.dx_dTheta[0][0] = solver._thetaDerivs.copy().fill(1);
		daValues.dx_dTheta_of[0][0] = createArray(1,2).fill(1);
		
		daValues.dF_dx[0][0] = solver._aggDerivs;
		daValues.dF_dx_of[0][0] = createArray(1,2).fill(1);
		
		daValues.dX_dTheta[0] = 1;
		
		solver.calc_dx_dX(daValues);
	
		ArrayAssert.assertEquals(daValues.dx_dTheta[0][0], daValues.dx_dX[0][0],DELTA);

	}
	
	@Test
	public void testNoControlsFirstOrder() throws ModelException
	{
		SolverForTest solver = setUpNoControls();
		
		DoubleArray<?> indTrans = _model.createIndividualTransitionGrid();
		indTrans.modifying().across(0).add(_indEndoStates);
		
		_state.setIndividualPolicy(indTrans);
		
		List<int[]> densitiesRequested = new ArrayList<>();
		
		_distProvider = idx -> {
			synchronized (densitiesRequested)
			{
				densitiesRequested.add(idx);
				return _density;
			}
		};
		
		_trans.setStates(createArray(1d));
		
		solver.updateAggregateTransition(_state);
		
		// Should have requested densities once for each shock
		assertEquals(2, densitiesRequested.size());
		
		// Don't care about order (and could differ depending on threading
		assertTrue(Arrays.equals(densitiesRequested.get(0), new int[] {0}) || 
				Arrays.equals(densitiesRequested.get(0), new int[] {1}));
		
		assertTrue(Arrays.equals(densitiesRequested.get(1), new int[] {0}) || 
				Arrays.equals(densitiesRequested.get(1), new int[] {1}));
		
		// df/dx=1, df/dX=0 => dX'/dX = 1, X'(1) = 2 => expect 1,2,3,4,5
		DoubleArray<?> expected = _model.createAggregateVariableGrid();
		expected.fillDimensions(Utils.sequence(1d,5d,5), 0);
		
		_context.assertIsSatisfied();
		ArrayAssert.assertEquals(expected, _state.getAggregateTransition(), MAX_ERROR);
		
		// Now df/dX is also 1, but only for the second agg shock level
		indTrans.at(-1,-1,-1,1).modifying().across(2).add(createArray(0d,1d,2d,3d,4d));
		
		solver.updateAggregateTransition(_state);
		
		// df/dx=1, df/dX=1 => dX'/dX = 2, X'(1) = 2 => expect 0,2,4,6,8
		expected.at(-1,1).fillDimensions(Utils.sequence(0d,8d,5), 0);
		ArrayAssert.assertEquals(expected, _state.getAggregateTransition(), MAX_ERROR);
		
		// Reset so df/dx is no longer constant
		indTrans = _model.createIndividualTransitionGrid();
		indTrans.modifying().across(0).add(_indEndoStates.multiply(_indEndoStates));
		indTrans.modifying().across(2).add(createArray(0d,1d,2d,3d,4d));
		_state.setIndividualPolicy(indTrans);
		
		// So 2->2 
		_trans.setStates(createArray(2d));
		
		solver.updateAggregateTransition(_state);
		
		// Difficult to predict this analytically because of the numeric gradients being calculated...
		DoubleArray<?> df_dx = DoubleArrayFunctions.gradWeigthedHarmonic(_indEndoStates.multiply(_indEndoStates), _indEndoStates, 0);
		double grad = df_dx.multiply(_indEndoStates).multiply(1d/10d).sum() + 1d;
		// df/dx=1, df/dX=1 => dX'/dX = 2, X'(3) = 3 => expect -3,-1,1,3,5
		expected.fillDimensions(_indEndoStates.subtract(2d).multiply(grad).add(2d),0);
		
		ArrayAssert.assertEquals(expected, _state.getAggregateTransition(), MAX_ERROR);
	}


	@Test
	public void testNoControlsSecondOrder() throws ModelException
	{
		SolverForTest solver = setUpNoControls();
		solver.secondOrder(true);
		DoubleArray<?> indTrans = _model.createIndividualTransitionGrid();
		indTrans.modifying().across(0).add(_indEndoStates);
		
		_state.setIndividualPolicy(indTrans);
		
		List<int[]> densitiesRequested = new ArrayList<>();
		
		_distProvider = idx -> {
			synchronized (densitiesRequested)
			{
				densitiesRequested.add(idx);
				return _density;
			}
		};
		
		_trans.setStates(createArray(1d));
		
		/* FIRST: With no underlying second-order variation it is the same as
		 * first order
		 */
		solver.updateAggregateTransition(_state);
		
		// Should have requested densities once for each shock
		assertEquals(2, densitiesRequested.size());
		
		// Don't care about order (and could differ depending on threading
		assertTrue(Arrays.equals(densitiesRequested.get(0), new int[] {0}) || 
				Arrays.equals(densitiesRequested.get(0), new int[] {1}));
		
		assertTrue(Arrays.equals(densitiesRequested.get(1), new int[] {0}) || 
				Arrays.equals(densitiesRequested.get(1), new int[] {1}));
		
		// df/dx=1, df/dX=0 => dX'/dX = 1, X'(1) = 2 => expect 1,2,3,4,5
		DoubleArray<?> expected = _model.createAggregateVariableGrid();
		expected.fillDimensions(Utils.sequence(1d,5d,5), 0);
		
		_context.assertIsSatisfied();
		ArrayAssert.assertEquals(expected, _state.getAggregateTransition(), MAX_ERROR);
		
		/* SECOND: d2f/dX2 = 1 
		 */
		final DoubleArray<?> diffs = createArray(0d,1d,3d,6d,10d);
		indTrans.at(-1,-1,-1,1).modifying().across(2).add(diffs);
		
		DoubleArray<?> grad = DoubleArrayFunctions.gradWeigthedHarmonic(diffs, _indEndoStates, 0);
		double dfdX = grad.get(1);
		
		DoubleArray<?> grad2 = DoubleArrayFunctions.gradWeigthedHarmonic(grad, _indEndoStates, 0);
		double d2fdX2 = grad2.get(1);
		
		// Check that first - order ignores the second deriv!
		solver.secondOrder(false);
		
		solver.updateAggregateTransition(_state);
		
		// df/dx=1, df/dX=1 => dX'/dX = 2, X'(1) = 2 => expect 0,2,4,6,8
		DoubleArray<?> expectedVals = _indEndoStates.add(-1d).multiply(dfdX+1).add(2);
		expected.at(-1,1).fillDimensions(expectedVals, 0);
		ArrayAssert.assertEquals(expected, _state.getAggregateTransition(), MAX_ERROR);
		
		solver.secondOrder(true);
		
		solver.updateAggregateTransition(_state);
		
		// Add the expected 2nd-order effect
		expectedVals.modifying().add(
				_indEndoStates.add(-1d).multiply(_indEndoStates.add(-1d)).
				multiply(d2fdX2/2));
		expected.at(-1,1).fillDimensions(expectedVals, 0);
		ArrayAssert.assertEquals(expected, _state.getAggregateTransition(), MAX_ERROR);	
	}
}
