/**
 * 
 */
package com.meliorbis.economics.aggregate.derivagg;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Map;
import java.util.function.Function;

import org.apache.commons.lang.ArrayUtils;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.lib.concurrent.Synchroniser;
import org.junit.Before;
import org.junit.Test;

import com.meliorbis.economics.infrastructure.AbstractModel;
import com.meliorbis.economics.infrastructure.AbstractStateBase;
import com.meliorbis.economics.infrastructure.ModelConfigBase;
import com.meliorbis.economics.infrastructure.ModelConfigForTests;
import com.meliorbis.economics.infrastructure.simulation.DiscretisedDistribution;
import com.meliorbis.economics.infrastructure.simulation.DiscretisedDistributionSimulator;
import com.meliorbis.economics.infrastructure.simulation.SimState;
import com.meliorbis.economics.infrastructure.simulation.TransitionRecord;
import com.meliorbis.economics.model.Model;
import com.meliorbis.economics.model.ModelConfig;
import com.meliorbis.economics.model.ModelException;
import com.meliorbis.economics.model.StateWithControls;
import com.meliorbis.numerics.convergence.Criterion;
import com.meliorbis.numerics.convergence.DoubleCriterion;
import com.meliorbis.numerics.generic.impl.IntegerArray;
import com.meliorbis.numerics.generic.primitives.DoubleArray;
import com.meliorbis.numerics.generic.primitives.impl.DoubleArrayFunctions;
import com.meliorbis.numerics.test.ArrayAssert;
import com.meliorbis.numerics.test.NumericsTestBase;
import com.meliorbis.utils.Utils;

/**
 * Tests the derivative aggregation solver
 * 
 * @author Tobias Grasl
 */
public class DerivAggSolverWithControlsTest extends NumericsTestBase
{
	private static final double DELTA = 1e-10;
	Mockery _context = new Mockery() {{
		setThreadingPolicy(new Synchroniser());
	}};
	private DiscretisedDistributionSimulator _simulator;
	private AbstractModel<ModelConfig, DerivAggCalcState<ModelConfig>> _model;
	private ModelConfigBase _config;
	private DoubleArray<?> _indEndoStates;
	private DoubleArray<?> _probabilities;
	private DiscretisedDistribution _density;
	private StateForTest _state;

	private Function<int[], DiscretisedDistribution> _distProvider;
	private DiscretisedDistribution _targetDensity;
	private TransitionRecord<DiscretisedDistribution,Integer> _trans;
	public DoubleArray<?> _detAggAggDerivs;
	
	abstract class StateForTest extends AbstractStateBase<ModelConfig> implements DerivAggCalcState<ModelConfig>
	{
		DoubleArray<?>[] _aggs = new DoubleArray<?>[2];
		DoubleArray<?>[] _futureAggs = new DoubleArray<?>[2];
		DoubleArray<?>[] _grads = new DoubleArray<?>[2];
		DoubleArray<?>[] _grad2s = new DoubleArray<?>[2];
		
		protected StateForTest(ModelConfig config_)
		{
			super(config_);
		}

		@Override
		public void setNamedArrays(Map<String, DoubleArray<?>> arraysByName_)
		{
			
		}

		@Override
		public void setDerivatives(int[] exoStates_, DoubleArray<?> currentAggs_, DoubleArray<?> futureAggs_, DoubleArray<?>... derivatives_)
		{
			int shock = exoStates_[0];
			
			_aggs[shock] = currentAggs_;
			_futureAggs[shock] = futureAggs_;
			_grads[shock] = derivatives_[0];
			_grad2s[shock] = derivatives_[1];
		}
	}; 
	
	abstract class StateWithControlsForTest extends StateForTest implements StateWithControls<ModelConfig>
	{
		private DoubleArray<?> _aggControlsPolicy;
		private DoubleArray<?> _indControlsPolicy;

		
		protected StateWithControlsForTest(ModelConfig config_)
		{
			super(config_);
		}

		@Override
		public void setCurrentControlsPolicy(DoubleArray<?> newPolicy_)
		{
			_aggControlsPolicy = newPolicy_;
		}
		
		

		@Override
		public void setIndividualControlsPolicyForSimulation(DoubleArray<?> newPolicy_)
		{
			_indControlsPolicy = newPolicy_;
		}

		@Override
		public DoubleArray<?> getCurrentControlsPolicy()
		{
			return _aggControlsPolicy;
		}

		@Override
		public DoubleArray<?> getIndividualControlsPolicy()
		{
			return _indControlsPolicy;
		}
		
		@Override
		public DoubleArray<?> getIndividualControlsPolicyForSimulation()
		{
			return _indControlsPolicy;
		}
		
		
	}
	
	private final class SolverForTest extends 
		DerivativeAggregationSolver<ModelConfig,DerivAggCalcState<ModelConfig>, 
												Model<ModelConfig,DerivAggCalcState<ModelConfig>>>
	{
		private DoubleArray<?> _aggDerivs;
		private DoubleArray<?> _aggAggDerivs;
		private DoubleArray<?> _thetaDerivs;
		private DoubleArray<?> _thetaDoubleDerivs;
		private DoubleArray<?> _detAggDerivs;
		private DoubleArray<?> _detAggAggDerivs;
		
		protected SolverForTest(
				Model<ModelConfig, DerivAggCalcState<ModelConfig>> model_, 
				ModelConfig config_)
		{
			super(model_, config_, DerivAggSolverWithControlsTest.this._simulator);
		}

		@Override
		public void initialise(DerivAggCalcState<ModelConfig> state_)
		{
			// Don't truncate values so we can see that it really works
			setConstrainToGrid(false);
		}

		@Override
		protected DoubleArray<?> deriveAggregationByIndividualState(
				int aggIndex_, 
				int indIndex_, 
				int[] aggExoIndex_, 
				double[] currentAggregates_, boolean current_)
		{
			return _aggDerivs;
		}

		@Override
		protected DoubleArray<?> deriveAggregationByAggregateState(
				int aggretationIndex_, 
				int stateIndex_, 
				int[] aggExoIndex_,
				double[] currentAggregates_)
		{
			return _aggAggDerivs;
		}

		@Override
		protected DoubleArray<?> deriveIndividualTransformationByTheta(
				int transformationIndex_, 
				int individualIndex_, 
				int[] aggExoIndex_,
				double[] currentAggregates_)
		{
			return _thetaDerivs;
		}

		@Override
		protected DoubleArray<?> doubleDeriveIndividualTransformationByTheta(
				int transformationIndex_, 
				int transformation2Index_,
				int individualIndex_, 
				int[] aggExoIndex_, 
				double[] currentAggregates_)
		{
			return _thetaDoubleDerivs;
		}

		@Override
		protected DoubleArray<?> deriveDeterminantAggregationByIndividualControl(
				int aggIndex_, 
				int indIndex_, 
				int[] aggExoIndex_,
				double[] currentAggregates_, 
				DoubleArray<?> controlPolicyAtAggs_)
		{
			return _detAggDerivs;
		}

		@Override
		protected DoubleArray<?> deriveDeterminantAggregationByAggregateState(
				int aggIndex_, 
				int aggState_, 
				int[] currentShockIndexes_,
				double[] aggregateStates_)
		{
			return _detAggAggDerivs;
		}
	};
	
	@Before
	public void setUp()
	{
		_simulator = _context.mock(DiscretisedDistributionSimulator.class);
		
		_config = new ModelConfigForTests();
		
		_config.setAggregateExogenousStates(createArray(1d,2d));
		_config.setAggregateNormalisingExogenousStates(createArray(1d));
		_config.setIndividualExogenousStates(createArray(1d,2d));

		_indEndoStates = createArray(0d,1d, 2d, 3d, 4d);
		_config.setIndividualEndogenousStates(_indEndoStates);
		_config.setAggregateEndogenousStates(_indEndoStates);
		
		_probabilities = createArray(2,2,2,2,1);
		
		_probabilities.modifying().add(1d/16d);
		
		_config.setExogenousStateTransiton(_probabilities);
		
	}
	
	private SolverForTest setUpControls() throws ModelException
	{
		_model = new AbstractModel<ModelConfig, DerivAggCalcState<ModelConfig>>()
		{

			@Override
			public boolean shouldUpdateAggregates(DerivAggCalcState<ModelConfig> state_)
			{
				return true;
			}

			@Override
			public DerivAggCalcState<ModelConfig> initialState()
			{
				return _state;
			}
			
			@Override
			public double[] calculateAggregateStates(SimState simState_, IntegerArray currentAggShock_, DerivAggCalcState<ModelConfig> calcState_)
					throws ModelException
			{
				return new double[] {
						((DiscretisedDistribution)simState_)._density.mean(_config.getIndividualEndogenousStatesForSimulation().get(0),0)
						};
			}
		};
		_config.setAggregateControls(createArray(.1,.2,.3));
		_model.setConfig(_config);
		_model.initialise();
		
		// Uniform distribution
		DoubleArray<?> gradDens = createArray(5,2);
		gradDens.modifying().add(1d/gradDens.numberOfElements());
		
		_density = new DiscretisedDistribution();
		_density._density = gradDens;
		_density._overflowAverages = createArray(1,2);
		_density._overflowProportions= createArray(1,2);
		
		DoubleArray<?> gradDens2 = createArray(5,2);
		gradDens2.modifying().add(1d/gradDens2.numberOfElements());
		_targetDensity = new DiscretisedDistribution();
		_targetDensity._density = gradDens2;
		_targetDensity._overflowAverages = createArray(1,2);
		_targetDensity._overflowProportions= createArray(1,2);
		
		_state = new StateWithControlsForTest(_config) {
			
			@Override
			public DiscretisedDistribution getDistributionForState(int[] shockLevels_)
			{
				return _distProvider.apply(shockLevels_);
			}

			@Override
			public Criterion getConvergenceCriterion()
			{
				// TODO Auto-generated method stub
				return DoubleCriterion.of(0);
			}
			
		};
		
		DoubleArray<?> initialTrans = createArray(5,3,2,1);
		_state.setAggregateTransition(initialTrans);

		DoubleArray<?> initialControlsPolicy = createArray(5,3,2,1);
		((StateWithControlsForTest)_state).setCurrentControlsPolicy(initialControlsPolicy);
		
		
		DoubleArray<?> periodStartGrid = createArray(5,2,5,3,2,1,1);
		periodStartGrid.modifying().across(0).add(_indEndoStates);
		
		_state.setStartOfPeriodStatesForSimulation(periodStartGrid);
		
		DoubleArray<?> eopStates = createArray(5,2);
		eopStates.modifying().across(0).add(_indEndoStates);
		_state.setEndOfPeriodStates(eopStates);
		
		_trans = new TransitionRecord<DiscretisedDistribution,Integer>();
		
		_trans.setResultingDist(_targetDensity);
		_trans.setStates(createArray(2d));
		_trans.setControls(createArray(.15d));
		
		_context.checking(new Expectations() {{
		
			allowing(_simulator).simulateTransition(
					with(_density), with(_model), with(_state), with(any(IntegerArray.class)), 
					with(any(IntegerArray.class)));
			will(returnValue(_trans));
		}});
		
		SolverForTest solver = new SolverForTest(_model, _config);
		solver._aggDerivs = createArray(5,2);
		solver._aggDerivs.modifying().add(1);
		
		solver._aggAggDerivs = createArray(5,2);
		
		solver._thetaDerivs = createArray(5,2);
		solver._thetaDerivs.modifying().across(0).add(_indEndoStates);

		solver._thetaDoubleDerivs = createArray(5,2);

		solver._detAggDerivs = createArray(5,2);
		solver._detAggDerivs.modifying().add(1);
		
		solver._detAggAggDerivs = createArray(5,2);
		
		solver.initialise(_state);
		return solver;
	}
	
	@Test
	public void testCalc_dc() throws ModelException
	{
		SolverForTest solver = setUpControls();
		
		DiscretisedDistribution dens = _density.createSameSized();
		
		// Non-uniform dist, only at level = 3 are there agents
		dens._overflowProportions.fill(.75,.25);
		dens._overflowAverages.fill(8,12);
		
		SolverForTest.DAValues val = solver.new DAValues(new int[] {0}, dens, 
				new double[] {1d}, new double[] {1});
		
		ArrayList<DoubleArray<?>> policyFunctionInputs = new ArrayList<DoubleArray<?>>();

		policyFunctionInputs.addAll(_config.getIndividualEndogenousStatesForSimulation());
		policyFunctionInputs.addAll(_config.getIndividualExogenousStates());
		policyFunctionInputs.addAll(_config.getAggregateEndogenousStates());
		policyFunctionInputs.addAll(_config.getAggregateControls());
		policyFunctionInputs.addAll(_config.getAggregateExogenousStates());
		policyFunctionInputs.addAll(_config.getAggregateNormalisingExogenousStates());
		
		int[] size = policyFunctionInputs.stream().mapToInt(x -> x.numberOfElements()).toArray();
		
		DoubleArray<?> statePol = createArray(size);
		DoubleArray<?> ctrlPol = createArray(ArrayUtils.add(size, 2));
		
		DoubleArray<?> indStates = _config.getIndividualEndogenousStatesForSimulation().get(0);
		ctrlPol.lastDimSlice(1).modifying().across(0).add(
				(DoubleArray<?>)(indStates.multiply(indStates))).across(2).
					add(_config.getAggregateEndogenousStates().get(0).multiply(5)).
					across(3).add(_config.getAggregateControls().get(0).multiply(3));
		ctrlPol.lastDimSlice(0).fill(3d);
//		val.indControlsPolicy = new DoubleGridFunctionFactory().createFunction(
//				policyFunctionInputs, values);

		_state.setIndividualPolicy(statePol);
		((StateWithControls<?>)_state).setIndividualControlsPolicyForSimulation(ctrlPol);
		
		solver.collect_f(val, _state);

		assertEquals(2,val.nIndControls);
		
		solver.calc_dc(val);
		
		assertEquals(0, val.dc_dC[0][0].get(0), DELTA);
		assertEquals(0, val.dc_dX[0][0].get(0), DELTA);
		assertEquals(0, val.dc_dx[0][0].get(0), DELTA);

		assertEquals(0, val.dc_dC_of[0][0].get(0), DELTA);
		assertEquals(0, val.dc_dX_of[0][0].get(0), DELTA);
		assertEquals(0, val.dc_dx_of[0][0].get(0), DELTA);
		
		// Arbitrary points, should be constant
		assertEquals(3, val.dc_dC[1][0].get(3), DELTA);
		assertEquals(5, val.dc_dX[1][0].get(7), DELTA);
		assertEquals(3, val.dc_dC_of[1][0].get(0), DELTA);
		assertEquals(5, val.dc_dX_of[1][0].get(0), DELTA);
		
		// This one is quadratic, so not constant
		assertEquals(1, val.dc_dx[1][0].get(0), DELTA);
		
		DoubleArray<?> grad = 
				DoubleArrayFunctions.gradWeigthedHarmonic(
						_indEndoStates.multiply(_indEndoStates), _indEndoStates, 0);
		DoubleArray<?> diff = DoubleArrayFunctions.diff(grad, 0);
		
		assertEquals(4*diff.get(3) + grad.last(), val.dc_dx_of[1][0].get(0), DELTA);
		assertEquals(8*diff.get(3) + grad.last(), val.dc_dx_of[1][0].get(1), DELTA);
	}
	
	@Test
	public void testCalc_dC_dX() throws ModelException
	{
		SolverForTest solver = setUpControls();
		
		SolverForTest.DAValues val = solver.new DAValues(new int[] {0}, _density, 
				new double[] {1d}, new double[] {1});
		
		val.nIndControls = 2;
		
		int[] size = new int[] {5,2};
		int[] size_of = new int[] {1,2};
		
		int[] aggSize = new int[] {5,2};
		int[] aggSize_of = new int[] {1,2};

		val.dc_dC = new DoubleArray[2][1];
		val.dc_dC[0][0] = createArray(size);
		val.dc_dC[1][0] = createArray(size);
		
		val.dc_dC_of = new DoubleArray[2][1];
		val.dc_dC_of[0][0] = createArray(size_of);
		val.dc_dC_of[1][0] = createArray(size_of);
		
		val.dc_dX = new DoubleArray[2][1];
		val.dc_dX[0][0] = createArray(size);
		val.dc_dX[1][0] = createArray(size);
		
		val.dc_dX_of = new DoubleArray[2][1];
		val.dc_dX_of[0][0] = createArray(size_of);
		val.dc_dX_of[1][0] = createArray(size_of);
		
		val.dc_dx = new DoubleArray[2][1];
		val.dc_dx[0][0] = createArray(size);
		val.dc_dx[1][0] = createArray(size);

		val.dc_dx_of = new DoubleArray[2][1];
		val.dc_dx_of[0][0] = createArray(size_of);
		val.dc_dx_of[1][0] = createArray(size_of);
		
		val.dc_dx[0][0].fill(1d);
		val.dc_dx[1][0].fill(1d);
		
		val.dx_dX[0][0] = createArray(aggSize);
		val.dx_dX[0][0].fill(1d);
		
		val.dx_dX_of[0][0] = createArray(aggSize_of);
		
		val.controlAggregationDerivatives = new DoubleArray<?>[1][2];
		val.controlAggregationDerivatives_of = new DoubleArray<?>[1][2];
		val.controlAggregationDerivatives[0][0] = createArray(aggSize);
		val.controlAggregationDerivatives_of[0][0] = createArray(aggSize_of);
		
		val.controlAggregationDerivatives[0][0].fill(1);
		
		val.controlAggregationDerivatives[0][1] = createArray(aggSize);
		val.controlAggregationDerivatives_of[0][1] = createArray(aggSize_of);
	
		val.controlAggregationByAggDerivatives = new DoubleArray<?>[1][2];
		val.controlAggregationByAggDerivatives_of = new DoubleArray<?>[1][2];
		val.controlAggregationByAggDerivatives[0][0] = createArray(aggSize);
		val.controlAggregationByAggDerivatives_of[0][0] = createArray(aggSize_of);
		val.controlAggregationByAggDerivatives[0][1] = createArray(aggSize);
		val.controlAggregationByAggDerivatives_of[0][1] = createArray(aggSize_of);
		
		solver.calc_dC_dX(val);
		
		// Second control does not impact agg, so 1
		// THIS ONE ALSO CHECKS EXPECTED ARRAY SIZE...
		ArrayAssert.assertEquals(createArray(1,1).fill(1),val.dC_dX, DELTA);
		
		val.controlAggregationDerivatives[0][1].fill(1);
		
		solver.calc_dC_dX(val);
		
		assertEquals(2,val.dC_dX.first(), DELTA);
		
		// Double impact of state on second ind control
		val.dc_dx[1][0].fill(2d);
		
		solver.calc_dC_dX(val);
		
		assertEquals(3,val.dC_dX.first(), DELTA);
		
		// (Only) One of the ind controls changes with C
		val.dc_dC[0][0].fill(-1);
		
		solver.calc_dC_dX(val);
		
		assertEquals(1.5,val.dC_dX.first(), DELTA);
		
		// With direct impact from aggregate state on ind control
		val.dc_dX[0][0].fill(-1.5);
		val.dc_dX[1][0].fill(-1.5);
		
		solver.calc_dC_dX(val);
		
		assertEquals(0,val.dC_dX.first(), DELTA);
		
	}
		
	@Test
	public void testControlsFirstOrder() throws ModelException
	{
		SolverForTest solver = setUpControls();
		
		DoubleArray<?> indTrans = _model.createIndividualTransitionGrid();
		indTrans.modifying().across(0).add(_indEndoStates);
		
		_state.setIndividualPolicy(indTrans);
		
		DoubleArray<?> indControls = _model.createIndividualVariableGrid();
		indControls.modifying().across(0).add(_indEndoStates);
		
		((StateWithControlsForTest) _state)._indControlsPolicy = indControls;		
		
		_distProvider = idx -> {
			return _density;
		};
		
		_trans.setStates(createArray(1d));
		
		solver.updateAggregateTransition(_state);
		
		// df/dx=1, df/dX=0 => dX'/dX = 1, X'(1) = 2 => expect 1,2,3,4,5
		DoubleArray<?> expected = _model.createAggregateVariableGrid();
		expected.fillDimensions(Utils.sequence(1d,5d,5), 0);
		
		_context.assertIsSatisfied();
		ArrayAssert.assertEquals(expected, _state.getAggregateTransition(), MAX_ERROR);
		
		// Now df/dX is also 1, but only for the second agg shock level
		indTrans.at(-1,-1,-1,-1,1).modifying().across(2).add(createArray(0d,1d,2d,3d,4d));
		
		solver.updateAggregateTransition(_state);
		
		// df/dx=1, df/dX=1 => dX'/dX = 2, X'(1) = 2 => expect 0,2,4,6,8
		expected.at(-1,-1,1).fillDimensions(Utils.sequence(0d,8d,5), 0);
		ArrayAssert.assertEquals(expected, _state.getAggregateTransition(), MAX_ERROR);
		
		// Reset so df/dx is no longer constant
		indTrans = createArray(5,2,5,3,2,1,1);
		indTrans.modifying().across(0).add(_indEndoStates.multiply(_indEndoStates));
		indTrans.modifying().across(2).add(createArray(0d,1d,2d,3d,4d));
		_state.setIndividualPolicy(indTrans);
		
		// So 2->2 
		_trans.setStates(createArray(2d));
		
		solver.updateAggregateTransition(_state);
		
		// Difficult to predict this analytically because of the numeric gradients being calculated...
		DoubleArray<?> df_dx = DoubleArrayFunctions.gradWeigthedHarmonic(_indEndoStates.multiply(_indEndoStates), _indEndoStates, 0);
		double grad = df_dx.multiply(_indEndoStates).multiply(1d/10d).sum() + 1d;
		// df/dx=1, df/dX=1 => dX'/dX = 2, X'(3) = 3 => expect -3,-1,1,3,5
		expected.fillDimensions(_indEndoStates.subtract(2d).multiply(grad).add(2d),0);
		
		ArrayAssert.assertEquals(expected, _state.getAggregateTransition(), MAX_ERROR);
	}
}
