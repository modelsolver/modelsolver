package com.meliorbis.economics.infrastructure;

import static com.meliorbis.numerics.DoubleArrayFactories.*;
import static org.junit.Assert.assertArrayEquals;

import org.jmock.Expectations;
import org.jmock.auto.Mock;
import org.jmock.integration.junit4.JUnitRuleMockery;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.meliorbis.economics.model.ModelConfig;
import com.meliorbis.economics.model.State;
import com.meliorbis.economics.model.StateWithControls;
import com.meliorbis.numerics.generic.primitives.DoubleArray;
import com.meliorbis.numerics.test.ArrayAssert;

/**
 * Tests for the conditionalExpectation method
 * 
 * @author Tobias Grasl
 */
public class TestConditionalExpectationControls extends Base
{
	@Rule public JUnitRuleMockery _mockery = new JUnitRuleMockery();
	@Mock public StateWithControls<ModelConfig> _state;
	private AbstractModel<ModelConfig, State<ModelConfig>> _model;
	
	@Before public void initBase() {
		System.setProperty("com.meliorbis.numerics.threads", "1");
		
		ModelConfigForTests config = new ModelConfigForTests();
		double[] values_ =
		{ 1, 2, 3, 4 };
		
		config.setAggregateEndogenousStates(createArray(values_));
		double[] values_1 =
		{ 10, 11 };
		config.setAggregateControls(createArray(values_1));
		double[] values_2 =
		{ 3, 4, 5 };
		config.setControlsAffectingExpectations(0);

		config.setAggregateExogenousStates(createArray(values_2));
		double[] values_3 =
		{ 5, 6 };
		config.setAggregateNormalisingExogenousStates(createArray(values_3));
		double[] values_4 =
		{ 1, 2, 3, 4, 5 };
		config.setIndividualEndogenousStates(createArray(values_4));
		double[] values_5 =
		{ 1, 2 };
		config.setIndividualExogenousStates(createArray(values_5));
		
		
		_model = new AbstractModel<ModelConfig, State<ModelConfig>>()
		{

			@Override
			public boolean shouldUpdateAggregates(State<ModelConfig> state_)
			{
				return false;
			}

			@Override
			public State<ModelConfig> initialState()
			{
				return null;
			}
		};
		
		_model.setConfig(config);
		_model.initialise();
	}
	
	@Test public void indTransVarybyCurShockAndControl()
	{
		int[] size_ =
		{ 3, 3, 2, 4, 2, 1 };
		DoubleArray<?> expectedAggStates = createArrayOfSize(size_);
		int[] size_1 =
		{ 3, 3, 2, 4, 2, 1 };
		DoubleArray<?> expectedControls = createArrayOfSize(size_1);
		double[] values_ =
		{ 1, 2, 3 };
		
		// Depending on the current shock, the future aggregate is either one two or three
		expectedAggStates.fillDimensions((DoubleArray<?>) createArray(values_), 0);
		// Also vary it by by current control
		expectedAggStates.modifying().across(4).add(createArray(0,1));
				
		double[] values_1 = { 10, 10.5, 11 };
		
				
		expectedControls.fillDimensions((DoubleArray<?>) createArray(values_1), 0);
		
		_mockery.checking(new Expectations() {{
			oneOf(_state).getExpectedAggregateStates(); will(returnValue(expectedAggStates));
			oneOf(_state).getExpectedAggregateControls(); will(returnValue(expectedControls));
		}});
		
		DoubleArray<?> input = _model.createIndividualTransitionGrid(2);
		double[] values_2 =
		{ 2, 3, 4, 5 };

		// Add one to the aggregate...
		input.lastDimSlice(0).fillDimensions(createArray(values_2), 2);
		double[] values_3 =
		{ 3, 4, 5, 6 };
		
		// Add two to the aggregate...
		input.lastDimSlice(1).fillDimensions(createArray(values_3), 2);
		double[] values_4 =
		{ 1, 2 };
		
		// The individual variable varies in the control
		input.modifying().across(3).multiply(createArray(values_4));
		DoubleArray<?> result = _model.conditionalExpectation(input, _state);
		
		final DoubleArray<?> expected = _model.createIndividualExpectationGrid(2);
		assertArrayEquals("Size should be individual expectations size",expected.size(), result.size());
		double[] values_5 =
		{ 2, 3, 4 };

		// Should vary along the current shock dimension
		expected.lastDimSlice(0).fillDimensions(createArray(values_5),0);
		double[] values_6 =
		{ 3, 4, 5 };
		expected.lastDimSlice(1).fillDimensions(createArray(values_6),0);
		double[] values_7 =
		{ 1, 1.5, 2 };
		
		// Should vary by current control
		expected.modifying().across(4).add(createArray(0,1));

		/* The individual variable varies in the *future* control!
		 * The future control varies with the current shock!
		 * So the expected individual variable varies with the current shock
		 */
		expected.modifying().across(0).multiply(createArray(values_7));
		
		ArrayAssert.assertEquals(expected, result,1e-10);
	}
	
	@Test public void aggVarVarybyCurShockAndState()
	{
		int[] size_ =
		{ 3, 3, 2, 4, 2, 1 };
		DoubleArray<?> expectedAggStates = createArrayOfSize(size_);
		int[] size_1 =
		{ 3, 3, 2, 4, 2, 1 };
		DoubleArray<?> expectedControls = createArrayOfSize(size_1);
		double[] values_ =
		{ 1, 2, 3 };
		
		// Depending on the current shock, the future aggregate is either one two or three
		expectedAggStates.fillDimensions((DoubleArray<?>) createArray(values_), 0);
		
		// Also vary it by by current control
		expectedAggStates.modifying().across(4).add(createArray(0,1));
		
		double[] values_1 =
		{ 10, 10.5, 11 };
		expectedControls.fillDimensions((DoubleArray<?>) createArray(values_1), 0);
		
		_mockery.checking(new Expectations() {{
			oneOf(_state).getExpectedAggregateStates(); will(returnValue(expectedAggStates));
			oneOf(_state).getExpectedAggregateControls(); will(returnValue(expectedControls));
		}});
		
		DoubleArray<?> input = _model.createAggregateVariableGrid(2);
		
		// Add one to the aggregate...
		double[] values_2 = { 2, 3, 4, 5 };
		input.lastDimSlice(0).fillDimensions(createArray(values_2), 0);
		
		// Add two to the aggregate...
		double[] values_3 = { 3, 4, 5, 6 };
		input.lastDimSlice(1).fillDimensions(createArray(values_3), 0);
		
		// The current variable varies in the shock
		double[] values_4 = { 1, 2, 3 };
		input.modifying().across(2).multiply(createArray(values_4));
		
		DoubleArray<?> result = _model.conditionalExpectation(input, _state);
		
		final DoubleArray<?> expected = _model.createAggregateExpectationGrid(2);
		assertArrayEquals("Size should be aggregate expectations size",expected.size(), result.size());
		
		// Should vary along the current shock dimension, due to current state variation
		double[] values_5 ={ 2, 3, 4 };
		expected.lastDimSlice(0).fillDimensions(createArray(values_5),0);
		
		double[] values_6 ={ 3, 4, 5 };
		expected.lastDimSlice(1).fillDimensions(createArray(values_6),0);
		
		// Should vary by current control
		expected.modifying().across(4).add(createArray(0,1));

		/* Since the current variable varies in the current shock, the expected variable
		 * must vary in the future shock
		 */
		double[] values_7 ={ 1, 2, 3 };
		expected.modifying().across(1).multiply(createArray(values_7));
		
		ArrayAssert.assertEquals(expected, result,1e-10);
	}
//	
//	@Test public void indTransVarybyFutShock()
//	{
//		DoubleArray<?> expectedAggStates = createArrayBySize(3,3,2,4,1);
//		
//		// Depending on the current shock, the future aggregate is either one two or three
//		expectedAggStates.fillDimensions(createArray(1,2,3), 1);
//		
//		_mockery.checking(new Expectations() {{
//			oneOf(_state).getExpectedAggregateStates(); will(returnValue(expectedAggStates));
//		}});
//		
//		DoubleArray<?> input = _model.createIndividualTransitionGrid(2);
//
//		// Add one to the aggregate...
//		input.lastDimSlice(0).fillDimensions(createArray(2,3,4,5), 2);
//		
//		// Add two to the aggregate...
//		input.lastDimSlice(1).fillDimensions(createArray(3,4,5,6), 2);
//		
//		DoubleArray<?> result = _model.conditionalExpectation(input, _state);
//		
//		final DoubleArray<?> expected = _model.createIndividualExpectationGrid(2);
//		assertArrayEquals("Size should be individual expectations size",expected.size(), result.size());
//
//		// Should vary along the current shock dimension
//		expected.lastDimSlice(0).fillDimensions(createArray(2,3,4),1);
//		expected.lastDimSlice(1).fillDimensions(createArray(3,4,5),1);
//		
//		ArrayAssert.assertEquals(expected, result,1e-10);
//	}
	
//	@Test public void indTransVarybyFutPermShock()
//	{
//		DoubleArray<?> expectedAggStates = createArrayBySize(3,3,2,4,1);
//		
//		// Depending on the current shock, the future aggregate is either one two or three
//		expectedAggStates.fillDimensions(createArray(2,3), 2);
//		
//		_mockery.checking(new Expectations() {{
//			oneOf(_state).getExpectedAggregateStates(); will(returnValue(expectedAggStates));
//		}});
//		
//		DoubleArray<?> input = _model.createIndividualTransitionGrid(2);
//
//		// Add one to the aggregate...
//		input.lastDimSlice(0).fillDimensions(createArray(2,3,4,5), 2);
//		
//		// ...also allow the individual trans to vary by the perm shock...
//		input.lastDimSlice(0).across(4).modifying().add(createArray(0,.5));
//		
//		// Add two to the aggregate...
//		input.lastDimSlice(1).fillDimensions(createArray(3,4,5,6), 2);
//		
//		DoubleArray<?> result = _model.conditionalExpectation(input, _state);
//		
//		final DoubleArray<?> expected = _model.createIndividualExpectationGrid(2);
//		assertArrayEquals("Size should be individual expectations size",expected.size(), result.size());
//
//		// Should vary along the perm shock dimension, with an additional
//		// variation from the individual variation along that dim (the .5)
//		expected.lastDimSlice(0).fillDimensions(createArray(3,4.5),2);
//		expected.lastDimSlice(1).fillDimensions(createArray(4,5),2);
//		
//		ArrayAssert.assertEquals(expected, result,1e-10);
//	}
//	
//	@Test public void indTransVarybyAggEndoState()
//	{
//		DoubleArray<?> expectedAggStates = createArrayBySize(3,3,2,4,1);
//		
//		// Future state = current state + 1
//		expectedAggStates.fillDimensions(createArray(2,3,4,5), 3);
//		
//		_mockery.checking(new Expectations() {{
//			oneOf(_state).getExpectedAggregateStates(); will(returnValue(expectedAggStates));
//		}});
//		
//		DoubleArray<?> input = _model.createIndividualTransitionGrid(2);
//
//		// Add one to the aggregate...
//		input.lastDimSlice(0).fillDimensions(createArray(2,3,4,5), 2);
//		
//		// ...also allow the individual trans to vary by the perm shock...
//		input.lastDimSlice(0).across(4).modifying().add(createArray(0,.5));
//		
//		// Add two to the aggregate...
//		input.lastDimSlice(1).fillDimensions(createArray(3,4,5,6), 2);
//		
//		DoubleArray<?> result = _model.conditionalExpectation(input, _state);
//		
//		final DoubleArray<?> expected = _model.createIndividualExpectationGrid(2);
//		assertArrayEquals("Size should be individual expectations size",expected.size(), result.size());
//
//		// Should vary along the perm shock dimension, with an additional
//		// variation from the individual variation along that dim (the .5)
//		expected.lastDimSlice(0).fillDimensions(createArray(3,4,5,6),3);
//		expected.lastDimSlice(0).across(2).modifying().add(createArray(0,.5));
//		expected.lastDimSlice(1).fillDimensions(createArray(4,5,6,7),3);
//		
//		ArrayAssert.assertEquals(expected, result,1e-10);
//	}
}
