package com.meliorbis.economics.infrastructure;

import static com.meliorbis.numerics.DoubleArrayFactories.createArrayOfSize;
import static org.junit.Assert.assertArrayEquals;

import org.jmock.Expectations;
import org.jmock.auto.Mock;
import org.jmock.integration.junit4.JUnitRuleMockery;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.meliorbis.economics.model.ModelConfig;
import com.meliorbis.economics.model.State;
import com.meliorbis.numerics.DoubleArrayFactories;
import com.meliorbis.numerics.generic.primitives.DoubleArray;
import com.meliorbis.numerics.test.ArrayAssert;

/**
 * Tests for the conditionalExpectation method
 * 
 * @author Tobias Grasl
 */
public class TestConditionalExpectationNoControls extends Base
{
	@Rule public JUnitRuleMockery _mockery = new JUnitRuleMockery();
	@Mock public State<ModelConfig> _state;
	private AbstractModel<ModelConfig, State<ModelConfig>> _model;
	
	@Before public void initBase() {
		
		ModelConfigForTests config = new ModelConfigForTests();
		double[] values_ =
		{ 1, 2, 3, 4 };
		
		config.setAggregateEndogenousStates(DoubleArrayFactories.createArray(values_));
		double[] values_1 =
		{ 3, 4, 5 };
		config.setAggregateExogenousStates(DoubleArrayFactories.createArray(values_1));
		double[] values_2 =
		{ 5, 6 };
		config.setAggregateNormalisingExogenousStates(DoubleArrayFactories.createArray(values_2));
		double[] values_3 =
		{ 1, 2, 3, 4, 5 };
		config.setIndividualEndogenousStates(DoubleArrayFactories.createArray(values_3));
		double[] values_4 =
		{ 1, 2 };
		config.setIndividualExogenousStates(DoubleArrayFactories.createArray(values_4));
		
		
		_model = new AbstractModel<ModelConfig, State<ModelConfig>>()
		{

			@Override
			public boolean shouldUpdateAggregates(State<ModelConfig> state_)
			{
				return false;
			}

			@Override
			public State<ModelConfig> initialState()
			{
				return null;
			}
		};
		
		_model.setConfig(config);
		_model.initialise();
	}
	
	@Test public void indTransVarybyCurShock()
	{
		int[] size_ =
		{ 3, 3, 2, 4, 1 };
		DoubleArray<?> expectedAggStates = createArrayOfSize(size_);
		double[] values_ =
		{ 1, 2, 3 };
		
		// Depending on the current shock, the future aggregate is either one two or three
		expectedAggStates.fillDimensions((DoubleArray<?>) DoubleArrayFactories.createArray(values_), 0);
		
		_mockery.checking(new Expectations() {{
			oneOf(_state).getExpectedAggregateStates(); will(returnValue(expectedAggStates));
		}});
		
		DoubleArray<?> input = _model.createIndividualTransitionGrid(2);
		double[] values_1 =
		{ 2, 3, 4, 5 };

		// Add one to the aggregate...
		input.lastDimSlice(0).fillDimensions(DoubleArrayFactories.createArray(values_1), 2);
		double[] values_2 =
		{ 3, 4, 5, 6 };
		
		// Add two to the aggregate...
		input.lastDimSlice(1).fillDimensions(DoubleArrayFactories.createArray(values_2), 2);
		
		DoubleArray<?> result = _model.conditionalExpectation(input, _state);
		
		final DoubleArray<?> expected = _model.createIndividualExpectationGrid(2);
		assertArrayEquals("Size should be individual expectations size",expected.size(), result.size());
		double[] values_3 =
		{ 2, 3, 4 };

		// Should vary along the current shock dimension
		expected.lastDimSlice(0).fillDimensions(DoubleArrayFactories.createArray(values_3),0);
		double[] values_4 =
		{ 3, 4, 5 };
		expected.lastDimSlice(1).fillDimensions(DoubleArrayFactories.createArray(values_4),0);
		
		ArrayAssert.assertEquals(expected, result,1e-10);
	}
	
	@Test public void indTransVarybyFutShock()
	{
		int[] size_ =
		{ 3, 3, 2, 4, 1 };
		DoubleArray<?> expectedAggStates = createArrayOfSize(size_);
		double[] values_ =
		{ 1, 2, 3 };
		
		// Depending on the current shock, the future aggregate is either one two or three
		expectedAggStates.fillDimensions((DoubleArray<?>) DoubleArrayFactories.createArray(values_), 1);
		
		_mockery.checking(new Expectations() {{
			oneOf(_state).getExpectedAggregateStates(); will(returnValue(expectedAggStates));
		}});
		
		DoubleArray<?> input = _model.createIndividualTransitionGrid(2);
		double[] values_1 =
		{ 2, 3, 4, 5 };

		// Add one to the aggregate...
		input.lastDimSlice(0).fillDimensions(DoubleArrayFactories.createArray(values_1), 2);
		double[] values_2 =
		{ 3, 4, 5, 6 };
		
		// Add two to the aggregate...
		input.lastDimSlice(1).fillDimensions(DoubleArrayFactories.createArray(values_2), 2);
		
		DoubleArray<?> result = _model.conditionalExpectation(input, _state);
		
		final DoubleArray<?> expected = _model.createIndividualExpectationGrid(2);
		assertArrayEquals("Size should be individual expectations size",expected.size(), result.size());
		double[] values_3 =
		{ 2, 3, 4 };

		// Should vary along the current shock dimension
		expected.lastDimSlice(0).fillDimensions(DoubleArrayFactories.createArray(values_3),1);
		double[] values_4 =
		{ 3, 4, 5 };
		expected.lastDimSlice(1).fillDimensions(DoubleArrayFactories.createArray(values_4),1);
		
		ArrayAssert.assertEquals(expected, result,1e-10);
	}
	
	@Test public void indTransVarybyFutPermShock()
	{
		int[] size_ =
		{ 3, 3, 2, 4, 1 };
		DoubleArray<?> expectedAggStates = createArrayOfSize(size_);
		double[] values_ =
		{ 2, 3 };
		
		// Depending on the current shock, the future aggregate is either one two or three
		expectedAggStates.fillDimensions((DoubleArray<?>) DoubleArrayFactories.createArray(values_), 2);
		
		_mockery.checking(new Expectations() {{
			oneOf(_state).getExpectedAggregateStates(); will(returnValue(expectedAggStates));
		}});
		
		DoubleArray<?> input = _model.createIndividualTransitionGrid(2);
		double[] values_1 =
		{ 2, 3, 4, 5 };

		// Add one to the aggregate...
		input.lastDimSlice(0).fillDimensions(DoubleArrayFactories.createArray(values_1), 2);
		double[] values_2 =
		{ 0, .5 };
		
		// ...also allow the individual trans to vary by the perm shock...
		input.lastDimSlice(0).across(4).modifying().add(DoubleArrayFactories.createArray(values_2));
		double[] values_3 =
		{ 3, 4, 5, 6 };
		
		// Add two to the aggregate...
		input.lastDimSlice(1).fillDimensions(DoubleArrayFactories.createArray(values_3), 2);
		
		DoubleArray<?> result = _model.conditionalExpectation(input, _state);
		
		final DoubleArray<?> expected = _model.createIndividualExpectationGrid(2);
		assertArrayEquals("Size should be individual expectations size",expected.size(), result.size());
		double[] values_4 =
		{ 3, 4.5 };

		// Should vary along the perm shock dimension, with an additional
		// variation from the individual variation along that dim (the .5)
		expected.lastDimSlice(0).fillDimensions(DoubleArrayFactories.createArray(values_4),2);
		double[] values_5 =
		{ 4, 5 };
		expected.lastDimSlice(1).fillDimensions(DoubleArrayFactories.createArray(values_5),2);
		
		ArrayAssert.assertEquals(expected, result,1e-10);
	}
	
	@Test public void indTransVarybyAggEndoState()
	{
		int[] size_ =
		{ 3, 3, 2, 4, 1 };
		DoubleArray<?> expectedAggStates = createArrayOfSize(size_);
		double[] values_ =
		{ 2, 3, 4, 5 };
		
		// Future state = current state + 1
		expectedAggStates.fillDimensions((DoubleArray<?>) DoubleArrayFactories.createArray(values_), 3);
		
		_mockery.checking(new Expectations() {{
			oneOf(_state).getExpectedAggregateStates(); will(returnValue(expectedAggStates));
		}});
		
		DoubleArray<?> input = _model.createIndividualTransitionGrid(2);
		double[] values_1 =
		{ 2, 3, 4, 5 };

		// Add one to the aggregate...
		input.lastDimSlice(0).fillDimensions(DoubleArrayFactories.createArray(values_1), 2);
		double[] values_2 =
		{ 0, .5 };
		
		// ...also allow the individual trans to vary by the perm shock...
		input.lastDimSlice(0).across(4).modifying().add(DoubleArrayFactories.createArray(values_2));
		double[] values_3 =
		{ 3, 4, 5, 6 };
		
		// Add two to the aggregate...
		input.lastDimSlice(1).fillDimensions(DoubleArrayFactories.createArray(values_3), 2);
		
		DoubleArray<?> result = _model.conditionalExpectation(input, _state);
		
		final DoubleArray<?> expected = _model.createIndividualExpectationGrid(2);
		assertArrayEquals("Size should be individual expectations size",expected.size(), result.size());
		double[] values_4 =
		{ 3, 4, 5, 6 };

		// Should vary along the perm shock dimension, with an additional
		// variation from the individual variation along that dim (the .5)
		expected.lastDimSlice(0).fillDimensions(DoubleArrayFactories.createArray(values_4),3);
		double[] values_5 =
		{ 0, .5 };
		expected.lastDimSlice(0).across(2).modifying().add(DoubleArrayFactories.createArray(values_5));
		double[] values_6 =
		{ 4, 5, 6, 7 };
		expected.lastDimSlice(1).fillDimensions(DoubleArrayFactories.createArray(values_6),3);
		
		ArrayAssert.assertEquals(expected, result,1e-10);
	}
}
