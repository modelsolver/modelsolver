package com.meliorbis.economics.infrastructure;

import java.io.IOException;

import com.meliorbis.economics.aggregate.AggregateProblemSolver;
import com.meliorbis.economics.individual.IndividualProblemSolver;
import com.meliorbis.numerics.io.NumericsReader;
import com.meliorbis.numerics.io.NumericsWriter;

public class ModelConfigForTests extends ModelConfigBase
{
	public boolean _constrained = false;
	
	@Override
	public boolean isConstrained()
	{
		return _constrained;
	}

	@Override
	public void writeParameters(NumericsWriter writer_) throws IOException
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void readParameters(NumericsReader reader_) throws IOException
	{
		// TODO Auto-generated method stub

	}

	@Override
	public Class<? extends IndividualProblemSolver<?>> getIndividualSolver()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Class<? extends AggregateProblemSolver<?>> getAggregateProblemSolver()
	{
		// TODO Auto-generated method stub
		return null;
	}

}
