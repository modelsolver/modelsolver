/**
 * 
 */
package com.meliorbis.economics.infrastructure;

import static com.meliorbis.numerics.DoubleArrayFactories.*;
import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;

import com.meliorbis.economics.model.ModelConfig;
import com.meliorbis.economics.model.State;
import com.meliorbis.numerics.generic.primitives.DoubleArray;
import com.meliorbis.numerics.test.ArrayAssert;

/**
 * @author Tobias Grasl
 *
 */
public class TestAbstractModel
{

	private static final double DELTA = 1e-10;

	@Test
	public void testExpnGridSizesStd()
	{
		ModelConfigForTests config = new ModelConfigForTests();
		
		config.setAggregateEndogenousStates(
				createArray(1d),
				createArray(1d,2d));
		
		config.setAggregateExogenousStates(
				createArray(1d,2d,3d),
				createArray(1d,2d,3d,4d));

		config.setAggregateNormalisingExogenousStates(
				createArray(1d,2d,3d,4d,5d),
				createArray(1d,2d,3d,4d,5d,6d));
		
		config.setIndividualEndogenousStates(
				createArray(1d,2d,3d,4d,5d,6d,7d),
				createArray(1d,2d,3d,4d,5d,6d,7d,8d)
				);
		
		config.setAggregateControls(
				createArray(1d,2d,3d),
				createArray(1d,2d));

		
		AbstractModel<ModelConfig, State<ModelConfig>> model = 
				new AbstractModel<ModelConfig, State<ModelConfig>>()
		{

			@Override
			public boolean shouldUpdateAggregates(State<ModelConfig> state_)
			{
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public State<ModelConfig> initialState()
			{
				// TODO Auto-generated method stub
				return null;
			}
		};
		model.setConfig(config);
		
		model.initialise();
		
		DoubleArray<?> grid = model.createAggregateExpectationGrid();
		
		assertArrayEquals(new int[] {3,4,3,4,5,6,1,2,2},grid.size());
		
		grid = model.createAggregateExpectationGrid(6);
		
		assertArrayEquals(new int[] {3,4,3,4,5,6,1,2,6},grid.size());

		grid = model.createIndividualExpectationGrid();

		assertArrayEquals(new int[] {3,4,3,4,5,6,1,2,7,8,2},grid.size());
		
		grid = model.createIndividualExpectationGrid(4);

		assertArrayEquals(new int[] {3,4,3,4,5,6,1,2,7,8,4},grid.size());
	}
	
	@Test
	public void testExpnGridSizesCtrlExpn()
	{
		ModelConfigForTests config = initModelConfigWithCtrlAffectingExpns();
		
		AbstractModel<ModelConfig, State<ModelConfig>> model = 
				new AbstractModel<ModelConfig, State<ModelConfig>>()
		{

			@Override
			public boolean shouldUpdateAggregates(State<ModelConfig> state_)
			{
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public State<ModelConfig> initialState()
			{
				// TODO Auto-generated method stub
				return null;
			}
		};
		model.setConfig(config);
		
		model.initialise();
		
		DoubleArray<?> grid = model.createAggregateExpectationGrid();
		
		assertArrayEquals(new int[] {3,4,3,4,5,6,1,2,3,2},grid.size());
		
		grid = model.createAggregateExpectationGrid(6);
		
		assertArrayEquals(new int[] {3,4,3,4,5,6,1,2,3,6},grid.size());

		grid = model.createIndividualExpectationGrid();

		assertArrayEquals(new int[] {3,4,3,4,5,6,1,2,3,7,8,2},grid.size());
		
		grid = model.createIndividualExpectationGrid(4);

		assertArrayEquals(new int[] {3,4,3,4,5,6,1,2,3,7,8,4},grid.size());
	}

	/**
	 * @return
	 */
	private ModelConfigForTests initModelConfigWithCtrlAffectingExpns()
	{
		ModelConfigForTests config = new ModelConfigForTests() {

			
			@Override
			public int[] getAggregatesKnownWithCertainty()
			{
				// Should not affect expectations
				return new int[] {1};
			}

			@Override
			public int[] getControlsAffectingExpectations()
			{
				// Should affect expectations
				return new int[] {0};
			}
			
		};
		
		config.setAggregateEndogenousStates(
				createArray(1d),
				createArray(1d,2d));
		
		config.setAggregateExogenousStates(
				createArray(1d,2d,3d),
				createArray(1d,2d,3d,4d));

		config.setAggregateNormalisingExogenousStates(
				createArray(1d,2d,3d,4d,5d),
				createArray(1d,2d,3d,4d,5d,6d));
		
		config.setIndividualEndogenousStates(
				createArray(1d,2d,3d,4d,5d,6d,7d),
				createArray(1d,2d,3d,4d,5d,6d,7d,8d)
				);
		
		config.setAggregateControls(
				createArray(1d,2d,3d),
				createArray(1d,2d));
		return config;
	}
	
	@Test public void adjustExpectedAggregates() 
	{
		AbstractModel<ModelConfig, State<ModelConfig>> model = 
				new AbstractModel<ModelConfig, State<ModelConfig>>()
		{

			@Override
			public boolean shouldUpdateAggregates(State<ModelConfig> state_)
			{
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public State<ModelConfig> initialState()
			{
				// TODO Auto-generated method stub
				return null;
			}
		};
		
		ModelConfigForTests config = initModelConfigWithCtrlAffectingExpns();
		
		model.setConfig(config);
		
		model.initialise();
		
		AbstractStateBase<ModelConfig> state = new AbstractStateBase<ModelConfig>(config)
		{
		};
		DoubleArray<?> expnsByControl = createArrayOfSize(3,2);
		expnsByControl.fillDimensions(new double[] {1,2,3}, 0);
		expnsByControl.modifying().across(1).multiply(createArray(1,2));

		state.setAggregateTransition(model.createAggregateVariableGrid(2).
				across(2,3).add(expnsByControl).across(6).
				multiply(createArray(1,2)));
		
		model.adjustExpectedAggregates(state);
		
		DoubleArray<?> result = state.getExpectedAggregateStates();
		
		// What is the (test) expectation?
		DoubleArray<?> testExpn = model.createAggregateExpectationGrid(2);
		
		// The aggregate should vary only by the current control which affects expectations, 
		// and differ between the two vars
		testExpn.modifying().across(8).add(createArray(1,2,3)).across(9).multiply(createArray(1,2));
		
		ArrayAssert.assertEquals(testExpn, result, DELTA);
		
	}

}
