/**
 * 
 */
package com.meliorbis.economics.infrastructure;

import static org.junit.Assert.assertArrayEquals;

import java.io.IOException;

import org.jmock.Expectations;
import org.jmock.auto.Mock;
import org.jmock.integration.junit4.JUnitRuleMockery;
import org.junit.Test;

import com.meliorbis.economics.aggregate.AggregateProblemSolver;
import com.meliorbis.economics.individual.IndividualProblemSolver;
import com.meliorbis.economics.infrastructure.simulation.SimState;
import com.meliorbis.economics.model.AbstractModelTest;
import com.meliorbis.economics.model.ModelConfig;
import com.meliorbis.economics.model.ModelException;
import com.meliorbis.economics.model.StateWithControls;
import com.meliorbis.numerics.DoubleArrayFactories;
import com.meliorbis.numerics.generic.MultiDimensionalArray;
import com.meliorbis.numerics.generic.impl.IntegerArray;
import com.meliorbis.numerics.generic.primitives.DoubleArray;
import com.meliorbis.numerics.io.NOOPWriterFactory;
import com.meliorbis.numerics.io.NumericsReader;
import com.meliorbis.numerics.io.NumericsWriter;

/**
 * @author toby
 */
public class CalculateAggregateControlsTest extends AbstractModelTest
{
	JUnitRuleMockery context = new JUnitRuleMockery();
	
	ModelConfigBase _config = new ModelConfigBase()
	{
		
		@Override
		public void writeParameters(NumericsWriter writer_) throws IOException
		{
			
		}
		
		@Override
		public void readParameters(NumericsReader reader_) throws IOException
		{
			
		}
		
		@Override
		public boolean isConstrained()
		{
			return false;
		}
		
		@Override
		public Class<? extends IndividualProblemSolver<?>> getIndividualSolver()
		{
			return null;
		}
		
		@Override
		public Class<? extends AggregateProblemSolver<?>> getAggregateProblemSolver()
		{
			return null;
		}

		@Override
		public SimState getInitialSimState()
		{
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public <N extends Number> MultiDimensionalArray<N, ?> getInitialExogenousStates()
		{
			// TODO Auto-generated method stub
			return null;
		}
	};
	
	@Mock
	StateWithControls<ModelConfig> _state;


	@Test
	public void testSingleControlConstantDet() throws ModelException
	{
		_config.setAggregateControls(create1DArray(.1,.2));
		
		context.checking(new Expectations(){{
		}});
		
		AbstractModelWithControls<ModelConfig, StateWithControls<ModelConfig>> model = new AbstractModelWithControls<ModelConfig, StateWithControls<ModelConfig>>()
		{
			
			@Override
			public boolean shouldUpdateAggregates(StateWithControls<ModelConfig> state_)
			{
					return false;
			}

			@Override
			public StateWithControls<ModelConfig> initialState()
			{
				return null;
			}

			@Override
			protected DoubleArray<?> calculateControlDeterminants(SimState simState_, DoubleArray<?> individualTransitionByAggregateControl_,
					double[] currentAggStates_, IntegerArray priorAggShockIndices_, StateWithControls<ModelConfig> calcState_)
			{
				// Creates determinants that are constant at .15
				double[] values_ =
				{ 2, 1 };
				DoubleArray<?> determinants = DoubleArrayFactories.createArray(values_);
				determinants.fill(.15);
				
				return determinants;
			}
		};
		
		model.setWriterFactory(new NOOPWriterFactory());
		model.setConfig(_config);
		model.initialise();
		
		double[] actualControls = model.calculateAggregateControls(null, null, null, null, _state);
		
		assertArrayEquals(new double[]{.15}, actualControls, 1e-9);
	}
	
	@Test
	public void testExpectationOnlyControl() throws ModelException
	{
		_config.setAggregateControls(create1DArray(.1,.2),create1DArray(1d));
		
		context.checking(new Expectations(){{
		}});
		
		AbstractModelWithControls<ModelConfig, StateWithControls<ModelConfig>> model = new AbstractModelWithControls<ModelConfig, StateWithControls<ModelConfig>>()
		{
			
			@Override
			public boolean shouldUpdateAggregates(StateWithControls<ModelConfig> state_)
			{
					return false;
			}

			@Override
			public StateWithControls<ModelConfig> initialState()
			{
				return null;
			}

			@Override
			protected DoubleArray<?> calculateControlDeterminants(SimState simState_, DoubleArray<?> individualTransitionByAggregateControl_,
					double[] currentAggStates_, IntegerArray priorAggShockIndices_, StateWithControls<ModelConfig> calcState_)
			{
				// Creates determinants that are constant at .15
				DoubleArray<?> determinants = DoubleArrayFactories.createArrayOfSize(2,1,2);
				determinants.lastDimSlice(0).fill(.15,.125);
				determinants.lastDimSlice(1).fill(.25,.225);
				
				return determinants;
			}
		};
		
		model.setWriterFactory(new NOOPWriterFactory());
		model.setConfig(_config);
		model.initialise();
		
		double[] actualControls = model.calculateAggregateControls(null, null, null, null, _state);
		
		assertArrayEquals(new double[]{.005/.125+.1,.005/.125+.2}, actualControls, 1e-9);
	}
	
	@Test
	public void testSingleControlVariableDet() throws ModelException
	{
		_config.setAggregateControls(create1DArray(.1,.2));
		
		context.checking(new Expectations(){{
		}});
		
		AbstractModelWithControls<ModelConfig, StateWithControls<ModelConfig>> model = new AbstractModelWithControls<ModelConfig, StateWithControls<ModelConfig>>()
		{
			
			@Override
			public boolean shouldUpdateAggregates(StateWithControls<ModelConfig> state_)
			{
					return false;
			}

			@Override
			public StateWithControls<ModelConfig> initialState()
			{
				return null;
			}

			@Override
			protected DoubleArray<?> calculateControlDeterminants(SimState simState_, DoubleArray<?> individualTransitionByAggregateControl_,
					double[] currentAggStates_, IntegerArray priorAggShockIndices_, StateWithControls<ModelConfig> calcState_)
			{
				// Creates determinants that are constant at .15
				double[] values_ =
				{ 2, 1 };
				DoubleArray<?> determinants = DoubleArrayFactories.createArray(values_);
				determinants.fill(.15,.125);
				
				return determinants;
			}
		};
		
		model.setWriterFactory(new NOOPWriterFactory());
		model.setConfig(_config);
		model.initialise();
		
		double[] actualControls = model.calculateAggregateControls(null, null, null, null, _state);
		
		assertArrayEquals(new double[]{.005/.125+.1}, actualControls, 1e-9);
	}
	
	@Test
	public void testMultipleControlVariableDet() throws ModelException
	{
		_config.setAggregateControls(DoubleArrayFactories.createArray(.1,.2),
				DoubleArrayFactories.createArray(.3,.5));
		
		context.checking(new Expectations(){{
		}});
		
		AbstractModelWithControls<ModelConfig, StateWithControls<ModelConfig>> model = new AbstractModelWithControls<ModelConfig, StateWithControls<ModelConfig>>()
		{
			
			@Override
			public boolean shouldUpdateAggregates(StateWithControls<ModelConfig> state_)
			{
					return false;
			}

			@Override
			public StateWithControls<ModelConfig> initialState()
			{
				return null;
			}

			@Override
			protected DoubleArray<?> calculateControlDeterminants(SimState simState_, DoubleArray<?> individualTransitionByAggregateControl_,
					double[] currentAggStates_, IntegerArray priorAggShockIndices_, StateWithControls<ModelConfig> calcState_)
			{
				// Creates determinants that are constant at .15
				DoubleArray<?> determinants = DoubleArrayFactories.createArrayOfSize(2,2,2);
				determinants.lastDimSlice(0).fillDimensions(new double[] {.15,.125},0);
				determinants.lastDimSlice(1).fillDimensions(new double[] {.45,.35},1);
				determinants.lastDimSlice(1).modifying().across(0).add(create1DArray(0,.1));
				return determinants;
			}
		};
		
		model.setWriterFactory(new NOOPWriterFactory());
		model.setConfig(_config);
		model.initialise();
		
		double[] actualControls = model.calculateAggregateControls(null, null, null, null, _state);
		
		double ctrl1 = .005/.125+.1;
		
		assertArrayEquals(new double[]{ctrl1,2d/3*(.15+(ctrl1-.1))+.3}, actualControls, 1e-9);
	}
	
	@Test
	public void testSingleControlAlternativeTarget() throws ModelException
	{
		_config.setAggregateControls(create1DArray(.1,.2));
		
		_config.setControlTargets(create1DArray(0d,0d));
		
		context.checking(new Expectations(){{
		}});
		
		AbstractModelWithControls<ModelConfig, StateWithControls<ModelConfig>> model = new AbstractModelWithControls<ModelConfig, StateWithControls<ModelConfig>>()
		{
			
			@Override
			public boolean shouldUpdateAggregates(StateWithControls<ModelConfig> state_)
			{
					return false;
			}

			@Override
			public StateWithControls<ModelConfig> initialState()
			{
				return null;
			}

			@Override
			protected DoubleArray<?> calculateControlDeterminants(SimState simState_, DoubleArray<?> individualTransitionByAggregateControl_,
					double[] currentAggStates_, IntegerArray priorAggShockIndices_, StateWithControls<ModelConfig> calcState_)
			{
				// Creates determinants that are constant at .15
				double[] values_ =
				{ 2, 1 };
				DoubleArray<?> determinants = DoubleArrayFactories.createArray(values_);
				determinants.fill(.15,-.5);
				
				return determinants;
			}
		};
		
		model.setWriterFactory(new NOOPWriterFactory());
		model.setConfig(_config);
		model.initialise();
		
		double[] actualControls = model.calculateAggregateControls(null, null, null, null, _state);
		
		assertArrayEquals(new double[]{.015/.65+.1}, actualControls, 1e-9);
	}

}
