/**
 * 
 */
package com.meliorbis.economics.infrastructure;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.Test;

import com.meliorbis.economics.aggregate.AggregateProblemSolver;
import com.meliorbis.economics.individual.IndividualProblemSolver;
import com.meliorbis.economics.model.AbstractModelTest;
import com.meliorbis.economics.model.Model;
import com.meliorbis.economics.model.ModelConfig;
import com.meliorbis.economics.model.ModelException;
import com.meliorbis.economics.model.State;
import com.meliorbis.numerics.convergence.DoubleCriterion;

/**
 * Test cases for the Solver class
 * 
 * @author Tobias Grasl
 */
public class SolverTest extends AbstractModelTest
{
	Mockery _context = new Mockery();

	@Test
	public void testSinglePeriodIndOnly() throws ModelException
	{
		@SuppressWarnings("unchecked")
		Model<?, State<?>> mockModel = _context.mock(Model.class);
		State<?> mockState = _context.mock(State.class);
		
		@SuppressWarnings("unchecked")
		IndividualProblemSolver<State<?>> indSolver = _context.mock(IndividualProblemSolver.class);
		
		_context.checking(new Expectations(){{
			atLeast(1).of(mockModel).getIndividualSolverInstance();will(returnValue(indSolver));
			exactly(1).of(indSolver).performIteration(with(mockState));			
		}});
		
		_gridSolver.performIndividualIteration(mockModel, mockState);
		
		_context.assertIsSatisfied();
	}
	
	@Test
	public void testPerformIterationWithAgg() throws SolverException
	{
		@SuppressWarnings("unchecked")
		Model<?, State<?>> mockModel = _context.mock(Model.class);
		State<?> mockState = _context.mock(State.class);
		
		@SuppressWarnings("unchecked")
		IndividualProblemSolver<State<?>> indSolver = _context.mock(IndividualProblemSolver.class);
		@SuppressWarnings("unchecked")
		AggregateProblemSolver<State<?>> aggSolver = _context.mock(AggregateProblemSolver.class);
		
		
		ModelConfig config = _context.mock(ModelConfig.class);
		
		_context.checking(new Expectations(){{
			
			allowing(config).hasAggUncertainty();will(returnValue(true));
			allowing(config).hasIndUncertainty();will(returnValue(true));
			
			atLeast(1).of(mockModel).getConfig();will(returnValue(config));
			
			
			allowing(mockModel).adjustExpectedAggregates(with(mockState));
			
			exactly(1).of(mockState).incrementPeriod();
			
			atLeast(1).of(mockModel).getIndividualSolverInstance();will(returnValue(indSolver));
			exactly(1).of(indSolver).performIteration(with(mockState));
			
			atLeast(1).of(mockModel).shouldUpdateAggregates(with(mockState));will(returnValue(true));
			atLeast(1).of(mockModel).getAggregateSolverInstance();will(returnValue(aggSolver));
			
			exactly(1).of(aggSolver).updateAggregateTransition(with(mockState));
			
		}});
		
		_gridSolver.performIteration(mockModel, mockState);
		
		_context.assertIsSatisfied();
	}
	
	@Test
	public void testPerformIterationNoAggShocks() throws SolverException
	{
		@SuppressWarnings("unchecked")
		Model<?, State<?>> mockModel = _context.mock(Model.class);
		State<?> mockState = _context.mock(State.class);
		
		@SuppressWarnings("unchecked")
		IndividualProblemSolver<State<?>> indSolver = _context.mock(IndividualProblemSolver.class);
		
		
		ModelConfig config = _context.mock(ModelConfig.class);
		
		_context.checking(new Expectations(){{
			
			// The model is configured as no agg uncert, but with ind uncert
			allowing(config).hasAggUncertainty(); will(returnValue(false));
			allowing(config).hasIndUncertainty(); will(returnValue(true));
			
			atLeast(1).of(mockModel).getConfig();will(returnValue(config));
			
			exactly(1).of(mockState).incrementPeriod();
			
			atLeast(1).of(mockModel).getIndividualSolverInstance();will(returnValue(indSolver));
			exactly(1).of(indSolver).performIteration(with(mockState));
		}});
		
		_gridSolver.performIteration(mockModel, mockState);
		
		_context.assertIsSatisfied();
	}
	
	@Test
	public void testPerformIterationNoAggUpdate() throws SolverException
	{
		@SuppressWarnings("unchecked")
		Model<?, State<?>> mockModel = _context.mock(Model.class);
		State<?> mockState = _context.mock(State.class);
		
		@SuppressWarnings("unchecked")
		IndividualProblemSolver<State<?>> indSolver = _context.mock(IndividualProblemSolver.class);
		
		ModelConfig config = _context.mock(ModelConfig.class);
		
		_context.checking(new Expectations(){{
			
			allowing(config).hasAggUncertainty(); will(returnValue(true));
			allowing(config).hasIndUncertainty(); will(returnValue(true));
			
			atLeast(1).of(mockModel).getConfig();will(returnValue(config));
			
			exactly(1).of(mockState).incrementPeriod();
			
			atLeast(1).of(mockModel).getIndividualSolverInstance();will(returnValue(indSolver));
			exactly(1).of(indSolver).performIteration(with(mockState));
			
			atLeast(1).of(mockModel).shouldUpdateAggregates(with(mockState));will(returnValue(false));
		}});
		
		_gridSolver.performIteration(mockModel, mockState);
		
		_context.assertIsSatisfied();
	}
	
	@Test
	public void testSolveModel() throws SolverException
	{
		@SuppressWarnings("unchecked")
		Model<?, State<?>> mockModel = _context.mock(Model.class);
		State<?> mockState = _context.mock(State.class);
		
		@SuppressWarnings("unchecked")
		IndividualProblemSolver<State<?>> indSolver = _context.mock(IndividualProblemSolver.class);
		@SuppressWarnings("unchecked")
		AggregateProblemSolver<State<?>> aggSolver = _context.mock(AggregateProblemSolver.class);
		
		
		ModelConfig config = _context.mock(ModelConfig.class);
		
		
		_context.checking(new Expectations(){{
			
			int period = 0;
			
			allowing(config).hasAggUncertainty(); will(returnValue(true));
			allowing(config).hasIndUncertainty(); will(returnValue(true));
			
			allowing(mockModel).getIndividualSolverInstance();will(returnValue(indSolver));
			allowing(mockModel).getAggregateSolverInstance();will(returnValue(aggSolver));
			
			atLeast(1).of(mockModel).getConfig();will(returnValue(config));
			
			allowing(indSolver).initialise(mockState);
			allowing(aggSolver).initialise(mockState);
			allowing(mockState).getPeriod();will(returnValue(period++));
			allowing(mockModel).adjustExpectedAggregates(with(mockState));
			
			exactly(1).of(mockState).incrementPeriod();
			
			exactly(1).of(indSolver).performIteration(with(mockState));
			
			exactly(1).of(mockModel).shouldUpdateAggregates(with(mockState));will(returnValue(false));
			
			exactly(1).of(mockState).incrementPeriod();
			
			exactly(1).of(indSolver).performIteration(with(mockState));
			
			exactly(1).of(mockModel).shouldUpdateAggregates(with(mockState));will(returnValue(true));
			
			exactly(1).of(aggSolver).updateAggregateTransition(with(mockState));

			exactly(1).of(mockState).getConvergenceCriterion(); will(returnValue(DoubleCriterion.of(10)));
			exactly(1).of(mockState).getConvergenceCriterion(); will(returnValue(DoubleCriterion.of(0)));
			
			exactly(1).of(mockModel).solutionFound(mockState);
		}});
		
		_gridSolver.solveModel(mockModel, mockState, false);
		
		_context.assertIsSatisfied();
	}
	

}
