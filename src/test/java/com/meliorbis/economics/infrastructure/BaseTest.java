package com.meliorbis.economics.infrastructure;


import java.io.IOException;

import org.jmock.Expectations;
import org.jmock.auto.Mock;
import org.jmock.integration.junit4.JUnitRuleMockery;
import org.junit.Rule;
import org.junit.Test;

import com.meliorbis.numerics.DoubleArrayFactories;
import com.meliorbis.numerics.generic.primitives.DoubleArray;
import com.meliorbis.numerics.io.NumericsWriter;
import com.meliorbis.numerics.io.NumericsWriterFactory;

public class BaseTest
{
	@Rule public JUnitRuleMockery _context = new JUnitRuleMockery();
	@Mock public NumericsWriterFactory _writerFactory;
	
	@Test
	public void testDebugWriteArray() throws IOException
	{	
		NumericsWriter writer = _context.mock(NumericsWriter.class);
	
		DoubleArray<?> array = DoubleArrayFactories.createArray(1.0,2.0,3.0);
		
		_context.checking(new Expectations() {{
			atLeast(1).of(_writerFactory).defaultExtension(); will(returnValue(".foo"));
			
			oneOf(_writerFactory).create("sample.foo"); will(returnValue(writer));
			
			oneOf(writer).writeArray(with("sample"), with(array));
			oneOf(writer).close();
		}});
		
		Base base = new Base();
		base.setWriterFactory(_writerFactory);
		

		
		base.debugWriteArray(array, "sample");
		_context.assertIsSatisfied();
	}
	
	@Test
	public void testDebugWriteArrayStripsExtension() throws IOException
	{	
		NumericsWriter writer = _context.mock(NumericsWriter.class);
	
		DoubleArray<?> array = DoubleArrayFactories.createArray(1.0,2.0,3.0);
		
		_context.checking(new Expectations() {{
			atLeast(1).of(_writerFactory).defaultExtension(); will(returnValue(".foo"));
			
			oneOf(_writerFactory).create("sample.foo"); will(returnValue(writer));
			
			oneOf(writer).writeArray(with("sample"), with(array));
			oneOf(writer).close();
		}});
		
		Base base = new Base();
		base.setWriterFactory(_writerFactory);
		

		
		base.debugWriteArray(array, "sample.thing");
		_context.assertIsSatisfied();
	}
	
	@Test
	public void testDebugWriteArrayLastDotOnly() throws IOException
	{	
		NumericsWriter writer = _context.mock(NumericsWriter.class);
	
		DoubleArray<?> array = DoubleArrayFactories.createArray(1.0,2.0,3.0);
		
		_context.checking(new Expectations() {{
			atLeast(1).of(_writerFactory).defaultExtension(); will(returnValue(".foo"));
			
			oneOf(_writerFactory).create("sample.thing.foo"); will(returnValue(writer));
			
			oneOf(writer).writeArray(with("sample.thing"), with(array));
			oneOf(writer).close();
		}});
		
		Base base = new Base();
		base.setWriterFactory(_writerFactory);
		

		
		base.debugWriteArray(array, "sample.thing.bob");
		_context.assertIsSatisfied();
	}

}
