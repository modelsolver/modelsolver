package com.meliorbis.economics.infrastructure.simulation.observers;

import static com.meliorbis.numerics.DoubleArrayFactories.createArrayOfSize;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.jmock.Expectations;
import org.jmock.auto.Mock;
import org.jmock.integration.junit4.JUnitRuleMockery;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.meliorbis.economics.infrastructure.simulation.SimState;
import com.meliorbis.economics.infrastructure.simulation.TransitionRecord;
import com.meliorbis.economics.model.Model;
import com.meliorbis.economics.model.ModelConfig;
import com.meliorbis.economics.model.StateWithControls;
import com.meliorbis.numerics.DoubleArrayFactories;
import com.meliorbis.numerics.IntArrayFactories;
import com.meliorbis.numerics.generic.IntegerArray;
import com.meliorbis.numerics.generic.primitives.DoubleArray;

public class ForecastingObserverTest
{
	private static final double DELTA = 1e-10;
	@Mock private Model<ModelConfig, StateWithControls<ModelConfig>> _model;
	@Mock private ModelConfig _config;
	@Mock private StateWithControls<ModelConfig> _state;

	@Rule public JUnitRuleMockery _context = new JUnitRuleMockery();
	private List<DoubleArray<?>> _shocks;
	private List<DoubleArray<?>> _states;
	private List<DoubleArray<?>> _controls;
	private List<DoubleArray<?>> _futureShocks;
	
	@Before public void setup() 
	{
		
		_shocks = new ArrayList<>();
		double[] values_ =
		{ .98, .99, 1.0 };
		_shocks.add((DoubleArray<?>) DoubleArrayFactories.createArray(values_));
		double[] values_1 =
		{ .6, .7, .8, .9 };
		_shocks.add((DoubleArray<?>) DoubleArrayFactories.createArray(values_1));
		
		_futureShocks = new ArrayList<>();
		double[] values_2 =
		{ .98, .99 };
		_futureShocks.add((DoubleArray<?>) DoubleArrayFactories.createArray(values_2));
		
		_states = new ArrayList<>();
		double[] values_3 =
		{ 1, 2 };
		_states.add((DoubleArray<?>) DoubleArrayFactories.createArray(values_3));
		double[] values_4 =
		{ .1, .2, .3 };
		_states.add((DoubleArray<?>) DoubleArrayFactories.createArray(values_4));
		
		_controls = new ArrayList<>();
		double[] values_5 =
		{ .4, 1.4, 2.4 };
		_controls.add((DoubleArray<?>) DoubleArrayFactories.createArray(values_5));
		
		_context.checking(new Expectations() {{
			allowing(_model).getConfig(); will(returnValue(_config));
			allowing(_config).getAggregateExogenousStates(); will(returnValue(_shocks));
			allowing(_config).getAggregateEndogenousStates(); will(returnValue(_states));
			allowing(_config).getAggregateNormalisingExogenousStates(); will(returnValue(_futureShocks));
			allowing(_config).getAggregateControls(); will(returnValue(_controls));

			allowing(_config).getAggregateControlCount(); will(returnValue(1));
			allowing(_config).getAggregateEndogenousStateCount(); will(returnValue(2));
			allowing(_config).getAggregateExogenousStateCount(); will(returnValue(2));
			allowing(_config).getAggregateNormalisingStateCount(); will(returnValue(1));
		}});
		
	}
	
	@Test
	public void testGetsSetupData()
	{
		ForecastingObserver<SimState, Integer> observer = new ForecastingObserver<SimState, Integer>();
		
		SimState initial = _context.mock(SimState.class);
		int[] size_ =
		{ 3, 4, 3, 4, 2, 2, 3, 2 };

		DoubleArray<?> expectedStates = createArrayOfSize(size_);
		int[] size_1 =
		{ 3, 4, 3, 4, 2, 2, 3, 1 };
		DoubleArray<?> expectedControls = createArrayOfSize(size_1);
		
		_context.checking(new Expectations() {{
				oneOf(_state).getExpectedAggregateStates(); will(returnValue(expectedStates));
				oneOf(_state).getExpectedAggregateControls(); will(returnValue(expectedControls));
		}});
	

		observer.beginSimulation(initial, _state, _model, 2);
		_context.assertIsSatisfied();
		
		assertEquals(observer._numControls, 1);
		assertEquals(observer._numShocks, 2);
		assertEquals(observer._numStates, 2);		
		assertEquals(observer._numNormShocks, 1);		
		
		assertArrayEquals(new int[] {2,1},observer._controlForecasts.size());
		assertArrayEquals(new int[] {2,2},observer._stateForecasts.size());

		assertEquals(7,observer._forecastInputs.length);
		
		assertNotNull(observer._aggregateExogenousStates);
	}

	@Test
	public void testContinuousShockInputs()
	{
		ForecastingObserver<SimState, Double> observer = new ForecastingObserver<SimState, Double>();
		
		observer._numShocks = 2;
		observer._numNormShocks = 1;
		double[] values_ =
		{ .34, .56, .67 };
		double[] values_1 =
		{ .78, .910, .1011 };
		
		double[] shockInputs = observer.shockInputs((DoubleArray<?>) DoubleArrayFactories.createArray(values_),(DoubleArray<?>) DoubleArrayFactories.createArray(values_1));
		
		assertArrayEquals(new double[] {.34,.56,.78,.910,.1011},shockInputs,DELTA);
	}
	
	@Test
	public void testDiscreteShockInputs()
	{
		ForecastingObserver<SimState, Integer> observer = new ForecastingObserver<SimState, Integer>();
		observer._numShocks = 2;
		observer._numNormShocks = 1;
		int[] values_ =
		{ 1, 2, 1 };
		
		IntegerArray<?> currentShockLevels = IntArrayFactories.createIntArray(values_);
		int[] values_1 =
		{ 0, 3, 0 };
		IntegerArray<?> futureShockLevels = IntArrayFactories.createIntArray(values_1);
		

		observer._aggregateExogenousStates = _shocks;
		observer._aggregateNormalisingStates = _futureShocks;
		
		double[] shockInputs = observer.shockInputs(currentShockLevels, futureShockLevels);
		
		assertArrayEquals(new double[] {.99,.8,.98,.9,.98},shockInputs,DELTA);
	}
	
	@Test public void testSimFirstPeriod()
	{
		ForecastingObserver<SimState, Integer> observer = beginSim();
		
		TransitionRecord<SimState, Integer> record = new TransitionRecord<>();
		double[] values_ =
		{ 1.5, .2 };
		
		record.setStates((DoubleArray<?>) DoubleArrayFactories.createArray(values_));
		double[] values_1 =
		{ .4 };
		record.setControls((DoubleArray<?>) DoubleArrayFactories.createArray(values_1));
		int[] values_2 =
		{ 0, 3, 0 };
		record.setShocks((IntegerArray<?>) IntArrayFactories.createIntArray(values_2));
		int[] values_3 =
		{ 0, 3, 0 };
		record.setFutureShocks(IntArrayFactories.createIntArray(values_3));
		
		observer.periodSimulated(null, record,_state, 0);
		
		// States today should be filled with initial values
		assertEquals(1.5d, observer._stateForecasts.get(0,0), DELTA);
		assertEquals(.2d, observer._stateForecasts.get(0,1), DELTA);

		// Should have copied current control value
		assertEquals(.4d, observer._controlForecasts.get(0,0), DELTA);

		// Controls tomorro:
		assertEquals(3d, observer._controlForecasts.get(1,0), DELTA);
		
		// States tomorrow...
		assertEquals(7d, observer._stateForecasts.get(1,0), DELTA);
		assertEquals(10d, observer._stateForecasts.get(1,1), DELTA);
	}
	
	@Test public void testSimMiddlePeriod()
	{
		ForecastingObserver<SimState, Integer> observer = beginSim();
		
		// Set a pretend value for the state forecast for this period
		TransitionRecord<SimState, Integer> record = new TransitionRecord<>();
		double[] values_ =
		{ 1.6, .2 };
		
		record.setStates((DoubleArray<?>) DoubleArrayFactories.createArray(values_));
		double[] values_1 =
		{ .4 };
		record.setControls((DoubleArray<?>) DoubleArrayFactories.createArray(values_1));
		int[] values_2 =
		{ 0, 1, 1 };
		record.setShocks((IntegerArray<?>) IntArrayFactories.createIntArray(values_2));
		int[] values_3 =
		{ 0, 3, 0 };
		record.setFutureShocks(IntArrayFactories.createIntArray(values_3));
		
		observer.periodSimulated(null, record,_state, 1);
		
		// Today's forecast states should not be modified
		assertEquals(0, observer._stateForecasts.get(1,0), DELTA);
		
		// Should interpolate half way between pos. 1 and 2 the two values for each variable
		// Controls tomorrow
		assertEquals(3.2, observer._controlForecasts.get(2,0), DELTA);
		
		// States tomorrow...
		assertEquals(5.2d, observer._stateForecasts.get(2,0), DELTA);
		assertEquals(8.2d, observer._stateForecasts.get(2,1), DELTA);
	}
	
	@Test public void testSimFinalPeriod()
	{
		ForecastingObserver<SimState, Integer> observer = beginSim();
		
		// Set a pretend value for the state forecast for this period
		TransitionRecord<SimState, Integer> record = new TransitionRecord<>();
		double[] values_ =
		{ 1.4, .2 };
		
		record.setStates((DoubleArray<?>) DoubleArrayFactories.createArray(values_));
		double[] values_1 =
		{ .4 };
		record.setControls((DoubleArray<?>) DoubleArrayFactories.createArray(values_1));
		int[] values_2 =
		{ 2, 1, 1 };
		record.setShocks((IntegerArray<?>) IntArrayFactories.createIntArray(values_2));
		int[] values_3 =
		{ 0, 3, 0 };
		record.setFutureShocks(IntArrayFactories.createIntArray(values_3));
		observer.periodSimulated(null, record,_state, 2);
		
		// States and controls should not be modified
		assertEquals(0, observer._stateForecasts.sum() + observer._controlForecasts.sum(),DELTA);	
	}

	/**
	 * @return
	 */
	private ForecastingObserver<SimState, Integer> beginSim()
	{
		ForecastingObserver<SimState, Integer> observer = new ForecastingObserver<SimState, Integer>();
		
		SimState initial = _context.mock(SimState.class);
		int[] size_ =
		{ 3, 4, 3, 4, 2, 2, 3, 2 };

		DoubleArray<?> expectedStates = createArrayOfSize(size_);
		int[] size_1 =
		{ 3, 4, 3, 4, 2, 2, 3, 1 };
		DoubleArray<?> expectedControls = createArrayOfSize(size_1);
		
		expectedControls.fillDimensions(new double[] {2, 4},5);
		double[] values_ =
		{ 3, 6, 5, 8 };
		
		expectedStates.fillDimensions((DoubleArray<?>) DoubleArrayFactories.createArray(values_),5,7);
		double[] values_1 =
		{ 0, 1, 2, 3 };
		expectedStates.across(1).modifying().add(DoubleArrayFactories.createArray(values_1));
		
				
		_context.checking(new Expectations() {{
				allowing(_state).getExpectedAggregateStates(); will(returnValue(expectedStates));
				allowing(_state).getExpectedAggregateControls(); will(returnValue(expectedControls));
		}});
	
		observer.beginSimulation(initial, _state, _model, 3);
		return observer;
	}
}
