package com.meliorbis.economics.infrastructure.simulation;

import static com.meliorbis.numerics.DoubleArrayFactories.*;
import static org.junit.Assert.*;
import static com.meliorbis.numerics.test.ArrayAssert.*;

import java.util.Arrays;

import org.jmock.Expectations;
import org.jmock.auto.Mock;
import org.jmock.integration.junit4.JUnitRuleMockery;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.meliorbis.economics.model.Model;
import com.meliorbis.economics.model.ModelConfig;
import com.meliorbis.economics.model.ModelException;
import com.meliorbis.economics.model.State;
import com.meliorbis.numerics.IntArrayFactories;
import com.meliorbis.numerics.generic.IntegerArray;
import com.meliorbis.numerics.generic.MultiDimensionalArray;
import com.meliorbis.numerics.generic.primitives.DoubleArray;
import com.meliorbis.numerics.generic.primitives.DoubleNaryOp;


/**
 * Tests simulation in the presence of multiple individual exogenous states.
 * 
 * @author Tobias Grasl
 */
public class TestSimWithMultipleExoIndStates
{
	private static final double DELTA = 1e-10;
	@Rule public JUnitRuleMockery _context = new JUnitRuleMockery();
	@Mock public Model<ModelConfig, State<ModelConfig>> _model;
	@Mock public ModelConfig _config;
	@Mock public State<ModelConfig> _state;
	private DiscretisedDistributionSimulatorImpl _simulator;
	private DiscretisedDistribution _src;
	private DiscretisedDistribution _target;
	private DoubleArray<?> _transProbs;
	private DoubleArray<?> _grid;
	private DoubleArray<?> _transitionFn;
	
	@Before
	public void setUp()
	{
		_context.checking(new Expectations() {{
			allowing(_model).getConfig(); will(returnValue(_config));
			allowing(_config).isConstrained(); will(returnValue(true));
		}});
		_simulator = new DiscretisedDistributionSimulatorImpl();
		
		_src = new DiscretisedDistribution();
		_src._density = createArrayOfSize(5,2,2);
		_src._overflowAverages = createArrayOfSize(1,2,2);
		_src._overflowProportions = createArrayOfSize(1,2,2);
				
		_target = _src.createSameSized();
		
		_transProbs = createArrayOfSize(2,2,2,2);

		// Identity Transition
		_transProbs.set(1,0,0,0,0);
		_transProbs.set(1,0,1,0,1);
		_transProbs.set(1,1,0,1,0);
		_transProbs.set(1,1,1,1,1);
		
		_grid = createArrayOfSize(5,2,2);
		_grid.fillDimensions(new double[] {1,2,3,4,5},0);
		
		_transitionFn = createArrayOfSize(5,2,2,1);
		_transitionFn.fill(_grid);
		
		_src._density.fill(.05);
	}
	
	@Test
	public void testIdentifyTransition()
	{

		// Since the transition is an identify fn, and the probs are too...
		_simulator.distribute(_src._density, _transitionFn, _transProbs, _target, _grid, true, -1, true);
		
		// Target should equal input
		assertEquals(_src._density, _target._density,DELTA);
	}
	
	@Test
	public void testProabilitiesShiftPopulation()
	{
		/* TEST PROBABILITIES
		 */
		// Everyone goes to state 1,1
		_transProbs.set(0,0,0,0,0);
		_transProbs.set(1,0,0,1,1);
		
		_transProbs.set(0,0,1,0,1);
		_transProbs.set(1,0,1,1,1);
		
		_transProbs.set(0,1,0,1,0);
		_transProbs.set(1,1,0,1,1);

		_simulator.distribute(_src._density, _transitionFn, _transProbs, _target, _grid, true, -1, true);
		
		DoubleArray<?> expected = createArrayOfSize(5,2,2);
		expected.at(-1,1,1).fill(1d/_src._density.size()[0]);
		// Others all stay at 0!
		
		assertEquals(expected, _target._density,DELTA);
	}
	
	@Test
	public void testLowerBound()
	{
		/* TEST TRANS FN
		 */
		// Mix population around - uniformly!
		_transProbs.fill(.25);
		
		_transitionFn.modifying().subtract(.5);
		
		_simulator.distribute(_src._density, _transitionFn, _transProbs, _target, _grid, true, -1, true);
		
		DoubleArray<?> expected = createArrayOfSize(5,2,2);
		expected.fill(.05);
		// The trans is down by .5, and then the redist to the grid will put .05 each
		// way; the bottom is constrained, so we end up with a similar grid as before
		// through symmetry except at bottom and top
		expected.at(0).modifying().multiply(1.5);
		expected.at(4).modifying().multiply(0.5);
		
		assertEquals(expected, _target._density,DELTA);
	}
	
	@Test
	public void testOverflow()
	{
		/* With Overflow
		 */
		_target = _target.createSameSized();
		
		_transProbs.fillDimensions(createArray(.1,.1,.1,.7), 2,3);
		
		_transitionFn.modifying().add(.5);
		
		_simulator.distribute(_src._density, _transitionFn, _transProbs, _target, _grid, true, -1, true);
		
		DoubleArray<?> expected = createArrayOfSize(5,2,2);
		
		expected.fill(.05);
		// The trans is down by .5, and then the redist to the grid will put .05 each
		// way; the bottom is constrained, so we end up with a similar grid as before
		// through symmetry except at bottom and top
		expected.at(0).fill(.025);
		expected.at(4).fill(.025);
		
		expected.modifying().across(1,2).multiply(createArray(.4,.4,.4,2.8));
		
		assertEquals(expected, _target._density,DELTA);
		
		DoubleArray<?> expectedOFP = createArrayOfSize(1,2,2);
		
		expectedOFP.fill(.05);
		expectedOFP.modifying().across(1,2).multiply(createArray(.4,.4,.4,2.8));
		
		assertEquals(expectedOFP, _target._overflowProportions,DELTA);
		assertEquals(1, _target._overflowProportions.sum() + _target._density.sum(),DELTA);
		
		DoubleArray<?> expectedOFV = createArrayOfSize(1,2,2);
		
		expectedOFV.fill(5.5);	
		assertEquals(expectedOFV, _target._overflowAverages,DELTA);
	}
	
	@Test
	public void testAsymmetricOverflow()
	{
		/* With Overflow
		 */
		_target = _target.createSameSized();
		
		_transProbs.at(0,0).fill(.15,.35,.1,.4);
		_transProbs.at(0,1).fill(.35,.1,.4,.15);
		_transProbs.at(1,0).fill(.1,.4,.15,.35);
		_transProbs.at(1,1).fill(.4,.15,.35,.1);
		
		_transitionFn.at(-1,0,0).modifying().add(.5);
		_transitionFn.at(-1,1,1).modifying().add(1);
		
		_simulator.distribute(_src._density, _transitionFn, _transProbs, _target, _grid, true, -1, true);
		
		DoubleArray<?> expected = createArrayOfSize(5,2,2);
		
		expected.fill(.05);
		// The trans is down by .5, and then the redist to the grid will put .05 each
		// way; the bottom is constrained, so we end up with a similar grid as before
		// through symmetry except at bottom and top
		expected.set(.025,0,0,0);
		expected.set(.0,0,1,1);
		expected.set(.025,4,0,0);
		
		// Do the probability reshuffling
		expected.fill(((DoubleArray<?>) createArrayOfSize(5,4).fill(expected.toArray())).matrixMultiply((DoubleArray<?>) createArrayOfSize(4,4).fill(_transProbs.toArray())).toArray());
		
		// Overflow proportions should say the same as with no assymetry above
		assertEquals(expected, _target._density,DELTA);
		
		DoubleArray<?> expectedOFP = createArrayOfSize(1,2,2);
		
		DoubleArray<?> from00 = createArray(.15,.35,.1,.4).multiply(.05);
		DoubleArray<?> from11 = createArray(.4,.15,.35,.1).multiply(.05);
		
		expectedOFP.fill(from00.add(from11).toArray());
		
		assertEquals(expectedOFP, _target._overflowProportions,DELTA);
		assertEquals(1, _target._overflowProportions.sum() + _target._density.sum(),DELTA);

		// But the values are different!
		DoubleArray<?> expectedOFV = createArrayOfSize(1,2,2);
		
		expectedOFV.fill(from00.multiply(5.5).add(from11.multiply(6)).toArray());
		expectedOFV.modifying().divide(expectedOFP);
		assertEquals(expectedOFV, _target._overflowAverages,DELTA);
	}
	
	@Test
	public void testTransitionAsymmetricOverflow()
	{
		/* With Overflow
		 */
		_target = _target.createSameSized();
		
		_transProbs.at(0,0).fill(.15,.35,.1,.4);
		_transProbs.at(0,1).fill(.35,.1,.4,.15);
		_transProbs.at(1,0).fill(.1,.4,.15,.35);
		_transProbs.at(1,1).fill(.4,.15,.35,.1);
		
		_transitionFn.at(-1,0,0).modifying().add(.5);
		_transitionFn.at(-1,1,1).modifying().add(1);
		
		_simulator.transitionForAge(_src, _target, _grid, _transitionFn, _transProbs,  _model, -1);
		
		DoubleArray<?> expected = createArrayOfSize(5,2,2);
		
		expected.fill(.05);
		// The trans is down by .5, and then the redist to the grid will put .05 each
		// way; the bottom is constrained, so we end up with a similar grid as before
		// through symmetry except at bottom and top
		expected.set(.025,0,0,0);
		expected.set(.0,0,1,1);
		expected.set(.025,4,0,0);
		
		// Do the probability reshuffling
		expected.fill(((DoubleArray<?>) createArrayOfSize(5,4).fill(expected.toArray())).matrixMultiply((DoubleArray<?>) createArrayOfSize(4,4).fill(_transProbs.toArray())).toArray());
		
		// Overflow proportions should say the same as with no assymetry above
		assertEquals(expected, _target._density,DELTA);
		
		DoubleArray<?> expectedOFP = createArrayOfSize(1,2,2);
		
		DoubleArray<?> from00 = createArray(.15,.35,.1,.4).multiply(.05);
		DoubleArray<?> from11 = createArray(.4,.15,.35,.1).multiply(.05);
		
		expectedOFP.fill(from00.add(from11).toArray());
		
		assertEquals(expectedOFP, _target._overflowProportions,DELTA);
		assertEquals(1, _target._overflowProportions.sum() + _target._density.sum(),DELTA);

		// But the values are different!
		DoubleArray<?> expectedOFV = createArrayOfSize(1,2,2);
		
		expectedOFV.fill(from00.multiply(5.5).add(from11.multiply(6)).toArray());
		expectedOFV.modifying().divide(expectedOFP);
		assertEquals(expectedOFV, _target._overflowAverages,DELTA);
	}
	
	@Test
	public void testTransitionFromOverflow()
	{
		/* With Overflow
		 */
		_target = _target.createSameSized();
		
		_transProbs.at(0,0).fill(.15,.35,.1,.4);
		_transProbs.at(0,1).fill(.35,.1,.4,.15);
		_transProbs.at(1,0).fill(.1,.4,.15,.35);
		_transProbs.at(1,1).fill(.4,.15,.35,.1);
		
		_transitionFn.at(-1,0,0).modifying().add(.5);
		_transitionFn.at(-1,1,1).modifying().add(1);
		
		// Half as many at 4
		_src._density.at(4).modifying().multiply(.5);
		
		// Put the other half in overflow...
		_src._overflowProportions.fill(_src._density.at(4));
		// ... with values 5.5,6
		DoubleArray<?> srcOAShaped = createArray(5.5,6,6.5,7);
		_src._overflowAverages.fill(srcOAShaped.toArray());
		
		_simulator.transitionForAge(_src, _target, _grid, _transitionFn, _transProbs,  _model, -1);
		
		DoubleArray<?> expected = createArrayOfSize(5,2,2);
		
		expected.fill(.05);
		// The trans is down by .5, and then the redist to the grid will put .05 each
		// way; the bottom is constrained, so we end up with a similar grid as before
		// through symmetry except at bottom and top
		expected.set(.025,0,0,0);
		expected.set(.0,0,1,1);
		expected.set(.025,4,0,0);
		expected.set(.025,4,0,1);
		expected.set(.025,4,1,0);
		
		
		// Do the probability reshuffling
		DoubleArray<?> reshapedTrans = (DoubleArray<?>) createArrayOfSize(4,4).fill(_transProbs.toArray());
		expected.fill(((DoubleArray<?>) createArrayOfSize(5,4).fill(expected.toArray())).matrixMultiply(reshapedTrans).toArray());
		
		// Overflow proportions should say the same as with no assymetry above
		assertEquals(expected, _target._density,DELTA);
		
		DoubleArray<?> expectedOFP = createArrayOfSize(1,2,2);
		
		// Remember only half are left in top row...
		DoubleArray<?> from00 = createArray(.15,.35,.1,.4).multiply(.025);
		DoubleArray<?> from11 = createArray(.4,.15,.35,.1).multiply(.025);
	
		// The other half are coming from ye' good old overflow
		expectedOFP.fill(from00.add(from11).add(createArrayOfSize(1,4).fill(.025).matrixMultiply(reshapedTrans)).toArray());
		
		assertEquals(expectedOFP, _target._overflowProportions,DELTA);
		assertEquals(1, _target._overflowProportions.sum() + _target._density.sum(),DELTA);

		// But the values are different!
		DoubleArray<?> expectedOFV = createArrayOfSize(1,2,2);
		
		DoubleArray<?> oaContrib = srcOAShaped.add(createArray(.5,0,0,1)).multiply(.025).matrixMultiply(reshapedTrans);
		
		expectedOFV.fill(from00.multiply(5.5).add(from11.multiply(6)).add(oaContrib).toArray());
		expectedOFV.modifying().divide(expectedOFP);
		assertEquals(expectedOFV, _target._overflowAverages,DELTA);
	}
	
//	public <S extends IndividualProblemState<?>, M extends Model<?, S>> TransitionRecord<DiscretisedDistribution, Integer> simulateTransition(final DiscretisedDistribution distribution_,
//			M model_, S calcState_, MultiDimensionalArray<Integer, ?> priorAggShockIndices_, MultiDimensionalArray<Integer, ?> futureShocks_) throws ModelException
	
	@Test
	public void testSimulateTransition() throws ModelException
	{
		final IntegerArray<?> currentShocks = IntArrayFactories.createIntArray(3,1);
		
		DoubleArray<?> simPolicy = createArrayOfSize(5,2,2,2,4,5,2,1);
		simPolicy.fillDimensions(createArray(1d,2d,3d,4d,5d), 0);
		
		// add 1 at aggregate 4, 0 at 3 - so at 3.5 will be adding .5
		simPolicy.at(-1,-1,-1,1).modifying().add(1);
		
		DoubleArray<?> eopStates = createArrayOfSize(5,2,2);		
		eopStates.fillDimensions(createArray(1d,2d,3d,4d,5d), 0);
		
		DoubleArray<?> transProbs = createArrayOfSize(2,2,4,5,2,2,4,5,2);
		// Normally 1/4 (i.e. uniform reshuffle) to be in either state
		transProbs.fill(.25);
		// but at the actual aggregate transition		
		transProbs.at(-1,-1,3,1,-1,-1,2,4,1).fill(
				.15,.35,.1,.4,
				.35,.1,.4,.15,
				.1,.4,.15,.35,
				.4,.15,.35,.1);
		
		// Equal likelihood of all future aggregate states
		transProbs.modifying().divide(40);
		
		_context.checking(new Expectations() {{
			allowing(_config).getAggregateExogenousStateCount();will(returnValue(2));
			allowing(_config).getAggregateNormalisingStateCount();will(returnValue(1));
			allowing(_config).getIndividualExogenousStateCount();will(returnValue(2));
			allowing(_config).getAggregateControlCount();will(returnValue(0));
			allowing(_config).getAggregateEndogenousStates();
				will(returnValue(Arrays.asList(createArray(3d,4d))));
			allowing(_config).getExogenousStateTransition();
				will(returnValue(transProbs));
				
			allowing(_model).getAggDetStateStart(); will(returnValue(3));
			allowing(_model).getAggStochStateStart(); will(returnValue(4));
			
			allowing(_model).beforeSimInterpolation(with.is(anything()), with.is(anything()), with.is(anything()));
			allowing(_model).afterSimInterpolation(with.is(anything()), with.is(anything()));
			
				
			oneOf(_model).calculateAggregateStates(with(_src), with(currentShocks), with(_state));
				will(returnValue(new double[] {3.5}));
				
			oneOf(_state).getIndividualPolicyForSimulation(); will(returnValue(simPolicy));
			oneOf(_state).getEndOfPeriodStatesForSimulation(); will(returnValue(eopStates));
			
		}});

		TransitionRecord<DiscretisedDistribution, Integer> record = _simulator.simulateTransition(_src, _model, _state, currentShocks, IntArrayFactories.createIntArray(2,4,1));
		
		// The resulting function should be adding .5, so this should be the outcome
		DoubleArray<?> expected = createArrayOfSize(5,2,2);
		expected.fill(.05);
		expected.at(0).modifying().divide(2);
		expected.at(4).modifying().divide(2);
		
		// Due to the funky probabilities above
		DoubleArray<?> reshapedTrans = (DoubleArray<?>) createArrayOfSize(4,4).fill(_transProbs.toArray());
		expected.fill(((DoubleArray<?>) createArrayOfSize(5,4).fill(expected.toArray())).matrixMultiply(reshapedTrans).toArray());
	
		
		assertEquals(expected, record.getResultingDistribution()._density, DELTA);
	}
	
	@Test
	public void testCreateShockSequenceBasics()
	{
		DoubleArray<?> transProbs = createArrayOfSize(2,4,5,2,4,5,2);
		// Normally 50/50 to be in either state
		transProbs.fill(.5);
		// but at the actual aggregate transition		
		transProbs.at(-1,3,1,-1,2,4,1).fill(.3,.7,.2,.8);
		
		// Equal likelihood of all future aggregate states
		transProbs.modifying().divide(40);
		
		_context.checking(new Expectations() {{
			oneOf(_config).getExogenousStateTransition(); will(returnValue(transProbs));
			oneOf(_config).getAggregateExogenousStateCount();will(returnValue(2));
			oneOf(_config).getAggregateNormalisingStateCount();will(returnValue(1));
			oneOf(_config).getIndividualExogenousStateCount();will(returnValue(1));
			
		}});
		
		IntegerArray<?> initial = IntArrayFactories.createIntArray(2,4,0);
		
		MultiDimensionalArray<Integer, ?> shocks = 
				_simulator.createShockSequence(initial, 11, _model);
		
		// The array has the right size
		assertArrayEquals(new int[] {11,3},shocks.size());
		
		// And starts with the right values
		assertEquals(initial, shocks.at(0));
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void testCreateShockSequenceProbabilities()
	{
		
		DoubleArray<?> z1zp2 = createArrayOfSize(2,4,5,2,4,5,2);
		
		//Z2' = Z1 + 1
		z1zp2.fillDimensions(createArray(0,1,0,0,0,
									0,0,1,0,0,
									0,0,0,1,0,
									0,0,0,0,1),1,5);	
		
		DoubleArray<?> z2zp1 = z1zp2.copy();
		
		z2zp1.fillDimensions(createArray(0,1,0,0,
									0,0,1,0,
									0,0,0,1,
									1,0,0,0,
									1,0,0,0),2,4);	
		
		DoubleArray<?> z2p = z1zp2.copy();
		
		z1zp2.fillDimensions(createArray(0,1,
										 1,0,
										 0,1,
										 1,0,
										 0,1),2,6);	
		
		// If they are all 1, so be it
		DoubleArray<?> transProbs = z1zp2.with(z2zp1, z2p).map(
				(DoubleNaryOp<RuntimeException>)(probs -> probs[0]*probs[1]*probs[2]));
		
		transProbs.modifying().across(0,1,2).divide(transProbs.across(3,4,5,6).sum());
		
		_context.checking(new Expectations() {{
			oneOf(_config).getExogenousStateTransition(); will(returnValue(transProbs));
			oneOf(_config).getAggregateExogenousStateCount();will(returnValue(2));
			oneOf(_config).getAggregateNormalisingStateCount();will(returnValue(1));
			oneOf(_config).getIndividualExogenousStateCount();will(returnValue(1));
			
		}});
		
		IntegerArray<?> initial = IntArrayFactories.createIntArray(2,4,0);
		
		MultiDimensionalArray<Integer, ?> shocks = 
				_simulator.createShockSequence(initial, 1001, _model);
		
		// The array has the right size
		assertArrayEquals(new int[] {1001,3},shocks.size());
		// And starts with the right values
		assertEquals(initial, shocks.at(0));
		
		for(int i = 0;i < 1000;i++) {
			
			 IntegerArray<?> currents = (IntegerArray<?>) shocks.at(i);
			 IntegerArray<?> futures = (IntegerArray<?>) shocks.at(i+1);
			
			 assertEquals((int)currents.get(0)+1, (int)futures.get(1));
			 
			 if(currents.get(1) < 4) 
			 {
				 assertEquals(((int)currents.get(1)+1)% 4, (int)futures.get(0));
			 }
			 else
			 {
				 assertEquals(0, (int)futures.get(0));
			 }
			 
			 assertEquals(((int)currents.get(1)+1)%2, (int)futures.get(2));
		}
		
	}
}
