package com.meliorbis.economics.core;
import static org.junit.Assert.*;

import org.junit.Test;

import com.meliorbis.economics.core.CobbDouglasProduction;

/**
 * 
 */

/**
 * @author toby
 *
 */
public class TestCobbDouglasProduction
{
	@Test
	public void test()
	{
		CobbDouglasProduction production = new CobbDouglasProduction(1d/3, 2d/3, 0.08);
		
		// Check output
		assertEquals(3*4,production.output(1,27,8),1e-10);
		
		// Check return
		assertEquals(1d/3*1d/9*4-0.08,production.netReturn(1,27,8),1e-10);

		// Check wage
		assertEquals(2d/3*3*1/2,production.wage(1,27,8),1e-10);
		
		// Check implied Cap-lab Ratio
		assertEquals(8d,production.impliedCapLabRatio(1,1d/12-0.08),1e-10);
	}
}
