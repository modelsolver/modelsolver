package com.meliorbis.economics.core;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestCRRAUtility
{

	@Test
	public void testValues()
	{
		UtilityFunction utility = new CRRAUtility(3);
		
		assertEquals(-1d/18,utility.utility(3d),1e-10);
		assertEquals(1d/27,utility.marginalUtility(0,3d),1e-10);
	}

}
