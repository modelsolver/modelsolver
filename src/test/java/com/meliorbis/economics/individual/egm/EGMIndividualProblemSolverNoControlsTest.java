package com.meliorbis.economics.individual.egm;

import static com.meliorbis.numerics.DoubleArrayFactories.createArrayOfSize;

import org.jmock.integration.junit4.JUnitRuleMockery;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.meliorbis.economics.infrastructure.AbstractModel;
import com.meliorbis.economics.infrastructure.AbstractStateBase;
import com.meliorbis.economics.infrastructure.ModelConfigForTests;
import com.meliorbis.economics.model.Model;
import com.meliorbis.economics.model.ModelException;
import com.meliorbis.economics.model.State;
import com.meliorbis.numerics.DoubleArrayFactories;
import com.meliorbis.numerics.generic.primitives.DoubleArray;
import com.meliorbis.numerics.generic.primitives.impl.Interpolation;
import com.meliorbis.numerics.test.ArrayAssert;
import com.meliorbis.utils.Utils;

public class EGMIndividualProblemSolverNoControlsTest
{
	private static final double DELTA = 1e-10;
	@Rule public JUnitRuleMockery _mockery = new JUnitRuleMockery();
	public State<ModelConfigForTests> _state;
	
	ModelConfigForTests _config;
	private AbstractModel<ModelConfigForTests, State<ModelConfigForTests>> _model;
	private DoubleArray<?> _indStates;
	private DoubleArray<?> _statesGrid;
	private EGMIndividualProblemSolver<ModelConfigForTests, State<ModelConfigForTests>, Model<ModelConfigForTests, State<ModelConfigForTests>>> _solver;
	
	@Before
	public void setUp()
	{		
		_config = new ModelConfigForTests();
		double[] values_ =
		{ 1, 2, 3, 4 };
		
		_config.setAggregateEndogenousStates(DoubleArrayFactories.createArray(values_));
		double[] values_1 =
		{ 3, 4, 5 };
		_config.setAggregateExogenousStates(DoubleArrayFactories.createArray(values_1));
		double[] values_2 =
		{ .9, 1, 1.1 };
		_config.setAggregateNormalisingExogenousStates(DoubleArrayFactories.createArray(values_2));
		double[] values_3 =
		{ 1, 2, 3, 4, 5 };
		_indStates = DoubleArrayFactories.createArray(values_3);
		_config.setIndividualEndogenousStates(_indStates);
		double[] values_4 =
		{ 1, 2 };
		_config.setIndividualExogenousStates(DoubleArrayFactories.createArray(values_4));
		_config._constrained = true;
		int[] size_ =
		{ 2, 3, 2, 3, 3 };

		_config.setExogenousStateTransiton(createArrayOfSize(size_).fill(1d/18d));
	
		
		_model = new AbstractModel<ModelConfigForTests, State<ModelConfigForTests>>()
		{

			@Override
			public boolean shouldUpdateAggregates(State<ModelConfigForTests> state_)
			{
				return false;
			}

			@Override
			public State<ModelConfigForTests> initialState()
			{
				return null;
			}
		};
		
		_model.setConfig(_config);
		_model.initialise();
		
		_statesGrid = _model.createIndividualVariableGrid(1);
		_statesGrid.fillDimensions(_indStates,0);
		
		_solver = new EGMIndividualProblemSolver<ModelConfigForTests, State<ModelConfigForTests>, Model<ModelConfigForTests,State<ModelConfigForTests>>>(_model, _config)
		{

			@Override
			protected DoubleArray<?> calculateFutureConditionalExpectations(int[] exogenousStates_, int[] aggDetStateIndex_,
					State<ModelConfigForTests> state_) throws ModelException
			{
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			protected DoubleArray<? extends DoubleArray<?>> calculateImpliedStartOfPeriodState(DoubleArray<?> expectedConditions_,
					State<ModelConfigForTests> state_) throws ModelException
			{
				// TODO Auto-generated method stub
				return null;
			}
		};
		_solver.setEndOfPeriodStates(_statesGrid);
		
		DoubleArray<?> sopStates = _model.createIndividualTransitionGrid(1);
		
		// baseline are the states
		sopStates.fillDimensions(_config.getIndividualEndogenousStates().get(0),0);
		
		// But they are normalised so that if a permanent shock hits, they are relative to the new productivity level
		sopStates.across(sopStates.numberOfDimensions()-2).modifying().divide(_config.getAggregateNormalisingExogenousStates().get(0));
		
		_solver.setStartOfPeriodStates(sopStates);
		
		_state = new AbstractStateBase<ModelConfigForTests>(_config)
		{
		};

		
		_solver.initialise(_state);
	}
	
	@Test
	public void testInterpIndividualTransition()
	{
		
		
		DoubleArray<?> impliedSOPStates = _model.createIndividualVariableGrid();
		impliedSOPStates.fillDimensions(_indStates.add(1), 0);

		DoubleArray<?> interped = _solver.interpIndividualTransition(impliedSOPStates, _state);
		
		// First, confirm the un-normalised case
		DoubleArray<?> expectedWithoutShock = _model.createIndividualVariableGrid();
		
		// Should be one less (since k+1 maps t k in the input, k must map to k-1
		// Expect there is a constraint, so the first value is also 1
		expectedWithoutShock.fillDimensions(_indStates.subtract(1), 0);
		expectedWithoutShock.at(0).fill(_indStates.get(0));
		
		ArrayAssert.assertEquals(expectedWithoutShock, interped.at(-1,-1,-1,-1,1,-1), DELTA);
		
		// Now for the negative shock, the normalised implied initial state is 10% higher
		DoubleArray<?> expectedWithBadShock = Interpolation.interpolateFunction(_statesGrid, expectedWithoutShock, 0, _indStates.divide(.9));
		
		ArrayAssert.assertEquals(expectedWithBadShock, interped.at(-1,-1,-1,-1,0,-1), DELTA);

		// Similarly for the positive shock
		DoubleArray<?> expectedWithGoodShock = Interpolation.interpolateFunction(_statesGrid, expectedWithoutShock, 0, _indStates.divide(1.1));
		
		ArrayAssert.assertEquals(expectedWithGoodShock, interped.at(-1,-1,-1,-1,2,-1), DELTA);

	}
	
	@Test public void testIndividualUnconditionalExpectation()
	{
		DoubleArray<?> conditionalExpectation = createArrayOfSize(_solver._futureConditionalContribsSize);
		
		conditionalExpectation.fillDimensions(Utils.sequence(1, 18, 18),1,2,3);
		
		DoubleArray<?> unconditionalExpectation = _solver.individualUnconditionalExpectation(conditionalExpectation, _state);
		
		final DoubleArray<?> expected = _model.createIndividualVariableGrid(1);
		expected.fill(19d/2);
		
		ArrayAssert.assertEquals(expected, unconditionalExpectation, DELTA);
		double[] values_ =
		{ 1, 2, 3, 4 };
		
		/* This time, differentiate by current agg endo state
		 */
		conditionalExpectation.modifying().across(4).multiply(DoubleArrayFactories.createArray(values_));

		unconditionalExpectation = _solver.individualUnconditionalExpectation(conditionalExpectation, _state);
		double[] values_1 =
		{ 1, 2, 3, 4 };
		
		expected.modifying().across(2).multiply(DoubleArrayFactories.createArray(values_1));
		ArrayAssert.assertEquals(expected, unconditionalExpectation, DELTA);
		double[] values_2 =
		{ 1, 2 };
		
		/* This time, differentiate by future ind exo state...
		 */
		conditionalExpectation.fillDimensions((DoubleArray<?>) DoubleArrayFactories.createArray(values_2), 1);

		//...and make the ind exo state sticky!
		DoubleArray<?> trans = _config.getExogenousStateTransition().copy();
		double[] values_3 =
		{ 2, 0, 0, 2 };
		trans.modifying().across(0,2).multiply(DoubleArrayFactories.createArray(values_3)); // need to double probabilities because half the states disappear
		_config.setExogenousStateTransiton(trans);
		
		unconditionalExpectation = _solver.individualUnconditionalExpectation(conditionalExpectation, _state);
		double[] values_4 =
		{ 1, 2 };
		
		expected.fillDimensions((DoubleArray<?>) DoubleArrayFactories.createArray(values_4), 1);
		ArrayAssert.assertEquals(expected, unconditionalExpectation, DELTA);
		
	}

}
